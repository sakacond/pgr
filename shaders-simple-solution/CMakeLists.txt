cmake_minimum_required(VERSION 3.15)
project(basic_project)

# sample cmakelist for your pgr projects

set(CMAKE_CXX_STANDARD 14)

include_directories(.)

add_executable(${PROJECT_NAME}
        shaders-simple.cpp)

add_definitions(-Wno-deprecated)

find_package(DevIL REQUIRED)
find_package(assimp REQUIRED)
find_package(GLUT REQUIRED)
find_package(OpenGL REQUIRED)

# HERE edit your path to pgr framework folder
set(PATH_TO_PRG_FRAMEWORK "/home/ondrej/School/6sem/PGR/pgr-framework")


include_directories(.
        ${CMAKE_EXTRA_GENERATOR_CXX_SYSTEM_INCLUDE_DIRS}
        ${PATH_TO_PRG_FRAMEWORK}/include)

target_include_directories(
        ${PROJECT_NAME}
        PUBLIC
        ${PATH_TO_PRG_FRAMEWORK}/include
        ${OPENGL_INCLUDE_DIR}
        ${GLUT_INCLUDE_DIR}
        ${IL_INCLUDE_DIR}/..
)

target_link_libraries(
        ${PROJECT_NAME}
        PUBLIC
        ${PATH_TO_PRG_FRAMEWORK}/build/libpgr.a
        ${assimp_DIR}/../../${ASSIMP_LIBRARIES}
        ${IL_LIBRARIES}
        ${OPENGL_LIBRARIES}
        ${GLUT_LIBRARIES}
)
