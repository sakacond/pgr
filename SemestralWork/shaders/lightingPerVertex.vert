#version 140

struct Material {      // structure that describes currently used material
  vec3  ambient;       // ambient component
  vec3  diffuse;       // diffuse component
  vec3  specular;      // specular component
  float shininess;     // sharpness of specular reflection

  bool  useTexture;    // defines whether the texture is used or not
  bool  useMixTexture;
};

uniform sampler2D texSampler;  // sampler for the texture access
uniform sampler2D fogSampler;
uniform sampler2D mixSampler;

uniform bool transformationCheck;

in vec3 position;           // vertex position in world space
in vec3 normal;             // vertex normal
in vec2 texCoord;           // incoming texture coordinates

uniform float time;         // time used for simulation of moving lights (such as sun)
uniform Material material;  // current material

uniform mat4 PVMmatrix;     // Projection * View * Model  --> model to clip coordinates
uniform mat4 Vmatrix;       // View                       --> world to eye coordinates
uniform mat4 Mmatrix;       // Model                      --> model to world coordinates
uniform mat4 normalMatrix;  // inverse transposed Mmatrix

uniform vec3 cameraPosition;
uniform vec3 reflectorPosition;   // reflector position (world coordinates)
uniform vec3 reflectorDirection;  // reflector direction (world coordinates)
uniform bool reflectorOn;

smooth out vec2 texCoord_v;  // outgoing texture coordinates
smooth out vec2 fogCoord_v;

out float dist_v;
out vec3 normalInterp;
out vec3 position_v;

void main() {
  vec4 worldPosition;

  if(transformationCheck == true) {
    vec3 nPos = vec3(sin(time * position.z) * 2.0 + position.x, cos(time) * 1.2 + position.y, position.z);
    gl_Position = PVMmatrix * vec4(nPos, 1);
    vec4 vertPos4 = Mmatrix * Vmatrix * vec4(nPos, 1.0);
    position_v = vec3(vertPos4) / vertPos4.w;
    normalInterp = vec3(normalMatrix * vec4(normal, 0.0));
    worldPosition = Mmatrix * vec4(nPos, 1.0);
  }
  else{
    gl_Position = PVMmatrix * vec4(position, 1.0);  
    vec4 vertPos4 = Mmatrix * Vmatrix * vec4(position, 1.0);
    position_v = vec3(vertPos4) / vertPos4.w;
    normalInterp = vec3(normalMatrix * vec4(normal, 0.0));
    worldPosition = Mmatrix * vec4(position, 1.0);
  }

  texCoord_v = texCoord;
  
  dist_v = distance(cameraPosition, worldPosition.xyz);

  float offsetY = (sin(time)/2+0.5)*0.5;
  fogCoord_v = texCoord;
  fogCoord_v.y += offsetY;
}
