#version 140

uniform sampler2D texSampler;
uniform float health;

smooth in vec2 texCoord_v;
out vec4 color_f;

void main() {
    float alpha = clamp((200 / health) - 1, 0.0, 1.0);
 
    color_f = texture(texSampler, texCoord_v);
    color_f = alpha * color_f;
}