#version 140

uniform mat4 inversePVmatrix;
uniform vec3 cameraPosition;
uniform float time;

in vec2 screenCoord;

out vec2 fogCoord_v;
out vec3 texCoord_v;
out float dist_v;

void main() {
  vec4 farplaneCoord = vec4(screenCoord, 0.9999, 1.0);
  vec4 worldViewCoord = inversePVmatrix * farplaneCoord;
  texCoord_v = worldViewCoord.xyz / worldViewCoord.w;
  gl_Position = farplaneCoord;

  dist_v = abs(cameraPosition.z);

  fogCoord_v = vec2(sin(time * 0.2 + screenCoord.x), sin(time * 0.2 + screenCoord.y));
}