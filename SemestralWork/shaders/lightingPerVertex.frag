#version 140

struct Material {           // structure that describes currently used material
  vec3  ambient;            // ambient component
  vec3  diffuse;            // diffuse component
  vec3  specular;           // specular component
  float shininess;          // sharpness of specular reflection

  bool  useTexture;         // defines whether the texture is used or not
  bool  useMixTexture;
};

struct Light {         // structure describing light parameters
  vec3  color;
  vec3  ambient;       // intensity & color of the ambient component
  vec3  diffuse;       // intensity & color of the diffuse component
  vec3  specular;      // intensity & color of the specular component
  vec3  position;      // light position
  vec3  spotDirection; // spotlight direction
  float spotCosCutOff; // cosine of the spotlight's half angle
  float spotExponent;  // distribution of the light energy within the reflector's cone (center->cone's edge)
  float power;
};

// hardcoded lights
Light sun;
float sunSpeed = 2.0f;
Light blueFlame;
Light reflector;

// assuming the monitor is calibrated to RGB space
const float gamma = 2.2;

// fog decay
float decay = 1.0f;

uniform sampler2D texSampler;  // sampler for the texture access
uniform sampler2D fogSampler;
uniform sampler2D mixSampler;

uniform bool transformationCheck;

in vec3 position;           // vertex position in world space
in vec3 normal;             // vertex normal
in vec2 texCoord;           // incoming texture coordinates

uniform float time;         // time used for simulation of moving lights (such as sun)
uniform Material material;  // current material

uniform mat4 PVMmatrix;     // Projection * View * Model  --> model to clip coordinates
uniform mat4 Vmatrix;       // View                       --> world to eye coordinates
uniform mat4 Mmatrix;       // Model                      --> model to world coordinates
uniform mat4 normalMatrix;  // inverse transposed Mmatrix

uniform vec3 cameraPosition;
uniform vec3 reflectorPosition;   // reflector position (world coordinates)
uniform vec3 reflectorDirection;  // reflector direction (world coordinates)
uniform bool reflectorOn;
uniform bool foggy;

smooth in vec2 texCoord_v;  // outgoing texture coordinates
smooth in vec2 fogCoord_v;
in float dist_v;

in vec3 normalInterp;
in vec3 position_v;


float calcAttentuation(vec3 lightPos, vec3 vertexPos) {
  float dist = length(lightPos - vertexPos);
  return clamp(10.0 / dist, 0.0, 1.0);
}

vec4 pointLight(Light light, Material material, vec3 vertexPosition, vec3 vertexNormal) {
  vec3 normal = normalize(normalInterp);
  vec3 lightDirection = light.position - vertexPosition;
  float dist = length(lightDirection);
  dist = pow(dist, 2.0);
  lightDirection = normalize(lightDirection);

  float lambertian = max(dot(lightDirection, normal), 0.0);
  float specular = 0.0;

  if (lambertian > 0.0) {
    vec3 viewDirection = normalize(-vertexPosition);

    // this is blinn phong
    vec3 halfDirection = normalize(lightDirection + viewDirection);
    float specAngle = max(dot(halfDirection, normal), 0.0);
    specular = pow(specAngle, material.shininess);
  }

  vec3 colorLinear = light.ambient +
                     light.diffuse * lambertian * light.color * light.power / dist +
                     light.specular * specular * light.color * light.power / dist;

  float attent = calcAttentuation(light.position, vertexPosition);
  float attentFactor = 1 / (0.0 * distance(light.position, vertexPosition) + 
                    attent * (distance(light.position, vertexPosition)) * 
                    (distance(light.position, vertexPosition)));
  
  colorLinear += attentFactor;

  vec3 colorGammaCorrected = pow(colorLinear, vec3(1.0 / gamma));
  
  // use the gamma corrected color in the fragment
  return vec4(colorGammaCorrected, 1.0); 
}

vec4 spotLight(Light light, Material material, vec3 vertexPosition, vec3 vertexNormal) {
  vec3 normal = normalize(normalInterp);
  vec3 lightDirection = light.position - vertexPosition;
  float dist = length(lightDirection);
  dist = pow(dist, 2.0);
  lightDirection = normalize(lightDirection);

  float lambertian = max(dot(lightDirection, normal), 0.0);
  float specular = 0.0;

  if (lambertian > 0.0) {
    vec3 viewDirection = normalize(-vertexPosition);

    // this is blinn phong
    vec3 halfDirection = normalize(lightDirection + viewDirection);
    float specAngle = max(dot(halfDirection, normal), 0.0);
    specular = pow(specAngle, material.shininess);
  }

  vec3 colorLinear = light.ambient +
                     light.diffuse * lambertian * light.color * light.power / dist +
                     light.specular * specular * light.color * light.power / dist;
  
  float spotCoef = max(0.0, dot(-lightDirection, light.spotDirection));

  if(spotCoef < light.spotCosCutOff)
    colorLinear *= 0.0;
  else
    colorLinear *= pow(spotCoef, light.spotExponent);

  // apply gamma correction (assume ambientColor, diffuseColor and specColor
  // have been linearized, i.e. have no gamma correction in them)
  vec3 colorGammaCorrected = pow(colorLinear, vec3(1.0 / gamma));
  
  // use the gamma corrected color in the fragment
  return vec4(colorGammaCorrected, 1.0);
}

vec4 directionalLight(Light light, Material material, vec3 vertexPosition, vec3 vertexNormal) {
  vec3 normal = normalize(normalInterp);
  vec3 lightDirection = normalize(light.position - vec3(0, 0, 0));
  float dist = length(lightDirection);
  dist = pow(dist, 2.0);
  lightDirection = normalize(lightDirection);

  float lambertian = max(dot(lightDirection, normal), 0.0);
  float specular = 0.0;

  if (lambertian > 0.0) {
    vec3 viewDirection = normalize(-vertexPosition);

    // this is blinn phong
    vec3 halfDirection = normalize(lightDirection + viewDirection);
    float specAngle = max(dot(halfDirection, normal), 0.0);
    specular = pow(specAngle, material.shininess);
  }

  vec3 colorLinear = light.ambient +
                     light.diffuse * lambertian * light.color * light.power / dist +
                     light.specular * specular * light.color * light.power / dist;
  
  // apply gamma correction (assume ambientColor, diffuseColor and specColor
  // have been linearized, i.e. have no gamma correction in them)
  vec3 colorGammaCorrected = pow(colorLinear, vec3(1.0 / gamma));
  
  // use the gamma corrected color in the fragment
  return vec4(colorGammaCorrected, 1.0);
}


void setupLights() {
  // set up sun parameters
  sun.ambient  = vec3(0.0);
  sun.diffuse  = vec3(1.0, 0.6, 0.5);
  sun.specular = vec3(1.0);
  sun.color = vec3(1.0, 0.6, 0.5);
  sun.power = 0.5;

  sun.position = (Vmatrix * vec4(15 * sin(time * sunSpeed), 15 * cos(time * sunSpeed), 15 * cos(time * sunSpeed), 0.0)).xyz;

  // set up reflector parameters
  reflector.ambient       = vec3(0.2);
  reflector.diffuse       = vec3(1.0);
  reflector.specular      = vec3(1.0);
  reflector.spotCosCutOff = 0.95f;
  reflector.spotExponent  = 0.2;
  reflector.color = vec3(0.0, 0.6, 0.6);
  reflector.power = 0.2;

  reflector.position = (Vmatrix * vec4(reflectorPosition, 1.0)).xyz;
  reflector.spotDirection = normalize((Vmatrix * vec4(reflectorDirection, 0.0)).xyz);

  // set up point light in the middle
  blueFlame.ambient  = vec3(0.2);
  blueFlame.diffuse  = vec3(0.0, 0.6, 0.1f);
  blueFlame.specular = vec3(0.2);
  blueFlame.color = vec3(0.0, 0.6, 0.2f);
  blueFlame.power = 2.0;

  blueFlame.position = (Vmatrix * vec4(0, 2, 1.05, 0.0)).xyz;
}

out vec4 color_f;        // outgoing fragment color

void main() {
  setupLights();
  
  // eye-coordinates position and normal of vertex
  vec3 vertexPosition = (Vmatrix * Mmatrix * vec4(position_v, 1.0)).xyz;       // vertex in eye coordinates

  vec3 vertexNormal = normalize( (Vmatrix * normalMatrix * vec4(normal, 0.0) ).xyz);   // normal in eye coordinates by NormalMatrix

  // initialize the output color with the global ambient term
  vec3 globalAmbientLight = vec3(0.4f);
  vec4 outputColor = vec4(material.ambient * globalAmbientLight, 0.0);

  // accumulate contributions from all lights
  outputColor += directionalLight(sun, material, vertexPosition, vertexNormal);
  outputColor += pointLight(blueFlame, material, vertexPosition, vertexNormal);
  if(reflectorOn)
    outputColor += spotLight(reflector, material, vertexPosition, vertexNormal);

  color_f = outputColor;

  // if material has a texture -> apply it
  if(material.useTexture)
    color_f =  color_f * texture(texSampler, texCoord_v);
  
  // if material has mix texture blend them
  if(material.useMixTexture) {
    float mixAlpha = 0.7;
    vec4 mixColor = texture(mixSampler, texCoord_v);

    color_f.xyz = mixAlpha * color_f.xyz + (1 - mixAlpha) * mixColor.xyz;
  }

  // create and blend fog
  vec4 fogColor = texture(fogSampler, fogCoord_v);

  float fogDensity = 0.06;
  if(foggy){
    fogDensity = 0.12;
  }

  float fogBlend;
  fogBlend = exp(-pow(fogDensity * dist_v, 2.0));
  fogBlend = clamp(fogBlend, 0.0, 1.0);

  color_f = fogBlend * color_f + (1 - fogBlend) * fogColor;
}
