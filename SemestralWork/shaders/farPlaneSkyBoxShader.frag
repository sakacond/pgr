#version 140

uniform samplerCube skyboxSampler;
uniform sampler2D fogSampler;
uniform bool foggy;

in vec3 texCoord_v;
in vec2 fogCoord_v;
in float dist_v;
out vec4 color_f;

void main() {
  color_f = texture(skyboxSampler, texCoord_v);

  float fogDensity = 1.2;
  if (foggy == true) {
    fogDensity = 2;
  }

  vec4 fogColor = texture(fogSampler, fogCoord_v);

  float fogBlend;
  fogBlend = exp(-pow((1 / fogDensity) * dist_v, 2.0));
  fogBlend = clamp(fogBlend, 0.0, 1.0);

  color_f = fogBlend * fogColor + (1 - fogBlend) * color_f;
}