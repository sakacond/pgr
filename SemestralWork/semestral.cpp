/**
 * @file semestral.cpp
 * @author Ondrej Sakači (sakacond@fel.cvut.cz)
 * @brief 
 * @version 0.1
 * @date 2021-05-30
 * 
 * 
 */

#include <time.h>
#include <list>
#include <iostream>
#include <tuple>
#include <cmath>

#include "pgr.h"
#include "render_stuff.h"
#include "shader.h"
#include "spline.h"

extern SCommonShaderProgram shaderProgram;
extern bool useLighting;

static int val = 0;
static int menuVal = 0;
static int configGround = 1;
static int showMaterials = 1;
static int skybox = 1;

/**
 * @brief Definitions of game state changing variables.
 * 
 */
struct GameState {
public: 
  int windowWidth;    // set by reshape callback
  int windowHeight;   // set by reshape callback

  Camera camera = FOLLOW;
  glm::vec3 lastCameraPosition = glm::vec3(0.0f, 0.0f, 0.0f);
  glm::vec3 lastCameraDirection = glm::vec3(0.0f, 0.0f, 0.0f);
  glm::vec3 lastCameraUpVector = glm::vec3(0.0f, 0.0f, 0.0f);
  float lastCameraLeaveTime = 0.01f;

  bool drawHitboxes = false;
  bool inputLocked = false;
  bool drawGround = true;

  bool gameOver;              // false;
  bool keyMap[KEYS_COUNT];    // false

  bool day = true;
  bool configGround = false;
  bool useCustomMats = true;
  bool dispMenu = true;
  bool reflectorOn = false;

  float elapsedTime;
  float missileLaunchTime;
  float ufoMissileLaunchTime;
} gameState;

GameObjects gameObjects;

/**
 * @brief Checks if the point is in sphere
 * 
 * @param point 
 * @param center 
 * @param radius 
 * @return true 
 * @return false 
 */
bool pointInSphere(const glm::vec3 &point, const glm::vec3 &center, float radius) {
  float dx = point.x - center.x;
  float dy = point.y - center.y;
  float dz = point.z - center.z;
  float dist = dx * dx + dy * dy + dz * dz;

  //if (dist <= radius * radius)
  //if(glm::dot(point - center, point - center) <= radius * radius)
  if(glm::distance(center, point) <= radius)
    return true;
  else 
    return false;
}

/**
 * @brief Checks spheres intersection.
 * 
 * @param center1 
 * @param radius1 
 * @param center2 
 * @param radius2 
 * @return true 
 * @return false 
 */
bool spheresIntersection(const glm::vec3 &center1, float radius1, const glm::vec3 &center2, float radius2) {
  float dx = center1.x - center2.x;
  float dy = center1.y - center2.y;
  float dz = center1.z - center2.z;
  float dist = dx * dx + dy * dy + dz * dz;

  if(glm::dot(center1-center2, center1 - center2) <= (radius1+radius2)*(radius1+radius2))
      return true;
  else
      return false;
}

void insertExplosion(const glm::vec3 &position) {

  ExplosionObject* newExplosion = new ExplosionObject;

  newExplosion->speed = 0.0f;
  newExplosion->destroyed = false;

  newExplosion->startTime = gameState.elapsedTime;
  newExplosion->currentTime = newExplosion->startTime;

  newExplosion->size = BILLBOARD_SIZE;
  newExplosion->direction = glm::vec3(0.0f, 0.0f, 1.0f);

  newExplosion->frameDuration = 0.1f;
  newExplosion->textureFrames = 16;

  newExplosion->position = position;

  gameObjects.explosions.push_back(newExplosion);
}

void insertBlueFire(const glm::vec3 &position, bool destroyable) {

  BlueFireObject* blueFire = new BlueFireObject;

  blueFire->speed = 0.0f;
  blueFire->destroyed = false;
  blueFire->destroyable = destroyable;

  blueFire->startTime = gameState.elapsedTime;
  blueFire->currentTime = blueFire->startTime;

  blueFire->size = BILLBOARD_SIZE;
  blueFire->direction = glm::vec3(0.0f, 0.0f, 1.0f);

  blueFire->frameDuration = 0.1f;
  blueFire->textureFrames = 20;

  blueFire->position = position;

  gameObjects.blueFires.push_back(blueFire);
}

void insertTopText(const std::string& text) {
  TextObject* textObject;
  textObject->text = text;

  textObject->startTime = gameState.elapsedTime;
  textObject->currentTime = textObject->startTime;

  textObject->position =  glm::vec3(-8.0f, +7.0f, 0.0f); 
  textObject->size = TOP_CHARACTER_SIZE;

  textObject->duration = 1.5f;

  GameObjectsList::iterator iter;
  for(iter = gameObjects.topTexts.begin(); iter != gameObjects.topTexts.end(); ++iter) {
    TextObject* obj = (TextObject*)(*iter);
    obj->moveUp(textObject->size * 2);
  }

  gameObjects.topTexts.push_back(textObject);

  printf("Created textObject with text: %s", textObject->text);
}

/**
 * @brief Deprecated function for creating text object.
 * 
 * @param text 
 */
void insertBottomText(const std::string& text) {
  TextObject* textObject;
  textObject->text = text;

  textObject->startTime = gameState.elapsedTime;
  textObject->currentTime = textObject->startTime;

  textObject->position = glm::vec3(5.0f, 5.0f, 0.0f);
  textObject->size = BOTTOM_CHARACTER_SIZE;
  
  textObject->duration = 1.5f;

  GameObjectsList::iterator iter;
  for(iter = gameObjects.bottomTexts.begin(); iter != gameObjects.bottomTexts.end(); ++iter) {
    TextObject* obj = (TextObject*)(*iter);
    obj->moveUp(textObject->size * 2);
  }

  gameObjects.bottomTexts.push_back(textObject);

  printf("Created textObject with text: %s", textObject->text);
}

void increaseCarSpeed(float delta = CAR_SPEED_INCREMENT) {
    if(!gameObjects.car->isBreaking)
        gameObjects.car->speed = std::min(gameObjects.car->speed + delta, CAR_SPEED_MAX);
}

void decreaseCarSpeed(float delta = CAR_SPEED_INCREMENT) {
    if (!gameObjects.car->isBreaking)
        gameObjects.car->speed = std::max(gameObjects.car->speed - delta, -(CAR_SPEED_MAX * 0.5f));
}

void increaseFreeCameraSpeed() {
  if (gameObjects.freeCameraSpeed < CAMERA_SPEED_MAX)
    gameObjects.freeCameraSpeed += 0.1f;
  else gameObjects.freeCameraSpeed = CAMERA_SPEED_MAX;
}

void decreaseFreeCameraSpeed() {
  if (gameObjects.freeCameraSpeed > CAMERA_SPEED_MIN)
    gameObjects.freeCameraSpeed -= 0.1f;
  else gameObjects.freeCameraSpeed = CAMERA_SPEED_MIN;
}

void turnCarLeft(float deltaAngle) {
    gameObjects.car->viewAngle += deltaAngle * gameObjects.car->speed;
    gameObjects.car->sideForce = deltaAngle;

    if (gameObjects.car->viewAngle > 360.0f)
        gameObjects.car->viewAngle -= 360.0f;

    float angle = glm::radians(gameObjects.car->viewAngle);

    gameObjects.car->direction.x = cos(angle);
    gameObjects.car->direction.y = sin(angle);
}

void turnCarRight(float deltaAngle) {
    gameObjects.car->viewAngle -= deltaAngle * gameObjects.car->speed;
    gameObjects.car->sideForce = -deltaAngle;

    if (gameObjects.car->viewAngle < 0.0f)
        gameObjects.car->viewAngle += 360.0f;

    float angle = glm::radians(gameObjects.car->viewAngle);

    gameObjects.car->direction.x = cos(angle);
    gameObjects.car->direction.y = sin(angle);
}

void turnCameraLeft(float deltaAngle) {
    gameObjects.freeCamera->viewAngle += deltaAngle;

    if (gameObjects.freeCamera->viewAngle > 360.0f)
        gameObjects.freeCamera->viewAngle -= 360.0f;

    float angle = glm::radians(gameObjects.freeCamera->viewAngle);

    gameObjects.freeCamera->direction.x = cos(angle);
    gameObjects.freeCamera->direction.y = sin(angle);
}

void turnCameraRight(float deltaAngle) {
    gameObjects.freeCamera->viewAngle -= deltaAngle;

    if (gameObjects.freeCamera->viewAngle < 0.0f)
        gameObjects.freeCamera->viewAngle += 360.0f;

    float angle = glm::radians(gameObjects.freeCamera->viewAngle);

    gameObjects.freeCamera->direction.x = cos(angle);
    gameObjects.freeCamera->direction.y = sin(angle);
}

void turnCameraUp(float deltaAngle) {
    gameObjects.freeCamera->verticalAngle += deltaAngle;

    if (gameObjects.freeCamera->verticalAngle > 180.0f)
        gameObjects.freeCamera->verticalAngle = 180.0f;
}

void turnCameraDown(float deltaAngle) {
    gameObjects.freeCamera->viewAngle -= deltaAngle;

    if (gameObjects.freeCamera->verticalAngle < 0.0f)
        gameObjects.freeCamera->verticalAngle = 0.0f;
}

void useHandBrake(float delta = CAR_BRAKE_INCREMENT) {
    gameObjects.car->isBreaking = true;

    printf("Breaking\n");

    if (gameObjects.car->speed > 0) {
        gameObjects.car->speed =
            std::max(gameObjects.car->speed - delta, 0.0f);
    }
    else {
        gameObjects.car->speed =
            std::min(gameObjects.car->speed + delta, 0.0f);
    }
    gameObjects.car->viewAngle += gameObjects.car->sideForce * 4.0f * gameObjects.car->speed;
}

/**
 * @brief Turns on reflector of the car.
 * 
 */
void turnOnHeadlights(void) {
  gameState.reflectorOn = !gameState.reflectorOn;
}

void teleport(void) {
  // generate new car position randomly
  gameObjects.car->position = glm::vec3(
    (float)(20.0 * (rand() / (double)RAND_MAX) - 10.0),
    (float)(20.0 * (rand() / (double)RAND_MAX) - 10.0),
    0.0f
  );
}

/**
 * @brief Cleans up objects
 * 
 */
void cleanUpObjects(void) {
  if(!gameObjects.car != NULL) {
    delete gameObjects.car;
    gameObjects.car = NULL;
  }

  if(!gameObjects.simpleCar != NULL) {
    delete gameObjects.simpleCar;
    gameObjects.simpleCar = NULL;
  }

  if(!gameObjects.simpleCar2 != NULL) {
    delete gameObjects.simpleCar2;
    gameObjects.simpleCar2 = NULL;
  }

  if(!gameObjects.freeCamera != NULL) {
    delete gameObjects.freeCamera;
    gameObjects.freeCamera = NULL;
  }

  if(!gameObjects.revolver != NULL) {
    delete gameObjects.revolver;
    gameObjects.revolver = NULL;
  }

  if(!gameObjects.epitrochoidCamera != NULL) {
    delete gameObjects.epitrochoidCamera;
    gameObjects.epitrochoidCamera = NULL;
  }

  if(!gameObjects.bannerObject != NULL) {
    delete gameObjects.bannerObject;
    gameObjects.bannerObject = NULL;
  }
  
  // delete houses
  while (!gameObjects.buildings.empty()) {
      delete gameObjects.buildings.back();
      gameObjects.buildings.pop_back();
  }

  // delete houses
  while (!gameObjects.lights.empty()) {
      delete gameObjects.lights.back();
      gameObjects.lights.pop_back();
  }

  // delete explosions
  while(!gameObjects.explosions.empty()) {
    delete gameObjects.explosions.back();
    gameObjects.explosions.pop_back();
  } 

  // delete blueFires
  while(!gameObjects.blueFires.empty()) {
    delete gameObjects.blueFires.back();
    gameObjects.blueFires.pop_back();
  } 

  // delete texts
  while(!gameObjects.topTexts.empty()) {
    delete gameObjects.topTexts.back();
    gameObjects.topTexts.pop_back();
  } 
  while(!gameObjects.bottomTexts.empty()) {
    delete gameObjects.bottomTexts.back();
    gameObjects.bottomTexts.pop_back();
  } 

  // remove banner
  if(gameObjects.bannerObject != NULL) {
    delete gameObjects.bannerObject;
    gameObjects.bannerObject = NULL;
  }
}

// generates random position that does not collide with the spaceship
glm::vec3 generateRandomPosition(void) {
 glm::vec3 newPosition;
  bool invalidPosition = false;

  do {

    // position is generated randomly
    // coordinates are in range -1.0f ... 1.0f
    newPosition = glm::vec3(
      (float)(2.0 * (rand() / (double)RAND_MAX) - 1.0),
      (float)(2.0 * (rand() / (double)RAND_MAX) - 1.0),
      0.0f
    );
    invalidPosition = pointInSphere(newPosition, gameObjects.car->position, 3.0f*CAR_SIZE);

  } while (invalidPosition == true);

  return newPosition;
}

/**
 * @brief Initial game setup. The game restarts and initializes everything.
 * 
 */
void restartGame(void) {
  cleanUpObjects();

  initConfigurableGround();

  gameState.elapsedTime = 0.001f * (float)glutGet(GLUT_ELAPSED_TIME); // milliseconds => seconds

  gameObjects.initializeGameWorld(false, gameState.elapsedTime);
  std::cout << "Objects initialized.." << std::endl;

  if(gameState.camera != FOLLOW) {
    gameState.camera = FOLLOW;
    glutPassiveMotionFunc(NULL);
  }

  insertBlueFire(glm::vec3(0, 2, 1.05), false);

  // reset key map
  for(int i=0; i<KEYS_COUNT; i++)
    gameState.keyMap[i] = false;

  gameState.gameOver = false;
}

BannerObject* createBanner(void) {
  BannerObject* newBanner = new BannerObject;
 
  newBanner->size = BANNER_SIZE;
  newBanner->position = glm::vec3(0.0f, 0.0f, 0.0f);
  newBanner->direction = glm::vec3(0.0f, 1.0f, 0.0f);
  newBanner->speed = 1.0f;
  newBanner->size = 1.0f;

  newBanner->destroyed = false;

  newBanner->startTime = gameState.elapsedTime;
  newBanner->currentTime = newBanner->startTime;

  return newBanner;
}

/**
 * @brief Function to compute camera position for following camera type.
 * 
 * @return glm::vec3 
 */
glm::vec3 computeFollowCameraPosition() {
  return glm::vec3(
    gameObjects.car->position.x - 0.16f * gameObjects.car->direction.x,
    gameObjects.car->position.y - 0.16f * gameObjects.car->direction.y,
    gameObjects.car->position.z + 0.046f
  );
}

glm::vec3 computeFollowCameraUpVector(glm::vec3 cameraDirection) {
  glm::vec3 helpVector = glm::normalize(glm::vec3(
    cameraDirection.y,
    -cameraDirection.x,
    0.0f
  ));
  
  glm::vec3 upVector = glm::normalize(glm::cross(helpVector, cameraDirection));
  glm::vec3 rotationAxis = glm::cross(cameraDirection, glm::vec3(0.0f, 0.0f, 1.0f));
  
  glm::mat4 cameraTransform = glm::rotate(
    glm::mat4(1.0f), 
    glm::radians(glm::dot(cameraDirection, glm::vec3(1.0, 0.0f, 0.0f))), 
    rotationAxis
  );

  return glm::normalize(glm::vec3(cameraTransform * glm::vec4(upVector, 0.0f)));
}

glm::vec3 computeFreeCameraDirection() {
  glm::vec3 rotatedInXZ = glm::vec3(
    sin(glm::radians(gameObjects.freeCamera->verticalAngle)),
    0.0f,
    cos(glm::radians(gameObjects.freeCamera->verticalAngle))
  );

  float viewAngle = glm::radians(gameObjects.freeCamera->viewAngle);
  return glm::normalize(glm::vec3(
    rotatedInXZ.x * cos(viewAngle) - rotatedInXZ.y * sin(viewAngle),
    rotatedInXZ.x * sin(viewAngle) + rotatedInXZ.y * cos(viewAngle),
    rotatedInXZ.z
    )
  );
}

glm::vec3 computeFreeCameraUpVector(const glm::vec3& direction) {
  glm::vec3 helpVector = glm::normalize(glm::vec3(
    direction.y,
    -direction.x,
    0.0f
  ));

  return glm::normalize(glm::cross(helpVector, direction));
}

glm::vec3 computeEpitrochoidCameraDirection() {
  glm::vec3 rotatedInXZ = glm::vec3(
    sin(glm::radians(gameObjects.epitrochoidCamera->verticalAngle)),
    0.0f,
    cos(glm::radians(gameObjects.epitrochoidCamera->verticalAngle))
  );

  float viewAngle = glm::radians(gameObjects.epitrochoidCamera->viewAngle);
  return glm::normalize(glm::vec3(
    rotatedInXZ.x * cos(viewAngle) - rotatedInXZ.y * sin(viewAngle),
    rotatedInXZ.x * sin(viewAngle) + rotatedInXZ.y * cos(viewAngle),
    rotatedInXZ.z
    )
  );
}

glm::vec3 computeEpitrochoidCameraUpVector(const glm::vec3& direction) {
  glm::vec3 helpVector = glm::normalize(glm::vec3(
    direction.y,
    -direction.x,
    0.0f
  ));

  return glm::normalize(glm::cross(helpVector, direction));
}

/**
 * @brief Switches to moving camera so the game can switch cameras fluently
 * 
 */
void switchToFreeCamera() {
  glm::vec3 currentCameraPosition;
  glm::vec3 currentCameraDirection;
  glm::vec3 currentCameraUpVector;
  
  // -90 degrees vector for computing the upvector of static cameras
  glm::vec3 helpVector;

  switch(gameState.camera) {
    case STATIC1:
      currentCameraPosition = glm::vec3(5.0f, -10.0f, 4.0f);
      currentCameraDirection = glm::normalize(glm::vec3(-4.0f, 5.0f, -3.0f));
      
      helpVector = glm::normalize(glm::vec3(
        currentCameraDirection.y,
        -currentCameraDirection.x,
        0.0f
      ));
      
      currentCameraUpVector = glm::cross(helpVector, currentCameraDirection);
      break;
    case STATIC2:
      currentCameraPosition = glm::vec3(5.0f, -10.0f, 4.0f);
      currentCameraDirection = glm::normalize(glm::vec3(-4.0f, 5.0f, -3.0f));
      
      helpVector = glm::normalize(glm::vec3(
        currentCameraDirection.y,
        -currentCameraDirection.x,
        0.0f
      ));
      
      currentCameraUpVector = glm::cross(helpVector, currentCameraDirection);
      break;
    case FOLLOW: 
      currentCameraPosition = computeFollowCameraPosition();
      currentCameraDirection = glm::normalize(glm::vec3(
        gameObjects.car->direction.x,
        gameObjects.car->direction.y,
        gameObjects.car->direction.z - 0.2f
      ));

      currentCameraUpVector = computeFollowCameraUpVector(currentCameraDirection);
      break;
    case EPITROCHOID:
      currentCameraPosition = gameObjects.epitrochoidCamera->position;
      currentCameraDirection = computeEpitrochoidCameraDirection();
      currentCameraUpVector = computeEpitrochoidCameraUpVector(currentCameraDirection);
      break;
    case TOP:
    default:
      currentCameraPosition = glm::vec3(0.0f, 0.0f, 10.0f);
      currentCameraDirection = glm::vec3(0.0f, 0.0f, -1.0f);
      currentCameraUpVector = glm::vec3(0.0f, 1.0f, 0.0f);
      break;
  }

  gameState.lastCameraPosition = currentCameraPosition;
  gameState.lastCameraDirection = currentCameraDirection;
  gameState.lastCameraUpVector = currentCameraUpVector;
  gameState.lastCameraLeaveTime = gameState.elapsedTime;

  gameState.camera = MOVING;
}

/**
 * @brief Returns current camera position in the game world
 * 
 */
glm::vec3 getCameraPosition(Camera type) {
  glm::vec3 position;
  float paramT;
  switch(type) {
    case FOLLOW:
      return computeFollowCameraPosition();
    case FREE:
      return gameObjects.freeCamera->position;
    case STATIC1:
      return glm::vec3(5.0f, -10.0f, 4.0f);
    case STATIC2:
      return glm::vec3(-8.0f, 9.0f, 4.0f);
    case MOVING:
      paramT = gameState.elapsedTime - gameState.lastCameraLeaveTime;
      return std::get<0>(transitionCameraPosition(gameState.lastCameraPosition, gameObjects.freeCamera->position, paramT));
    case EPITROCHOID:
      return gameObjects.epitrochoidCamera->position;
    case TOP:
    default:
      return glm::vec3(0.0f, 0.0f, 10.0f);
  }
}

/**
 * @brief Get the View Matrix object
 * 
 * @param camera 
 * @return glm::mat4 
 */
std::tuple<glm::mat4, glm::mat4> getViewMatrices(Camera camera) {
  glm::mat4 viewMatrix;
  glm::mat4 projectionMatrix;
  glm::vec3 cameraPosition;
  glm::vec3 cameraUpVector;
  glm::vec3 cameraCenter;
  glm::vec3 cameraViewDirection;
  glm::vec3 rotationAxis;
  glm::mat4 cameraTransform;
  glm::vec3 helpVector;
  glm::vec3 verticalRotationAxis;

  // variables for moving camera case
  float paramT;
  glm::vec3 freeCamDirection;
  std::tuple<glm::vec3, Camera> positionAndCamera;
  
  switch(camera) {
    case FOLLOW:
      cameraPosition = computeFollowCameraPosition();
      cameraCenter;

      cameraViewDirection = glm::normalize(glm::vec3(
        gameObjects.car->direction.x,
        gameObjects.car->direction.y,
        gameObjects.car->direction.z - 0.2f
      ));

      cameraUpVector = computeFollowCameraUpVector(cameraViewDirection);
      cameraCenter = cameraPosition + cameraViewDirection;

      viewMatrix = glm::lookAt(
        cameraPosition,
        cameraCenter,
        cameraUpVector
      );

      projectionMatrix = glm::perspective(glm::radians(60.0f), gameState.windowWidth/(float)gameState.windowHeight, 0.1f, 10.0f);
      break;
    case FREE:
      cameraPosition = gameObjects.freeCamera->position;
      cameraViewDirection = computeFreeCameraDirection();

      cameraUpVector = computeFreeCameraUpVector(cameraViewDirection);
      cameraCenter = cameraPosition + cameraViewDirection;

      viewMatrix = glm::lookAt(
        cameraPosition,
        cameraCenter, 
        cameraUpVector
      );

      projectionMatrix = glm::perspective(glm::radians(60.0f), gameState.windowWidth/(float)gameState.windowHeight, 0.1f, 10.0f);
      break;
    case STATIC1:
      cameraPosition = glm::vec3(5.0f, -10.0f, 4.0f);
      cameraViewDirection = glm::normalize(glm::vec3(-4.0f, 5.0f, -3.0f));
      
      helpVector = glm::normalize(glm::vec3(
        cameraViewDirection.y,
        -cameraViewDirection.x,
        0.0f
      ));
      
      cameraUpVector = glm::cross(helpVector, cameraViewDirection);
      cameraCenter = cameraPosition + cameraViewDirection;

      viewMatrix = glm::lookAt(
        cameraPosition,
        cameraCenter,
        cameraUpVector
      );

      projectionMatrix = glm::perspective(glm::radians(60.0f), gameState.windowWidth/(float)gameState.windowHeight, 0.1f, 10.0f);
      break;
    case STATIC2:
      cameraPosition = glm::vec3(-8.0f, 9.0f, 4.0f);
      cameraViewDirection = glm::vec3(1.0f, -0.5f, -1.0f);
      
      helpVector = glm::normalize(glm::vec3(
        cameraViewDirection.y,
        -cameraViewDirection.x,
        0.0f
      ));
      
      cameraUpVector = glm::cross(helpVector, cameraViewDirection);
      cameraCenter = cameraPosition + cameraViewDirection;

      viewMatrix = glm::lookAt(
        cameraPosition,
        cameraCenter,
        cameraUpVector
      );

      projectionMatrix = glm::perspective(glm::radians(60.0f), gameState.windowWidth/(float)gameState.windowHeight, 0.1f, 10.0f);
      break;
    case EPITROCHOID:
      cameraPosition = gameObjects.epitrochoidCamera->position;
      cameraViewDirection = computeEpitrochoidCameraDirection();
      cameraUpVector = computeEpitrochoidCameraUpVector(cameraViewDirection);

      cameraCenter = cameraPosition + cameraViewDirection;

      viewMatrix = glm::lookAt(
        cameraPosition,
        cameraCenter, 
        cameraUpVector
      );

      projectionMatrix = glm::perspective(glm::radians(60.0f), gameState.windowWidth/(float)gameState.windowHeight, 0.1f, 10.0f);
      break;
    case MOVING:
      paramT = gameState.elapsedTime - gameState.lastCameraLeaveTime;
      freeCamDirection = computeFreeCameraDirection();
      positionAndCamera = transitionCameraPosition(gameState.lastCameraPosition, gameObjects.freeCamera->position, paramT);

      cameraPosition = std::get<0>(positionAndCamera);
      cameraViewDirection = transitionCameraDirection(gameState.lastCameraDirection, freeCamDirection, paramT);
      
      cameraUpVector = transitionCameraUpVector(gameState.lastCameraUpVector, computeFreeCameraUpVector(freeCamDirection), paramT);

      cameraCenter = cameraPosition + cameraViewDirection;

      viewMatrix = glm::lookAt(
        cameraPosition,
        cameraCenter,
        cameraUpVector
      );

      gameState.camera = std::get<1>(positionAndCamera);

      projectionMatrix = glm::perspective(glm::radians(60.0f), gameState.windowWidth/(float)gameState.windowHeight, 0.1f, 10.0f);
      break;
    case TOP:
    default:
      viewMatrix = glm::lookAt(
        glm::vec3(0.0f, 0.0f, 1.0f),
        glm::vec3(0.0f, 0.0f, 0.0f),
        glm::vec3(0.0f, 1.0f, 0.0f)
      );
      projectionMatrix = glm::ortho(
        -SCENE_WIDTH, SCENE_WIDTH,
        -SCENE_HEIGHT, SCENE_HEIGHT,
        -10.0f*SCENE_DEPTH, 10.0f*SCENE_DEPTH
      );
      break;
  }

  return std::forward_as_tuple(viewMatrix, projectionMatrix);
}

/**
 * @brief Main drawing function.
 * 
 */
void drawWindowContents() {
  // setup parallel projection
  glm::mat4 orthoProjectionMatrix = glm::ortho(
    -SCENE_WIDTH, SCENE_WIDTH,
    -SCENE_HEIGHT, SCENE_HEIGHT,
    -10.0f*SCENE_DEPTH, 10.0f*SCENE_DEPTH
  );
  // static viewpoint - top view
  glm::mat4 orthoViewMatrix = glm::lookAt(
    glm::vec3(0.0f, 0.0f, 1.0f),
    glm::vec3(0.0f, 0.0f, 0.0f),
    glm::vec3(0.0f, 1.0f, 0.0f)
  );
  
  // setup camera & projection transform
  glm::mat4 viewMatrix;
  glm::mat4 projectionMatrix;
  glm::vec3 currentCameraPosition = getCameraPosition(gameState.camera);

  std::tuple<glm::mat4, glm::mat4> matrices = getViewMatrices(gameState.camera);
  viewMatrix = std::get<0>(matrices); 
  projectionMatrix = std::get<1>(matrices);

  glUseProgram(shaderProgram.program);
  glUniform1f(shaderProgram.timeLocation, gameState.elapsedTime);
  
  glUniform3fv(shaderProgram.reflectorPositionLocation, 1, glm::value_ptr(gameObjects.car->position));
  glUniform3fv(shaderProgram.reflectorDirectionLocation, 1, glm::value_ptr(gameObjects.car->direction));
  glUniform1i(shaderProgram.transformationCheckLocation, 0);
  glUseProgram(0);

  // draw skybox
  drawSkybox(currentCameraPosition, viewMatrix, projectionMatrix, gameState.elapsedTime, gameState.day); 
  
  // Draw car
  // find normal vector and height of the ground
  std::tuple<glm::vec3, glm::vec3, glm::vec3> nearestPoints = getNearestGroundVertices(gameObjects.car->position, gameState.configGround);
  std::tuple<glm::vec3, float> normalAndHeight = getNormalAndHeight(gameObjects.car->position, nearestPoints);

  drawCarObject(
    gameObjects.car, 
    currentCameraPosition,
    viewMatrix, 
    projectionMatrix, 
    std::get<0>(normalAndHeight), 
    std::get<1>(normalAndHeight), 
    gameState.reflectorOn,
    gameState.drawHitboxes
  );


  // draw simpleCar
  // firstly find normal and height thanks to the nearest vertices
  nearestPoints = getNearestGroundVertices(gameObjects.simpleCar->position, gameState.configGround);
  normalAndHeight = getNormalAndHeight(gameObjects.simpleCar->position, nearestPoints);
  // now draw CarObject
  drawCarObject(
    gameObjects.simpleCar, 
    currentCameraPosition, 
    viewMatrix, 
    projectionMatrix, 
    std::get<0>(normalAndHeight), 
    std::get<1>(normalAndHeight),
    gameState.reflectorOn, 
    gameState.drawHitboxes
  );


  // draw simpleCar2
  // firstly find normal and height thanks to the nearest vertices
  nearestPoints = getNearestGroundVertices(gameObjects.simpleCar2->position, gameState.configGround);
  normalAndHeight = getNormalAndHeight(gameObjects.simpleCar2->position, nearestPoints);
  // now draw CarObject
  drawCarObject(
    gameObjects.simpleCar2, 
    currentCameraPosition, 
    viewMatrix, 
    projectionMatrix, 
    std::get<0>(normalAndHeight), 
    std::get<1>(normalAndHeight),
    gameState.reflectorOn, 
    gameState.drawHitboxes
  );


  //draw ground
  if(gameState.drawGround) {
    drawGround(
      currentCameraPosition, 
      viewMatrix, 
      projectionMatrix,
      gameState.reflectorOn, 
      gameState.configGround
    );
  }

  // draw billboard
  nearestPoints = getNearestGroundVertices(gameObjects.billboards->position, gameState.configGround);
  normalAndHeight = getNormalAndHeight(gameObjects.billboards->position, nearestPoints);
  drawAnyObject(
    gameObjects.billboards, 
    currentCameraPosition, 
    viewMatrix, 
    projectionMatrix, 
    std::get<1>(normalAndHeight),
    gameState.reflectorOn, 
    true, 
    gameState.drawHitboxes
  );

  // draw buildings
  glEnable(GL_STENCIL_TEST);
  glStencilOp(GL_KEEP, GL_KEEP, GL_REPLACE);
  CHECK_GL_ERROR(); 
  int id = -1;
  std::list<Object*>::iterator iter;
  for(iter = gameObjects.buildings.begin(); iter != gameObjects.buildings.end(); iter++) { 
    ++id;
    glStencilFunc(GL_ALWAYS, id + 1, -1);

    Object* building = (Object*)(*iter);
    building->id = id + 1;
    nearestPoints = getNearestGroundVertices(building->position, gameState.configGround);
    normalAndHeight = getNormalAndHeight(building->position, nearestPoints);

    if(id < 10 && gameState.useCustomMats)
      drawAnyObjectCM(
        building, 
        currentCameraPosition, 
        viewMatrix, 
        projectionMatrix, 
        std::get<1>(normalAndHeight),
        id,
        gameState.reflectorOn
      );
    else  
      drawAnyObject(
        building, 
        currentCameraPosition, 
        viewMatrix, 
        projectionMatrix, 
        std::get<1>(normalAndHeight), 
        false,
        gameState.reflectorOn,
        gameState.drawHitboxes
      );
  }
  glDisable(GL_STENCIL_TEST);
  glDisable(GL_DEPTH_TEST);
  CHECK_GL_ERROR();

  // draw revolver and crosshair
  if (gameState.camera == FREE) {
    glEnable(GL_DEPTH_TEST);
    gameObjects.revolver->position = gameObjects.freeCamera->position;
    gameObjects.revolver->direction = computeFreeCameraDirection();
    drawRevolver(
      gameObjects.revolver,
      currentCameraPosition,
      viewMatrix, 
      projectionMatrix,
      computeFreeCameraUpVector(gameObjects.revolver->direction),
      gameObjects.freeCamera->viewAngle,
      gameObjects.freeCamera->verticalAngle,
      gameState.reflectorOn
    );

    drawCrosshair(orthoViewMatrix, orthoProjectionMatrix);
  }


  //drawExplosions
  for(GameObjectsList::iterator it = gameObjects.explosions.begin(); it != gameObjects.explosions.end(); ++it) {
    ExplosionObject* explosion = (ExplosionObject *)(*it);
    drawExplosion(explosion, currentCameraPosition, viewMatrix, projectionMatrix);
  }
  glEnable(GL_DEPTH_TEST);
  CHECK_GL_ERROR();

  // draw BlueFires
  for(GameObjectsList::iterator it = gameObjects.blueFires.begin(); it != gameObjects.blueFires.end(); ++it) {
    BlueFireObject* blueFire = (BlueFireObject *)(*it);
    drawBlueFire(blueFire, currentCameraPosition, viewMatrix, projectionMatrix);
  }
  glEnable(GL_DEPTH_TEST);
  CHECK_GL_ERROR();

  // draw game over banner
  if(gameState.gameOver == true) {
    // draw game over banner
    if(gameObjects.bannerObject != NULL)
      drawBanner(gameObjects.bannerObject, orthoViewMatrix, orthoProjectionMatrix);
      drawText("game", TOP_CHARACTER_SIZE, glm::vec3(-7.0, 6.0f, 0.0f), orthoViewMatrix, orthoProjectionMatrix);
      drawText("over", BOTTOM_CHARACTER_SIZE, glm::vec3(5.0, -4.0f, 0.0f), orthoViewMatrix, orthoProjectionMatrix);
      drawText("#ubuntu", BOTTOM_CHARACTER_SIZE, glm::vec3(-8.0, 8.0f, 0.0f), orthoViewMatrix, orthoProjectionMatrix);
      glDisable(GL_DEPTH_TEST);
      CHECK_GL_ERROR();
  }

  // draw Text
  for(GameObjectsList::iterator it = gameObjects.bottomTexts.begin(); it != gameObjects.bottomTexts.end(); it++) {
    TextObject* text = (TextObject*)(*it);
    printf("drawing text %s", text->text);
    drawText(text->text, text->size, text->position, orthoViewMatrix, orthoProjectionMatrix);
  }
  CHECK_GL_ERROR();

  drawHUD(gameObjects.car->healthPoints, orthoViewMatrix, orthoProjectionMatrix);
  glEnable(GL_DEPTH_TEST);
  CHECK_GL_ERROR();
}

void createMenu()
 {
  gameState.dispMenu = false;//  todo
 }

// Called to update the display. You should call glutSwapBuffers after all of your
// rendering to display what you rendered.
void displayCallback() {
  GLbitfield mask = GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT;
  mask |= GL_STENCIL_BUFFER_BIT;

  glClear(mask);
	
  if(gameState.dispMenu == true) {
    // put all the menu functions in one nice procedure
    createMenu();
    // set the clearcolor and the callback
    glClearColor(0.0,0.0,0.0,0.0);
  }

  drawWindowContents();

  glutSwapBuffers();
}

// Called whenever the window is resized. The new window size is given, in pixels.
// This is an opportunity to call glViewport or glScissor to keep up with the change in size.
void reshapeCallback(int newWidth, int newHeight) {

  gameState.windowWidth = newWidth;
  gameState.windowHeight = newHeight;

  glViewport(0, 0, (GLsizei) newWidth, (GLsizei) newHeight);
}

void updateObjects(float elapsedTime) {
  float damage = 0;

  // update car
  float timeDelta = elapsedTime - gameObjects.car->currentTime;
  gameObjects.car->currentTime = elapsedTime;
  gameObjects.car->position += timeDelta * gameObjects.car->speed * gameObjects.car->direction;

  // check the new position and wrap it if it is necessary
  std::tuple<glm::vec3, bool> boundStatus = checkBounds(gameObjects.car->position, gameObjects.car->size);
  if(std::get<1>(boundStatus))
    damage += 5.0f;
  gameObjects.car->position = std::get<0>(boundStatus);
  
  if(gameObjects.car->healthPoints <= 0.0f)
    gameState.gameOver = true;


  // update simple car
  gameObjects.simpleCar->currentTime = elapsedTime;
  float curveParamT = gameObjects.simpleCar->speed * (gameObjects.simpleCar->currentTime - gameObjects.simpleCar->startTime);

  gameObjects.simpleCar->position = gameObjects.simpleCar->initPosition + evaluateClosedCurve(curveData, curveSize, curveParamT);
  glm::vec3 newCarDirection =  glm::normalize(evaluateClosedCurve_1stDerivative(curveData, curveSize, curveParamT));
  gameObjects.simpleCar->viewAngle = acos(glm::dot(glm::vec3(1.0, 0.0, 0.0), newCarDirection)) * 180 / PI;
  gameObjects.simpleCar->direction = newCarDirection;

  // check the new position and wrap it if it is necessary
  boundStatus = checkBounds(gameObjects.simpleCar->position, gameObjects.simpleCar->size);
  gameObjects.simpleCar->position = std::get<0>(boundStatus);
  


  // update simple car 
  gameObjects.simpleCar2->currentTime = elapsedTime;
  curveParamT = gameObjects.simpleCar2->speed * (gameObjects.simpleCar2->currentTime - gameObjects.simpleCar2->startTime);

  gameObjects.simpleCar2->position = gameObjects.simpleCar2->initPosition + calculateParametricCurvePoint(curveParamT);  
  newCarDirection =  glm::normalize(calculateParametricCurvePoint_1stDerivative(curveParamT));
  gameObjects.simpleCar2->viewAngle = acos(glm::dot(glm::vec3(1.0, 0.0, 0.0), newCarDirection)) * 180 / PI;
  gameObjects.simpleCar2->direction = newCarDirection;
  
  // check the new position and wrap it if it is necessary
  boundStatus = checkBounds(gameObjects.simpleCar2->position, gameObjects.simpleCar2->size);
  gameObjects.simpleCar2->position = std::get<0>(boundStatus);

  //check car collisions
  damage += gameObjects.checkCollisions(gameState.configGround);
  


  if(damage > 0.0f) {
    gameObjects.car->healthPoints -= damage;
    std::cout << "Car health at: " << gameObjects.car->healthPoints << std::endl;
    gameObjects.car->speed = -(gameObjects.car->speed / 2);
    gameObjects.car->position -= 0.02f * gameObjects.car->direction;

    // insertExplosion(gameObjects.car->position);
    insertBlueFire(gameObjects.car->position, true);
    // insertBottomText("car");
    // insertText(TextType::BOTTOM_RIGHT, "was");
    // insertText(TextType::BOTTOM_RIGHT, "hit");
  }



  //update epitrochoid camera
  if(gameState.camera == EPITROCHOID) {
    gameObjects.epitrochoidCamera->currentTime = elapsedTime;
    curveParamT = gameObjects.epitrochoidCamera->speed * (gameObjects.epitrochoidCamera->currentTime - gameObjects.epitrochoidCamera->startTime);

    gameObjects.epitrochoidCamera->position = 
      gameObjects.epitrochoidCamera->initPosition + 
      calculateEpitrochoidParametricCurvePoint(
        EPITROCHOID_A, 
        EPITROCHOID_B, 
        EPITROCHOID_C, 
        curveParamT
    );

    glm::vec3 newCameraDirection = glm::normalize(
      calculateEpitrochoidCurvePoint_1stDerivative(
        EPITROCHOID_A, 
        EPITROCHOID_B,
        EPITROCHOID_C,
        curveParamT
      )
    );

    gameObjects.epitrochoidCamera->viewAngle = acos(glm::dot(glm::vec3(1.0, 0.0, 0.0), newCameraDirection)) * 180 / PI;
    gameObjects.epitrochoidCamera->direction = newCameraDirection;
  }



  // update explosion billboards
  GameObjectsList::iterator it = gameObjects.explosions.begin();
  while(it != gameObjects.explosions.end()) {
    ExplosionObject* explosion = (ExplosionObject*)(*it);

    // update explosion
    explosion->currentTime = elapsedTime;

    if(explosion->currentTime > explosion->startTime + explosion->textureFrames*explosion->frameDuration)
      explosion->destroyed = true;

    if(explosion->destroyed == true) {
      it = gameObjects.explosions.erase(it);
    }
    else {
      ++it;
    }
  }

  // update blueFires billboards
  it = gameObjects.blueFires.begin();
  while(it != gameObjects.blueFires.end()) {
    BlueFireObject* blueFire = (BlueFireObject*)(*it);

    // update blueFire
    blueFire->currentTime = elapsedTime;

    if(blueFire->currentTime > (blueFire->startTime + 5 * blueFire->textureFrames * blueFire->frameDuration))
      blueFire->destroyed = true;

    if(blueFire->destroyed == true && blueFire->destroyable == true) {
      it = gameObjects.blueFires.erase(it);
    }
    else {
      ++it;
    }
  }


  // // update buildings
  it = gameObjects.buildings.begin();
  while(it != gameObjects.buildings.end()) {
    Object* building = (Object*)(*it);
    building->currentTime = elapsedTime;
    ++it;
  }


  // // update billboard
  gameObjects.billboards->currentTime = elapsedTime;


  // // update revolver
  gameObjects.revolver->currentTime = elapsedTime;


  // update texts 
  it = gameObjects.bottomTexts.begin();
  while(it != gameObjects.bottomTexts.end()) {
    TextObject* text = (TextObject*)(*it);

    text->currentTime = elapsedTime;

    if((*text).expired() || (*text).position.y >= 10.0f) {
      it = gameObjects.bottomTexts.erase(it);
    }
    else {
      ++it;
    }
  }
  it = gameObjects.topTexts.begin();
  while(it != gameObjects.topTexts.end()) {
    TextObject* text = (TextObject*)(*it);

    text->currentTime = elapsedTime;

    if((*text).expired() || (*text).position.y >= 8.0f) {
      it = gameObjects.topTexts.erase(it);
    }
    else {
      ++it;
    }
  }
}

/**
 * @brief Moves free camera in the direction of looking.
 * 
 * @param speed 
 */
void moveCameraForward(float speed) {
  gameObjects.freeCamera->position += speed * computeFreeCameraDirection();
}

/**
 * @brief Moves free camera to the side in XY plane.
 * 
 * @param speed 
 */
void moveCameraSide(float speed) {
  glm::vec3 direction = computeFreeCameraDirection();

  if (speed > 0) {
    gameObjects.freeCamera->position += speed * glm::cross(
      direction,
      computeFreeCameraUpVector(direction)
    );
  }
  else if (speed < 0) {
    gameObjects.freeCamera->position += speed * glm::cross(
      direction,
      computeFreeCameraUpVector(direction)
    );
  }
}

void moveCameraUp(float speed) {
  gameObjects.freeCamera->position.z += speed; 
}

void rotateCameraAroundZ(float angle) {
  gameObjects.freeCamera->viewAngle -= angle;
  if(gameObjects.freeCamera->viewAngle > 360.0f) 
    gameObjects.freeCamera->viewAngle -= 360.0f;
  else if(gameObjects.freeCamera->viewAngle < 0.0f)
    gameObjects.freeCamera->viewAngle += 360.0f;
}

void rotateCameraVertically(float angle) {
  gameObjects.freeCamera->verticalAngle -= angle;
  if(gameObjects.freeCamera->verticalAngle >= 175.0f)
    gameObjects.freeCamera->verticalAngle = 174.0f;
  else if(gameObjects.freeCamera->verticalAngle <= 5.0f)
    gameObjects.freeCamera->verticalAngle = 6.0f;
}

// Callback responsible for the scene update
void timerCallback(int) {

  // update scene time
  gameState.elapsedTime = 0.001f * (float)glutGet(GLUT_ELAPSED_TIME); // milliseconds => seconds

  // call appropriate actions according to the currently pressed keys in key map
  // (combinations of keys are supported but not used in this implementation)
  if(gameState.camera != FREE) {
    if(gameState.keyMap[KEY_RIGHT_ARROW] == true)
      turnCarRight(CAR_VIEW_ANGLE_DELTA_FORCE);

    if(gameState.keyMap[KEY_LEFT_ARROW] == true)
      turnCarLeft(CAR_VIEW_ANGLE_DELTA_FORCE);

    if(gameState.keyMap[KEY_UP_ARROW] == true)
        if(!gameObjects.car->isBreaking)
          increaseCarSpeed();

    if(gameState.keyMap[KEY_DOWN_ARROW] == true)
        if (!gameObjects.car->isBreaking)
          decreaseCarSpeed(CAR_SPEED_INCREMENT * 0.8f);      
  } 
  else {
    if(gameState.keyMap[KEY_RIGHT_ARROW] == true)
      moveCameraSide(CAMERA_SPEED * gameObjects.freeCameraSpeed);

    if(gameState.keyMap[KEY_LEFT_ARROW] == true)
      moveCameraSide(-(CAMERA_SPEED * gameObjects.freeCameraSpeed));

    if(gameState.keyMap[KEY_UP_ARROW] == true)
      moveCameraForward(CAMERA_SPEED * gameObjects.freeCameraSpeed);

    if(gameState.keyMap[KEY_DOWN_ARROW] == true)
      moveCameraForward(-(CAMERA_SPEED * gameObjects.freeCameraSpeed));

    if(gameState.keyMap[KEY_SPACE] == true)
      moveCameraUp(CAMERA_SPEED * gameObjects.freeCameraSpeed);

    if(gameState.keyMap[KEY_X] == true)
      moveCameraUp(-(CAMERA_SPEED * gameObjects.freeCameraSpeed));

    if(gameState.keyMap[KEY_E] == true)
      rotateCameraAroundZ(CAMERA_ROTATION_INCREMENT);

    if(gameState.keyMap[KEY_Q] == true)
      rotateCameraAroundZ(-CAMERA_ROTATION_INCREMENT);

    if(gameState.keyMap[KEY_PLUS] == true)
      rotateCameraVertically(CAMERA_ROTATION_INCREMENT);

    if(gameState.keyMap[KEY_MINUS] == true)
      rotateCameraVertically(-CAMERA_ROTATION_INCREMENT);
    
    if(gameState.keyMap[KEY_DIVIDE] == true)
      increaseFreeCameraSpeed();
  
    if(gameState.keyMap[KEY_MULTIPLY] == true)
      decreaseFreeCameraSpeed();

    if(gameState.keyMap[KEY_1] == true) {
      // insertText(TextType::TOP_LEFT, std::string("orthogonal"));
      // insertText(TextType::TOP_LEFT, std::string("camera"));
      gameState.camera = TOP;
    }
    
    if(gameState.keyMap[KEY_2] == true) {
      // insertText(TextType::TOP_LEFT, std::string("follow"));
      // insertText(TextType::TOP_LEFT, std::string("camera"));
      gameState.camera = FOLLOW;
    }

    if(gameState.keyMap[KEY_3] == true && gameState.camera != FREE) {
      // insertText(TextType::TOP_LEFT, std::string("#heart"));
      // insertText(TextType::TOP_LEFT, std::string("free"));
      // insertText(TextType::TOP_LEFT, std::string("camera"));
      switchToFreeCamera();
    }

    if(gameState.keyMap[KEY_4] == true) {
      // insertText(TextType::TOP_LEFT, std::string("static"));
      // insertText(TextType::TOP_LEFT, std::string("one"));
      // insertText(TextType::TOP_LEFT, std::string("camera"));
      gameState.camera = STATIC1;
    }

    if(gameState.keyMap[KEY_5] == true) {
      // insertText(TextType::TOP_LEFT, std::string("static"));
      // insertText(TextType::TOP_LEFT, std::string("two"));
      // insertText(TextType::TOP_LEFT, std::string("camera"));
      gameState.camera = STATIC2;
    }

    if(gameState.keyMap[KEY_6] == true) {
      // insertText(TextType::TOP_LEFT, std::string("epitrochoid"));
      // insertText(TextType::TOP_LEFT, std::string("camera"));
      gameState.camera = EPITROCHOID;
    }
  }

  if((gameState.gameOver == true) && (gameObjects.bannerObject != NULL)) {
    (gameObjects.car->speed > 0.0f) ? gameObjects.car->speed -= (CAR_SPEED_INCREMENT / 2) : gameObjects.car->speed = 0.0f;
    gameObjects.bannerObject->currentTime = gameState.elapsedTime;
  }

  if (gameState.keyMap[KEY_SPACE] == true) {
      gameObjects.car->isBreaking = true;
      useHandBrake(CAR_BRAKE_INCREMENT);
  }
  // update objects in the scene
  updateObjects(gameState.elapsedTime);
  
  // game over? -> create banner with scrolling text "game over"
  if(gameState.gameOver == true) {
    gameState.keyMap[KEY_SPACE] = false;
    if(gameObjects.bannerObject == NULL) {
      // if game over and banner still not created -> create banner
      gameObjects.bannerObject = createBanner();
    }
  }

  // set timeCallback next invocation
  glutTimerFunc(33, timerCallback, 0);

  glutPostRedisplay();
}

// Called when mouse is moving while no mouse buttons are pressed.
void passiveMouseMotionCallback(int mouseX, int mouseY) {
  if(gameState.camera == FREE) {
    if(floor(mouseX) > floor(WINDOW_WIDTH/2)) {
      rotateCameraAroundZ(0.1f * CAMERA_ROTATION_INCREMENT);
    }

    else if(floor(mouseX) < floor(WINDOW_WIDTH/2)) {
      rotateCameraAroundZ(-0.1f * CAMERA_ROTATION_INCREMENT);
    }

    if(floor(mouseY) > floor(WINDOW_HEIGHT/2)) {
      rotateCameraVertically(0.1f * CAMERA_ROTATION_INCREMENT);
    }

    else if(floor(mouseX) < floor(WINDOW_HEIGHT/2)) {
      rotateCameraVertically(-0.1f * CAMERA_ROTATION_INCREMENT);
    }

    // set mouse pointer to the window center
    glutWarpPointer(gameState.windowWidth/2, gameState.windowHeight/2);
    glutPostRedisplay();
  }
}

ModelType chooseRandomModelType() {
  return static_cast<ModelType>(rand() % 14);
}

void cycleCamera() {
  switch(gameState.camera) {
    case TOP: 
      // insertText(TextType::TOP_LEFT, std::string("follow"));
      // insertText(TextType::TOP_LEFT, std::string("camera"));
      gameState.camera = FOLLOW;
      break;
    case FOLLOW:
      printf("switching camera..."); 
      // insertText(TextType::TOP_LEFT, std::string("#heart"));
      // insertText(TextType::TOP_LEFT, std::string("free"));
      // insertText(TextType::TOP_LEFT, std::string("camera"));
      switchToFreeCamera();
      break;
    case FREE: 
      // insertText(TextType::TOP_LEFT, std::string("static"));
      // insertText(TextType::TOP_LEFT, std::string("one"));
      // insertText(TextType::TOP_LEFT, std::string("camera"));
      gameState.camera = STATIC1;
      break;
    case STATIC1: 
      // insertText(TextType::TOP_LEFT, std::string("static"));
      // insertText(TextType::TOP_LEFT, std::string("two"));
      // insertText(TextType::TOP_LEFT, std::string("camera"));
      gameState.camera = STATIC2;
      break;
    case STATIC2: 
      // insertText(TextType::TOP_LEFT, std::string("epitrochoid"));
      // insertText(TextType::TOP_LEFT, std::string("camera"));
      gameState.camera = EPITROCHOID;
      break;
    case MOVING:
      break;
    default:
      // insertText(TextType::TOP_LEFT, std::string("top"));
      // insertText(TextType::TOP_LEFT, std::string("camera"));
      gameState.camera = TOP;
      break;
  }
}

// Called whenever a key on the keyboard was pressed. The key is given by the "keyPressed"
// parameter, which is in ASCII. It's often a good idea to have the escape key (ASCII value 27)
// to call glutLeaveMainLoop() to exit the program.
void keyboardCallback(unsigned char keyPressed, int mouseX, int mouseY) {
  std::cout << " KEY PRESSED = " << int(keyPressed) << std::endl;
  switch(keyPressed) {
    case 27: // escape
#ifndef __APPLE__
      glutLeaveMainLoop();
#else
      exit(0);
#endif
      break;
    case 49:
      gameState.keyMap[KEY_1] = true;
      break;
    case 50:
      gameState.keyMap[KEY_2] = true;
      break;
    case 51:
      gameState.keyMap[KEY_3] = true;
      break;
    case 52:
      gameState.keyMap[KEY_4] = true;
      break;
    case 53:
      gameState.keyMap[KEY_5] = true;
      break;
    case 54:
      gameState.keyMap[KEY_6] = true;
      break;
    case 'w':
      gameState.keyMap[KEY_UP_ARROW] = true;
      break;
    case 's':
      gameState.keyMap[KEY_DOWN_ARROW] = true;
      break;
    case 'a':
      gameState.keyMap[KEY_LEFT_ARROW] = true;
      break;
    case 'd':
      gameState.keyMap[KEY_RIGHT_ARROW] = true;
      break;
    case 'q':
      // rotate camera left
      gameState.keyMap[KEY_Q] = true;
      break;
    case 'e':
      // rotate camera right
      gameState.keyMap[KEY_E] = true;
      break;
    case '+':
      // rotate camera left
      gameState.keyMap[KEY_PLUS] = true;
      break;
    case '-':
      // rotate camera down
      gameState.keyMap[KEY_MINUS] = true;
      break;
    case '*':
      // decrease free camera speed
      gameState.keyMap[KEY_MULTIPLY] = true;
      break;
    case '/':
      // increase free camera speed
      gameState.keyMap[KEY_DIVIDE] = true;
      break;
    case ' ': 
      //use handbrake or go up
      gameState.keyMap[KEY_SPACE] = true;
      break;
    case 'x': 
      //use handbrake or go up
      gameState.keyMap[KEY_X] = true;
      break;
    case 'r': 
      //restart game
      restartGame();
      break;
    case 'h':
      gameState.drawHitboxes = !gameState.drawHitboxes;
      break;
    case 't': 
      // teleport car
      if(gameState.gameOver != true)
        teleport();
      break;
    case 'f': // turn on/off spotlight
      if (gameState.gameOver != true)
        turnOnHeadlights();
      break;
    case 'o':
      gameState.day = !gameState.day;
      break;
    case 'u':
      gameState.configGround = !gameState.configGround;
      break;
    case 'c': 
      // switch camera
      gameState.keyMap[KEY_C] = true;
      cycleCamera();
      break;
    case 'm':
      gameObjects.car->modelType = (gameObjects.car->modelType == CAR) ? chooseRandomModelType() : CAR ;
      std::cout << "Changng model to: " << getModelName(gameObjects.car->modelType) << std::endl;
      break;
    case 'g': // game over
      gameState.gameOver = true;
      break;
    default:
      ; // printf("Unrecognized key pressed\n");
  }
}

// Called whenever a key on the keyboard was released. The key is given by
// the "keyReleased" parameter, which is in ASCII. 
void keyboardUpCallback(unsigned char keyReleased, int mouseX, int mouseY) {
  switch(keyReleased) {
    case 49:
      gameState.keyMap[KEY_1] = false;
      break;
    case 50:
      gameState.keyMap[KEY_2] = false;
      break;
    case 51:
      gameState.keyMap[KEY_3] = false;
      break;
    case 52:
      gameState.keyMap[KEY_4] = false;
      break;
    case 53:
      gameState.keyMap[KEY_5] = false;
      break;
    case 54:
      gameState.keyMap[KEY_6] = false;
      break;
    case ' ':
      gameState.keyMap[KEY_SPACE] = false;
      gameObjects.car->isBreaking = false;
      break;
    case 'x':
      gameState.keyMap[KEY_X] = false;
      break;
    case 'w':
      gameState.keyMap[KEY_UP_ARROW] = false;
      break;
    case 's':
      gameState.keyMap[KEY_DOWN_ARROW] = false;
      break;
    case 'a':
      gameState.keyMap[KEY_LEFT_ARROW] = false;
      gameObjects.car->sideForce = 0.0f;
      break;
    case 'd':
      gameState.keyMap[KEY_RIGHT_ARROW] = false;
      gameObjects.car->sideForce = 0.0f;
      break;
    case 'q':
      gameState.keyMap[KEY_Q] = false;
      break;
    case 'e':
      gameState.keyMap[KEY_E] = false;
      break;
    case '+':
      gameState.keyMap[KEY_PLUS] = false;
      break;
    case '-':
      gameState.keyMap[KEY_MINUS] = false;
      break;
    case '*':
      gameState.keyMap[KEY_MULTIPLY] = false;
      break;
    case '/':
      gameState.keyMap[KEY_DIVIDE] = false;
      decreaseFreeCameraSpeed();
      break;
    default:
      break;
  }
}

// The special keyboard callback is triggered when keyboard function or directional
// keys are pressed.
void specialKeyboardCallback(int specKeyPressed, int mouseX, int mouseY) {

  if(gameState.gameOver == true)
    return;

  switch (specKeyPressed) {
    case GLUT_KEY_RIGHT:
      gameState.keyMap[KEY_RIGHT_ARROW] = true;
      break;
    case GLUT_KEY_LEFT:
      gameState.keyMap[KEY_LEFT_ARROW] = true;
      break;
    case GLUT_KEY_UP:
      gameState.keyMap[KEY_UP_ARROW] = true;
      break;
    case GLUT_KEY_DOWN:
      gameState.keyMap[KEY_DOWN_ARROW] = true;
      break;
    default:
      ; // printf("Unrecognized special key pressed\n");
  }
}

// The special keyboard callback is triggered when keyboard function or directional
// keys are released.
void specialKeyboardUpCallback(int specKeyReleased, int mouseX, int mouseY) {

  if(gameState.gameOver == true)
    return;

  switch (specKeyReleased) {
	case GLUT_KEY_RIGHT:
      gameState.keyMap[KEY_RIGHT_ARROW] = false;
      gameObjects.car->sideForce = 0.0f;
      break;
    case GLUT_KEY_LEFT:
      gameState.keyMap[KEY_LEFT_ARROW] = false;
      gameObjects.car->sideForce = 0.0f;
      break;
    case GLUT_KEY_UP:
      gameState.keyMap[KEY_UP_ARROW] = false;
      break;
    case GLUT_KEY_DOWN:
      gameState.keyMap[KEY_DOWN_ARROW] = false;
      break;
		default:
      ; // printf("Unrecognized special key released\n");
  }
}

void mouseCallback(int buttonPressed, int buttonState, int mouseX, int mouseY ) {

  if( ( buttonPressed == GLUT_LEFT_BUTTON ) && ( buttonState == GLUT_DOWN ) ) {
    unsigned char objectId = 0;
    glReadPixels(
      mouseX, 
      gameState.windowHeight - mouseY - 1, 
      1, 
      1, 
      GL_STENCIL_INDEX, 
      GL_UNSIGNED_BYTE, 
      &objectId
    );

    // the buffer was initially cleared to zeros
    if(objectId == 0) {
   		// background was clicked
      printf("Clicked on background\n");
    }
    else {
      // object was clicked
      printf("Clicked on object with ID: %d\n", (int)objectId);
      ModelType helpModelType = gameObjects.car->modelType;
      for(GameObjectsList::iterator iter = gameObjects.buildings.begin(); iter != gameObjects.buildings.end(); iter++) {
        Object* building = (Object*)(*iter);
        if(building->id == (int)objectId) {
          gameObjects.car->modelType = building->modelType;
          (*building).modelType = helpModelType;
        }
      }

      printf("Switched model car model %s with building model %s.\n ", getModelName(helpModelType), getModelName(gameObjects.car->modelType));
    }
  }
}

// Called after the window and OpenGL are initialized. Called exactly once, before the main loop.
void initializeApplication() {

  // initialize random seed
  srand ((unsigned int)time(NULL));

  // initialize OpenGL
  glClearColor(0.1f, 0.1f, 0.1f, 1.0f);
  glClearStencil(0);  // this is the default value
  glEnable(GL_DEPTH_TEST);

  useLighting = true;

  // initialize shaders
  initializeShaderPrograms();
  initializeHitboxes();
  // create geometry for all models used
  initializeModels();

  gameObjects.car = NULL;
  gameObjects.bannerObject = NULL;

  // test whether the curve segment is correctly computed (tasks 1 and 2)
  testCurve(evaluateCurveSegment, evaluateCurveSegment_1stDerivative);

  restartGame();
}

void finalizeApplication(void) {

  cleanUpObjects();

  delete gameObjects.car;
  gameObjects.car = NULL;

  // delete buffers - space ship, asteroid, missile, ufo, banner, and explosion
  cleanupModels();

  // delete shaders
  cleanupShaderPrograms();
}

int main(int argc, char** argv) {

  // initialize windowing system
  glutInit(&argc, argv);

#ifndef __APPLE__
  glutInitContextVersion(pgr::OGL_VER_MAJOR, pgr::OGL_VER_MINOR);
  glutInitContextFlags(GLUT_FORWARD_COMPATIBLE);

  glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE | GLUT_DEPTH | GLUT_STENCIL);
#else
  glutInitDisplayMode(GLUT_3_2_CORE_PROFILE | GLUT_RGB | GLUT_DOUBLE | GLUT_DEPTH | GLUT_STENCIL);
#endif

  // initial window size
  glutInitWindowSize(WINDOW_WIDTH, WINDOW_HEIGHT);
  glutCreateWindow(WINDOW_TITLE);

  glutDisplayFunc(displayCallback);
  // register callback for change of window size
  glutReshapeFunc(reshapeCallback);
  // register callbacks for keyboard
  glutKeyboardFunc(keyboardCallback);
  glutKeyboardUpFunc(keyboardUpCallback);
  glutSpecialFunc(specialKeyboardCallback);     // key pressed
  glutSpecialUpFunc(specialKeyboardUpCallback); // key released

  glutMouseFunc(mouseCallback);
  glutPassiveMotionFunc(passiveMouseMotionCallback);

  glutTimerFunc(33, timerCallback, 0);

  // initialize PGR framework (GL, DevIl, etc.)
  if(!pgr::initialize(pgr::OGL_VER_MAJOR, pgr::OGL_VER_MINOR))
    pgr::dieWithError("pgr init failed, required OpenGL not supported?");

  initializeApplication();

#ifndef __APPLE__
  glutCloseFunc(finalizeApplication);
#else
  glutWMCloseFunc(finalizeApplication);
#endif

  glutMainLoop();
  
  return 0;
}
