/**
 * @file shader.h
 * @author Ondrej Sakači (sakacond@fel.cvut.cz)
 * @brief Generating shaders based on given parameters
 * @version 0.1
 * @date 2021-06-09
 * 
 * 
 */

#ifndef __SHADER_H
#define __SHADER_H

#include <string>
#include <fstream>
#include <sstream>
#include <iostream>
#include <map>
#include <iostream>
#include <vector>
#include <algorithm>
#include <alloca.h>

#include "pgr.h"

//
// DEFAULT SHADERS
//
// default shaders - color per vertex and matrix multiplication
const std::string colorVertexShaderSrc(
    "#version 140\n"
    "\n"
    "in vec3 position;\n"
    "in vec3 color;\n"
    "uniform mat4 PVMmatrix;\n"
    "smooth out vec4 theColor;\n"
    "\n"
    "void main() {\n"
	  "  gl_Position = PVMmatrix * vec4(position, 1.0);\n"
	  "  theColor = vec4(color, 1.0);\n"
    "}\n"
);

const std::string colorFragmentShaderSrc(
    "#version 140\n"
    "smooth in vec4 theColor;\n"
    "out vec4 outputColor;\n"
    "void main() {\n"
    "  outputColor = theColor;\n"
    "}\n"
);

// each vertex shader receives screen space coordinates and calculates world direction
const std::string skyboxFarPlaneVertexShaderSrc(
  "#version 140\n"
  "\n"
  "uniform mat4 inversePVmatrix;\n"
  "uniform vec3 cameraPosition;\n"
  "uniform float time;\n"
  " \n"
  "in vec2 screenCoord;\n"
  "\n"
  "out vec2 fogCoord_v;\n"
  "out vec3 texCoord_v;\n"
  "out float dist_v;\n"
  "\n"
  "void main() {\n"
  "  vec4 farplaneCoord = vec4(screenCoord, 0.9999, 1.0);\n"
  "  vec4 worldViewCoord = inversePVmatrix * farplaneCoord;\n"
  "  texCoord_v = worldViewCoord.xyz / worldViewCoord.w;\n"
  "  gl_Position = farplaneCoord; \n"
  "\n"
  "  dist_v = abs(cameraPosition.z);\n"
  "\n"
  "  fogCoord_v = vec2(sin(time * 0.2 + screenCoord.x), sin(time * 0.2 + screenCoord.y));\n"
  "}\n"
);

// fragment shader uses interpolated 3D tex coords to sample cube map
const std::string skyboxFarPlaneFragmentShaderSrc(
"#version 140\n"
"\n"
"uniform samplerCube skyboxSampler;\n"
"uniform sampler2D fogSampler;\n"
"uniform bool foggy;\n"
"\n"
"in vec3 texCoord_v;\n"
"in vec2 fogCoord_v;\n"
"in float dist_v;\n"
"out vec4 color_f;\n"
"\n"
"void main() {\n"
"  color_f = texture(skyboxSampler, texCoord_v);\n"
"\n"
"  float fogDensity = 1.2;\n"
"  if (foggy == true) {\n"
"    fogDensity = 2;\n"
"  }\n"
"\n"
"  vec4 fogColor = texture(fogSampler, fogCoord_v);\n"
"\n"
"  float fogBlend;\n"
"  fogBlend = exp(-pow((1 / fogDensity) * dist_v, 2.0));\n"
"  fogBlend = clamp(fogBlend, 0.0, 1.0);\n"
"\n"
"  color_f = fogBlend * fogColor + (1 - fogBlend) * color_f;\n"
"}\n"
);

/**
 * @brief Common shader program
 * 
 */
typedef struct _commonShaderProgram {
  // identifier for the shader program
  GLuint program;          // = 0;
  // vertex attributes locations
  GLint posLocation;       // = -1;
  GLint colorLocation;     // = -1;
  GLint normalLocation;    // = -1;
  GLint texCoordLocation;  // = -1;
  
  // uniforms locations
  GLint PVMmatrixLocation;    // = -1;
  GLint VmatrixLocation;      // = -1;  view/camera matrix
  GLint MmatrixLocation;      // = -1;  modeling matrix
  GLint normalMatrixLocation; // = -1;  inverse transposed Mmatrix
  GLint cameraPositionLocation;

  GLint timeLocation;         // = -1; elapsed time in seconds

  // material 
  GLint diffuseLocation;    // = -1;
  GLint ambientLocation;    // = -1;
  GLint specularLocation;   // = -1;
  GLint shininessLocation;  // = -1;
  
  // textures
  GLint useTextureLocation; // = -1;
  GLint mixTextureLocation;

  // bools
  GLint foggyLocation; 
  GLint transformationCheckLocation;

  GLint texSamplerLocation; // = -1;
  GLint fogSamplerLocation;
  GLint mixSamplerLocation;
  
  // reflector related uniforms
  GLint reflectorPositionLocation;  // = -1; 
  GLint reflectorDirectionLocation; // = -1;
  GLint reflectorOnLocation;
} SCommonShaderProgram;

/**
 * @brief Explosion shader program
 * 
 */
typedef struct ExplosionShaderProgram {
  // identifier for the shader program
  GLuint program;              // = 0;
  // vertex attributes locations
  GLint posLocation;           // = -1;
  GLint texCoordLocation;      // = -1;
  // uniforms locations
  GLint PVMmatrixLocation;     // = -1;
  GLint VmatrixLocation;       // = -1;
  GLint timeLocation;          // = -1;
  GLint texSamplerLocation;    // = -1;
  GLint frameDurationLocation; // = -1;

} ExplosionShaderProgram;


/**
 * @brief  Blue fire shader program definitions
 * 
 */
typedef struct BlueFireShaderProgram {
  // identifier for the shader program
  GLuint program;              // = 0;
  // vertex attributes locations
  GLint posLocation;           // = -1;
  GLint texCoordLocation;      // = -1;
  // uniforms locations
  GLint PVMmatrixLocation;     // = -1;
  GLint VmatrixLocation;       // = -1;
  GLint timeLocation;          // = -1;
  GLint texSamplerLocation;    // = -1;
  GLint frameDurationLocation; // = -1;

} BlueFireShaderProgram;

/**
 * @brief Definitions for crosshair shader program
 * 
 */
typedef struct CrosshairShaderProgram {
  // identifier for the shader program
  GLuint program;              // = 0;
  // vertex attributes locations
  GLint posLocation;           // = -1;
  GLint texCoordLocation;      // = -1;
  // uniforms locations
  GLint PVMmatrixLocation;     // = -1;
  GLint texSamplerLocation;    // = -1;
} CrosshairShaderProgram;

/**
 * @brief Definitions for HUD shader program
 * 
 */
typedef struct HUDShaderProgram {
  // identifier for the shader program
  GLuint program;              // = 0;
  // vertex attributes locations
  GLint posLocation;
  GLint texCoordLocation;      // = -1;
  // uniforms locations
  GLint PVMmatrixLocation;     // = -1;
  GLint texSamplerLocation;    // = -1;
  GLint healthLocation;
} HUDShaderProgram;

/**
 * @brief Definitions for banner program
 * 
 */
typedef struct BannerShaderProgram {
  // identifier for the shader program
  GLuint program;           // = 0;
  // vertex attributes locations
  GLint posLocation;        // = -1;
  GLint texCoordLocation;   // = -1;
  // uniforms locations
  GLint PVMmatrixLocation;  // = -1;
  GLint timeLocation;       // = -1;
  GLint texSamplerLocation; // = -1;
} BannerShaderProgram;

/**
 * @brief Definitions for skybox shader program
 * 
 */
typedef struct SkyboxFarPlaneShaderProgram {
  // identifier for the shader program
  GLuint program;                 // = 0;
  // vertex attributes locations
  GLint screenCoordLocation;      // = -1;
  // uniforms locations
  GLint inversePVmatrixLocation; // = -1;
  GLint skyboxSamplerLocation;    // = -1;
  GLint fogSamplerLocation;
  GLint cameraPositionLocation;
  GLint foggyLocation;
  GLint timeLocation;

  GLint dayLocation;    // = -1;
} SkyboxFarPlaneShaderProgram;

/**
 * @brief definitions for fog shader program
 * 
 */
typedef struct FogShaderProgram {
  GLuint program;

  GLint posLocation;

  GLuint elapsedTimeLocation;

  GLint texSamplerLocation;
  GLint texCoordLocation;
  GLint colorLocation;

  GLint distLocation;

  GLint cameraPositionLocation;
  GLint PVMmatrixLocation;
  GLint VmatrixLocation;
  GLint MmatrixLocation;
} FogShaderProgram;

typedef struct GroundShader {
  GLuint program;

  GLint posLocation;

  GLint grassTexLocation;
  GLint groundTexLocation;

  GLint texCoordLocation;
  
  GLint colorLocation;
  GLint distLocation;

  GLint PVMmatrixLocation;
} GroundShaderProgram;

#endif // __SHADER_H