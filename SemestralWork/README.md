     _              _            _         _                  
    | | __ _ ______| | __ _     / \  _   _| |_ ___  _ __ ___  
 _  | |/ _` |_  / _` |/ _` |   / _ \| | | | __/ _ \| '_ ` _ \ 
| |_| | (_| |/ / (_| | (_| |  / ___ \ |_| | || (_) | | | | | |
 \___/ \__,_/___\__,_|\__,_| /_/   \_\__,_|\__\___/|_| |_| |_|

PGR Semestral project by Ondrej Sakači

Zadanie:
Jazda automobilom v mestskom prostredí. Jednalo sa o voľnejšiu tému, v ktorej som sa rozhodol primárne pracovať s rotáciami a terénom. Interakcie sú umožnené predovšetkým formou kolízií alebo klikaním myšou.
Generovanie terénu môže byť ovplyvnené configuračným súborom.

Ovládanie:
w / šípka hore - akcelerácia automobilu/pohyb voľnej kamery
a / šípka doľava - zmena smeru automobilu/pohyb voľnej kamery
s / šípka dole - deakcelerácia a následné cúvanie automobilu/pohyb kamery
d / šípka doprava - zmena smeru automobilu/pohyb voľnej kamery

medzerník - ručná brzda/zvýšenie výšky voľnej kamery
x - zníženie výšky voľnej kamery
myš - rozhliadanie voľnej kamery
+ / - / q / e - alternatívne rozhliadanie voľnej kamery
ľavé tlačítko myši - interakcia s objektom (výmena modelov)

r - reštart hry
f - prepínanie svetiel
t - teleport automobilu na náhodné miesto
u - zmena modelu zeme
c - cyklické prepínanie kamier
m - zmena modelu (náhodný alebo opäť auto)
g - koniec hry
num 1-6 - výber kamery
