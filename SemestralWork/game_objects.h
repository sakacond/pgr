/**
 * @file game_objects.h
 * @author Ondrej Sakači (sakacond@fel.cvut.cz)
 * @brief 
 * @version 0.1
 * @date 2021-05-13
 * 
 * 
 */

#ifndef __GOBJECTS_H
#define __GOBJECTS_H

#include <stdlib.h>
#include <time.h>
#include <vector>

#include "pgr.h"
#include "data.h"

/**
 * @brief Enum characterizing camera type.
 * 
 */
enum Camera {
  TOP,
  FOLLOW,
  FREE,
  STATIC1,
  STATIC2,
  EPITROCHOID,
  MOVING
};

/**
 * @brief Model enumerator. It says which model the game object ought to use for drawing.
 * 
 */
typedef enum ModelType{
  CAR,
  SIMPLE_CAR,
  BUILDING,
  TOWER01,
  TOWER02,
  TOWER03,
  TOWER04,
  TOWER05,
  TOWER06,
  TOWER07,
  TOWER08,
  TOWER09,
  TOWER10,
  REVOLVER,
  BILLBOARD
} ModelType;

/**
 * @brief Hitbox definition.
 * 
 */
typedef struct HitBox {
  float height; // z axis
  float width;  // x axis
  float length; // y axis
} HitBox;

// GEOMETRY DEFINITION
// geometry is shared among all instances of the same object type
typedef struct _MeshGeometry {

  GLuint        vertexBufferObject;   // identifier for the vertex buffer object
  GLuint        elementBufferObject;  // identifier for the element buffer object
  GLuint        vertexArrayObject;    // identifier for the vertex array object
  unsigned int  numTriangles;         // number of triangles in the mesh
  
  // material
  glm::vec3     ambient;
  glm::vec3     diffuse;
  glm::vec3     specular;
  float         shininess;
  GLuint        texture;
  GLuint        mixTexture;

  float         elapsedTime;
  glm::vec3     position;
} MeshGeometry;

/**
 * @brief This is the container for the holding information about the model.
 * 
 */
typedef struct Model {
  // Object specifications for transformations
  float size;
  std::vector<MeshGeometry> geometry;
  
  // Hitbox
  bool hasHitbox = false;
  MeshGeometry* hitboxGeometry;
  HitBox hitbox;

  public:
    void setHitbox(float height, float width, float length) {    
      this->hitbox.height = height;
      this->hitbox.width = width;
      this->hitbox.length = length;

      this->hasHitbox = true;
    }

} Model;

// OBJECT DEFINITIONS
// parameters of individual objects in the scene (e.g. position, size, speed, etc.)
typedef struct _Object {
    ModelType modelType;

    int id = -1; //-1 non interactive object

    glm::vec3 initPosition;
    glm::vec3 position;
    glm::vec3 direction;

    float speed;
    float size;
    float verticalAngle;
    float viewAngle;

    bool destroyed;

    float startTime;
    float currentTime;

} Object;

typedef struct CarObject : public Object {
  glm::vec3 upVector;

  float sideForce;
  float healthPoints;
  bool isBreaking;
} CarObject;

typedef struct _ExplosionObject : public Object {
  int    textureFrames;
  float  frameDuration;
} ExplosionObject;

typedef struct _BlueFireObject : public Object {
  int textureFrames;
  float frameDuration;
  bool destroyable;
} BlueFireObject;

typedef struct _BillboardObject : public Object {
} BillboardObject;

typedef struct _BannerObject : public Object {
} BannerObject;

/**
 * @brief Not functioning properly, probably string conversion problems.
 * 
 */
typedef struct TextObject : public Object {
  std::string text = "";

  float duration;

  TextObject() =default;

  // Object functions
  float getTransparency() {
    return (this->currentTime - this->startTime) / this->duration;
  }
  bool expired() {
    return (this->currentTime - this->startTime) > this->duration;
  }
  void moveUp(float size) {
    this->position.y += size * 2;
  }
} TextObject;

/**
 * @brief Object for custom light object.
 * 
 */
typedef struct _Light {
  glm::vec3 position;
  glm::vec3 direction;
  
  glm::vec3 ambienceColor;
  glm::vec3 diffuseColor;
  glm::vec3 specularColor;
  
  float intensity;
  bool lightOn;

  bool isDirectionLight(void*) {
    return !(direction.x == direction.y == direction.z == 0); 
  }
} Light;

typedef struct SpotLight : public Light {
  float innerCone;
  float outerCone;
} SpotLight;

/**
 * @brief Object for characterizing material.
 * 
 */
typedef struct _Material {
  glm::vec3 ambient;
  glm::vec3 diffuse;
  glm::vec3 specular;
  float shinines;
  float transparency;
} Material;

/**
 * @brief Definition of the ground object
 * 
 */
typedef struct Ground {
  int fragX;
  int fragY;
  float *vertices;
  glm::vec3 color = glm::vec3(0.0f, 1.0f, 0.0f);

  Ground() {
    this->fragX = GROUND_FRAGMENTS_X;
    this->fragY = GROUND_FRAGMENTS_Y;
    this->vertices = new float [(GROUND_FRAGMENTS_X + 1) * (GROUND_FRAGMENTS_Y + 1) * 3];
  }

  Ground(int x, int y) {
    this->fragX = x;
    this->fragY = y;
    this->vertices = new float [(x + 1) * (y + 1) * 3];
  }

  /**
   * @brief Function for generating semirandomly the vertices of the ground object based on the parameters of the Ground object.
   * 
   */
  void generateVertices() {
    // srand((unsigned) time(NULL));

    float coordX;
    float coordY = GROUND_LIMIT;
    float stepX = 2 * GROUND_LIMIT / this->fragX;
    float stepY = -(2 * GROUND_LIMIT / this->fragY);

    printf("fragmentsX %i fragmentsY %i stepX %f stepY %f.", this->fragY, this->fragY, stepX, stepY);

    for (int i = 0; i < fragY + 1; i++) {
      coordX = -GROUND_LIMIT;

      for (int j = 0; j < fragX + 1; j++) {
        int index = (j * 3) + (i * (fragX + 1) * 3);
        this->vertices[index] = coordX;
        this->vertices[index + 1] = coordY;
        if(j == 0 || j == fragX || i == fragY || i == 0) {
          this->vertices[index + 2] = (2.0f - ((j + i) % 10) / 10);
        }
        else {
          this->vertices[index + 2] = (coordY > -1.0f) ? 1.0f : (0.0f - (rand() % 10)) / 10; //(coordX + coordY > 0) ? -1 : 1 ;
        }
        coordX += stepX;
      }

      coordY += stepY;
    }
  }

  int getTriangleCount() {
    return (this->fragX * this->fragY) * 2;
  }

} Ground;

Ground* generateGround();
Ground* generateGround(int X, int Y);
std::string getCameraName(Camera camera);
std::string getModelName(ModelType type);

//OTHER DEFINITIONS

#endif // __GOBJECTS_H