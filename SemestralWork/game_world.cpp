/**
 * @file game_world.cpp
 * @author Ondrej Sakači (sakacond@fel.cvut.cz)
 * @brief 
 * @version 0.1
 * @date 2021-06-12
 * 
 * 
 */

#include <iostream>
#include <cmath>
#include <tuple>
#include <fstream>
#include <sstream>

#include "pgr.h"
#include "render_stuff.h"
#include "game_world.h"

/**
 * @brief Function for initializing game world (scene).
 * 
 * @param readFile 
 * @param elapsedTime 
 */
void GameObjects::initializeGameWorld(bool readFile, float elapsedTime) {
    // initialize car
    if(this->car == NULL)
        this->car = new CarObject;

    this->car->position = glm::vec3(0.0f, 0.0f, 0.0f);
    this->car->viewAngle = 90.0f; // degrees
    this->car->direction = glm::vec3(cos(glm::radians(this->car->viewAngle)), sin(glm::radians(this->car->viewAngle)), 0.0f);
    this->car->speed = 0.0f;
    this->car->sideForce = 0.0f;
    this->car->isBreaking = false;
    this->car->size = CAR_SIZE;
    this->car->destroyed = false;
    this->car->startTime = elapsedTime;
    this->car->currentTime = this->car->startTime;
    this->car->healthPoints = 200;
    this->car->modelType = CAR;
    
    // initialize simple car
    if(this->simpleCar == NULL)
        this->simpleCar = new CarObject;

    this->simpleCar->position = glm::vec3(-3.0f, -3.0f, 0.0f);
    this->simpleCar->initPosition = glm::vec3(-3.0f, -3.0f, 0.0f);
    this->simpleCar->viewAngle = 0.0f; // degrees
    this->simpleCar->direction = glm::vec3(cos(glm::radians(this->simpleCar->viewAngle)), sin(glm::radians(this->simpleCar->viewAngle)), 0.0f);
    this->simpleCar->speed = 0.5f;
    this->simpleCar->sideForce = 0.0f;
    this->simpleCar->isBreaking = false;
    this->simpleCar->size = CAR_SIZE;
    this->simpleCar->destroyed = false;
    this->simpleCar->startTime = elapsedTime;
    this->simpleCar->currentTime = this->simpleCar->startTime;
    this->simpleCar->healthPoints = 200;
    this->simpleCar->modelType = SIMPLE_CAR;

    // initialize simple car
    if(this->simpleCar2 == NULL)
        this->simpleCar2 = new CarObject;

    this->simpleCar2->position = glm::vec3(0.0f, -6.0f, 0.0f);
    this->simpleCar2->initPosition = glm::vec3(0.0f, -6.0f, 0.0f);
    this->simpleCar2->viewAngle = 0.0f; // degrees
    this->simpleCar2->direction = glm::vec3(cos(glm::radians(this->simpleCar->viewAngle)), sin(glm::radians(this->simpleCar->viewAngle)), 0.0f);
    this->simpleCar2->speed = 0.5f;
    this->simpleCar2->sideForce = 0.0f;
    this->simpleCar2->isBreaking = false;
    this->simpleCar2->size = CAR_SIZE;
    this->simpleCar2->destroyed = false;
    this->simpleCar2->startTime = elapsedTime;
    this->simpleCar2->currentTime = this->simpleCar->startTime;
    this->simpleCar2->healthPoints = 200;
    this->simpleCar2->modelType = SIMPLE_CAR;

    // initialize billboard
    if(this->billboards == NULL)
        this->billboards = new BillboardObject;
    
    this->billboards->position = glm::vec3(1.0f, 1.0f, 0.0f);
    this->billboards->direction = glm::normalize(glm::vec3(-1.0f, -1.0f, 0.0f));
    this->billboards->size = BILLBOARD_SIZE;
    this->billboards->modelType = BILLBOARD;
    this->billboards->currentTime = elapsedTime;

    // initialize buildings
    Object* obj = new Object;
    obj->position = positions[0];
    obj->direction = directions[0];
    obj->modelType = static_cast<ModelType>(2);
    obj->size = HOUSE_SIZE;
    obj->currentTime = elapsedTime;

    this->buildings.push_back(obj);

    for(int model = 0; model < 10; model++) {
        obj = new Object;
        obj->position = positions[model];
        obj->direction = directions[model];
        obj->modelType = static_cast<ModelType>(model + 3);
        obj->size = TOWER_SIZE;
        obj->currentTime = elapsedTime;

        this->buildings.push_back(obj);
    }

    // initialize free camera
    if(this->freeCamera == NULL)
        this->freeCamera = new Object;
    this->freeCamera->position = glm::vec3(0.0f, 0.0f, 1.1f);
    this->freeCamera->direction = glm::vec3(1.0f, 0.0f, 0.0f);
    this->freeCamera->viewAngle = 90.0f;
    this->freeCamera->verticalAngle = 90.0f;

    // initialize free camera
    if(this->epitrochoidCamera == NULL)
        this->epitrochoidCamera = new Object;
    this->epitrochoidCamera->position = glm::vec3(0.0f, 0.0f, 1.3f);
    this->epitrochoidCamera->initPosition = glm::vec3(0.0f, 0.0f, 1.3f);
    this->epitrochoidCamera->direction = glm::vec3(1.0f, 0.0f, 0.0f);
    this->epitrochoidCamera->viewAngle = 0.0f;
    this->epitrochoidCamera->verticalAngle = 100.0f;
    this->epitrochoidCamera->startTime = elapsedTime;
    this->epitrochoidCamera->currentTime = elapsedTime;
    this->epitrochoidCamera->speed = 0.2f;

    // initialize revolver - not an interactable object doesnt have to be paired with id
    if(this->revolver == NULL) 
        this->revolver = new Object;
    
    this->revolver->position = this->freeCamera->position;
    this->revolver->direction = this->freeCamera->direction;
    this->revolver->size = REVOLVER_SIZE;
    this->revolver->modelType = REVOLVER;
    this->revolver->currentTime = elapsedTime;
}

/**
 * @brief Function that checks if the Car object had collided with something. Returns complete damage.
 * 
 * @param configGround 
 * @return float 
 */
float GameObjects::checkCollisions(const bool configGround) {
    float damage = 0.0f;
    std::tuple<glm::vec3, glm::vec3, glm::vec3> nearestPoints = getNearestGroundVertices(this->car->position, configGround);
    glm::vec3 upVector = std::get<0>(getNormalAndHeight(this->car->position, nearestPoints));
    float carAngle = glm::dot(this->car->direction, upVector);

    // check collision with simpleCar
    nearestPoints = getNearestGroundVertices(this->simpleCar->position, configGround);
    glm::vec3 upVector2 = std::get<0>(getNormalAndHeight(this->simpleCar->position, nearestPoints));
    float angle2 = glm::dot(this->simpleCar->direction, upVector);
    
    if(hasCollided(this->car, carAngle, this->simpleCar, angle2))
        damage += 10.0f;

    // check collision with simpleCar2
    nearestPoints = getNearestGroundVertices(this->simpleCar2->position, configGround);
    upVector2 = std::get<0>(getNormalAndHeight(this->simpleCar2->position, nearestPoints));
    angle2 = glm::dot(this->simpleCar2->direction, upVector);
    
    if(hasCollided(this->car, carAngle, this->simpleCar2, angle2))
        damage += 10.0f;

    // check collision with billboard
    if(hasCollided(this->car, carAngle, this->billboards, 0.0f))
        damage += 10;
    
    // check collisions with buildings
    std::list<Object*>::iterator iter;
    for(iter = buildings.begin(); iter != buildings.end(); iter++) {
        if(hasCollided(this->car, carAngle, *iter, 0.0f))
            damage += 10;
    }

    return damage;
}