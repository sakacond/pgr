/**
 * @file render_stuff.h
 * @author Ondrej Sakači (sakacond@fel.cvut.cz)
 * @brief Rendering objects and initializing shaders.
 * @version 0.1
 * @date 2021-06-12
 * 
 * 
 */

#ifndef __RENDER_STUFF_H
#define __RENDER_STUFF_H

#include <iostream>
#include <tuple>
#include <cmath>

#include "pgr.h"
#include "game_world.h"
#include "shader.h"

MeshGeometry createMeshGeometry(aiMesh* mesh, aiMaterial* material, SCommonShaderProgram& shader, const std::string &fileName);

void pushMesh(MeshGeometry& mesh, Object* object);
bool loadModel(const std::string &fileName, SCommonShaderProgram& shader, std::vector<MeshGeometry>* geometryVec);

std::tuple<glm::vec3, bool> checkBounds(const glm::vec3 & position, float objectSize);
bool hasCollided(CarObject* car, float angle1, Object* object, float angle2);

/**
 * @brief Get the Nearest Ground Vertices. Finds nearest vertices of the ground.
 * 
 * @param position 
 * @param configGround 
 * @return std::tuple<glm::vec3, glm::vec3, glm::vec3> 
 */
std::tuple<glm::vec3, glm::vec3, glm::vec3> getNearestGroundVertices(const glm::vec3 &position, const bool configGround);

/**
 * @brief Get the Normal and and the height of the ground in current point. 
 * 
 * @param position 
 * @param nearestPoints 
 * @return std::tuple<glm::vec3, float> 
 */
std::tuple<glm::vec3, float> getNormalAndHeight(glm::vec3 &position, std::tuple<glm::vec3, glm::vec3, glm::vec3> &nearestPoints);

void drawCarObject(Object* car, const glm::vec3& cameraPosition, const glm::mat4& viewMatrix, const glm::mat4& projectionMatrix, const glm::vec3& upVector, float height, bool reflectorOn, bool drawHitboxes);
void drawAnyObject(Object* object, const glm::vec3& cameraPosition, const glm::mat4& viewMatrix, const glm::mat4& projectionMatrix, float height, bool reflectorOn, bool transformation, bool drawHitboxes);
void drawAnyObjectCM(Object* object, const glm::vec3& cameraPosition, const glm::mat4& viewMatrix, const glm::mat4& projectionMatrix, float height, int material, bool reflectorOn);
void drawRevolver(Object* revolver, const glm::vec3& cameraPosition, const glm::mat4& viewMatrix, const glm::mat4& projectionMatrix, const glm::vec3& upVector, float angleX, float angleY, bool reflectorOn);

void drawCrosshair(const glm::mat4 & viewMatrix, const glm::mat4 & projectionMatrix);
void drawText(TextObject* text, const glm::mat4 & viewMatrix, const glm::mat4 & projectionMatrix);
void drawText(const std::string& text, float size, const glm::vec3& position, const glm::mat4 & viewMatrix, const glm::mat4 & projectionMatrix);
void drawHUD(const float health, const glm::mat4 & viewMatrix, const glm::mat4 & projectionMatrix);

void drawFog(const glm::mat4& modelMatrix, const glm::mat4 & viewMatrix, const glm::mat4 & projectionMatrix, float elapsedTime);
// void drawFog(const glm::vec3& cameraPosition, const glm::mat4 & viewMatrix, const glm::mat4 & projectionMatrix, float elapsedTime);
// void drawHitbox(MeshGeometry* geometry);

void drawBillboard(Object* object, const glm::vec3& cameraPosition, const glm::mat4 & viewMatrix, const glm::mat4 & projectionMatrix, bool reflectorOn, bool transformation, float height);
void drawGround(const glm::vec3& cameraPosition, const glm::mat4 & viewMatrix, const glm::mat4 & projectionMatrix, bool reflectorOn, bool configGround);

void drawExplosion(ExplosionObject* explosion, const glm::vec3& cameraPosition, const glm::mat4 & viewMatrix, const glm::mat4 & projectionMatrix);
void drawBlueFire(BlueFireObject* blueFire, const glm::vec3& cameraPosition, const glm::mat4 & viewMatrix, const glm::mat4 & projectionMatrix);
void drawBanner(BannerObject* banner, const glm::mat4 & viewMatrix, const glm::mat4 & projectionMatrix);

void drawSkybox(const glm::vec3& cameraPosition, const glm::mat4 & viewMatrix, const glm::mat4 & projectionMatrix, float elapsedTime, bool day);

void initializeShaderPrograms();
void cleanupShaderPrograms();

void initializeHitboxes();
void initConfigurableGround();
void initializeModels();

void cleanupModels();

#endif // __RENDER_STUFF_H