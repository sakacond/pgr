cmake_minimum_required(VERSION 3.15)
project(semestral)

set(CMAKE_CXX_STANDARD 14)

include_directories(.)

add_executable(semestral
        semestral.cpp
        file_definitions.h
        data.h
        render_stuff.cpp
        render_stuff.h
        spline.cpp
        spline.h
        game_objects.cpp
        game_objects.h
        game_world.cpp
        game_world.h
        shader.h
        )

add_definitions(-Wno-deprecated)

find_package(DevIL REQUIRED)
find_package(assimp REQUIRED)
find_package(GLUT REQUIRED)
find_package(OpenGL REQUIRED)

set(PATH_TO_PGR_FRAMEWORK "/home/ondrej/School/6sem/PGR/pgr-framework")

include_directories(.
        ${CMAKE_EXTRA_GENERATOR_CXX_SYSTEM_INCLUDE_DIRS}
        ${PATH_TO_PGR_FRAMEWORK}/include
)

target_include_directories(
        ${PROJECT_NAME}
        PUBLIC
        ${PATH_TO_PGR_FRAMEWORK}/include
        ${OPENGL_INCLUDE_DIR}
        ${GLUT_INCLUDE_DIR}
        ${IL_INCLUDE_DIR}/..
)

target_link_libraries (
        ${PROJECT_NAME}
        PUBLIC
        ${PATH_TO_PGR_FRAMEWORK}/build/libpgr.a
        ${assimp_DIR}/../../${ASSIMP_LIBRARIES}
        ${IL_LIBRARIES}
        ${OPENGL_LIBRARIES}
        ${GLUT_LIBRARIES}
)
