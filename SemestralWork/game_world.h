/**
 * @file game_world.h
 * @author Ondrej Sakači (sakacond@fel.cvut.cz)
 * @brief Definitions of game world and objects in it.
 * @version 0.1
 * @date 2021-05-13
 * 
 * 
 */

#ifndef __SCENE_H
#define __SCENE_H

#include "pgr.h"
#include "game_objects.h"

#include <list>

/**
 * @brief Positions of buildings in the scene.
 * 
 */
const glm::vec3 positions[] = {
    glm::vec3(-8.0f, 8.0f, 0.0f), // 1
    glm::vec3(-5.0f, 8.0f, 0.0f), // 2
    glm::vec3(0.0f, 8.0f, 0.0f), // 3
    glm::vec3(5.0f, 8.0f, 0.0f), // 4
    glm::vec3(8.0f, 8.0f, 0.0f), // 5
    glm::vec3(-8.0f, 6.0f, 0.0f), // 6
    glm::vec3(-5.0f, 6.0f, 0.0f), // 7
    glm::vec3(0.0f, 6.0f, 0.0f), // 8
    glm::vec3(5.0f, 6.0f, 0.0f), // 9
    glm::vec3(8.0f, 6.0f, 0.0f), // 10
    glm::vec3(7.5f, 3.0f, 0.0f) // 11
};

/**
 * @brief Directions of buildings in the scene.
 * 
 */
const glm::vec3 directions[] = {
    glm::vec3(-1.0f, 0.0f, 0.0f), // 1
    glm::vec3(-2.0f, 1.0f, 0.0f), // 2
    glm::vec3(-3.0f, 3.0f, 0.0f), // 3
    glm::vec3(2.0f, 1.0f, 0.0f), // 4
    glm::vec3(-9.0f, 6.0f, 0.0f), // 5
    glm::vec3(3.77f, 2.0f, 0.0f), // 6
    glm::vec3(2.5f, 7.0f, 0.0f), // 7
    glm::vec3(-9.0f, 5.0f, 0.0f), // 8
    glm::vec3(3.0f, -1.0f, 0.0f), // 9
    glm::vec3(1.0f, -3.0f, 0.0f), // 10
    glm::vec3(-9.0f, 0.0f, 0.0f) // 11
};

typedef std::list<Object*> GameObjectsList;
typedef std::list<Light*> LightsList;

/**
 * @brief Storage of game objects.
 * 
 */
typedef struct GameObjects {
    float freeCameraSpeed = 1.0f;
    
    Object* freeCamera;
    Object* epitrochoidCamera;

    CarObject* car;
    CarObject* simpleCar; //for the curve movement
    CarObject* simpleCar2;

    Object* revolver;
    Object* billboards;
    GameObjectsList buildings;

    LightsList lights;
    GameObjectsList explosions;
    GameObjectsList blueFires;
    
    // texts (some bugs prevent them from being used)
    GameObjectsList bottomTexts;
    GameObjectsList topTexts;

    BannerObject* bannerObject; // NULL;
    
    // Initialization functions
    void initializeGameWorld(bool readFile, float elapsedTime);

    // Game functions
    float checkCollisions(const bool configGround);
} GameObjects;

/**
 * @brief Storage of models and textures.
 * 
 */
typedef struct GameWorldModels {
    Ground* groundVertices;
    Ground* groundVerticesConfigurable;

    MeshGeometry* ground;
    MeshGeometry* groundConfigurable;

    Model simple_car;
    Model car;
    Model building1;
    // Model building2;
    Model tower01;
    Model tower02;
    Model tower03;
    Model tower04;
    Model tower05;
    Model tower06;
    Model tower07;
    Model tower08;
    Model tower09;
    Model tower10;
    Model billboard;
    Model revolver;
    
    Material* materials[10];

    /**
     * @brief Initializer for the custom materials. 
     * 
     */
    void initMaterials() {
        Material* material = new Material;
        material->ambient = glm::vec3(0.6f, 0.0f, 0.2f);
        material->diffuse = glm::vec3(1.0f, 0.0f, 0.0f);
        material->specular = glm::vec3(0.0f, 0.0f, 6.0f);
        material->shinines = 0.6f;
        material->transparency = 0.0f;
        materials[0] = material;
        
        material = new Material;
        material->ambient = glm::vec3(0.0f, 6.0f, 0.3f);
        material->diffuse = glm::vec3(0.0f, 0.0f, 0.0f);
        material->specular = glm::vec3(0.0f, 0.1f, 0.0f);
        material->shinines = 0.1f;
        material->transparency = 0.0f;
        materials[1] = material;
        
        material = new Material;
        material->ambient = glm::vec3(0.0f, 0.0f, 0.7f);
        material->diffuse = glm::vec3(0.0f, 0.0f, 0.1f);
        material->specular = glm::vec3(0.0f, 0.0f, 0.1f);
        material->shinines = 0.3f;
        material->transparency = 0.0f;
        materials[2] = material;

        for(int i = 3; i < 10; i++) {
            Material* material = new Material;
            material->ambient = glm::vec3(i/10.0f, 0.0f, 0.2f);
            material->diffuse = glm::vec3(i/10.0f, (10-i)/10.0f, 0.0f);
            material->specular = glm::vec3(i/10.0f, 1.0f, 0.0f);
            material->shinines = i / 10.0f;
            material->transparency = i / 10.0f;
            materials[i] = material;
        }
    }
} GameWorldModels;

#endif //__SCENE_H