/**
 * @file game_objects.cpp
 * @author Ondrej Sakači (sakacond@fel.cvut.cz)
 * @brief 
 * @version 0.1
 * @date 2021-06-02
 * 
 * 
 */

#include "data.h"
#include "game_objects.h"

Ground* generateGround() {
    Ground* ground = new Ground();

    ground->generateVertices();

    return ground;
}

Ground* generateGround(int x, int y) {
    Ground* ground = new Ground(x, y);

    ground->generateVertices();

    return ground;
}

std::string getCameraName(Camera camera) {
    switch(camera) {
        case TOP:
            return "TOP";
        case FOLLOW:
            return "FOLLOW";
        case FREE:
            return "FREE";
        case STATIC1:
            return "STATIC 1";
        case STATIC2:
            return "STATIC 2";
        default:
            return "Err??";
    }
}

std::string getModelName(ModelType type) {
    switch(type) {
        case CAR:
            return "CAR MODEL";
        case SIMPLE_CAR:
            return "SIMPLE_CAR MODEL";
        case BUILDING:
            return "BUILDING MODEL";
        case TOWER01:
            return "TOWER 1 MODEL";
        case TOWER02:
            return "TOWER 2 MODEL";
        case TOWER03:
            return "TOWER 3 MODEL";
        case TOWER04:
            return "TOWER 4 MODEL";
        case TOWER05:
            return "TOWER 5 MODEL";
        case TOWER06:
            return "TOWER 6 MODEL";
        case TOWER07:
            return "TOWER 7 MODEL";
        case TOWER08:
            return "TOWER 8 MODEL";
        case TOWER09:
            return "TOWER 9 MODEL";
        case TOWER10:
            return "TOWER 10 MODEL";
        case BILLBOARD:
            return "BILLBOARD MODEL";
        case REVOLVER:
            return "REVOLVER MODEL";
        default:
            return "Err??";
    }
}