/**
 * @file file_definitions.h
 * @author Ondrej Sakači (sakacond@fel.cvut.cz)
 * @brief Definitions of files used in the programs.
 * @version 0.1
 * @date 2021-06-12
 * 
 * 
 */

#ifndef __FILE_DEF
#define __FILE_DEF

#include <string>

const std::string CONFIG_FILE_NAME = "data/config.conf";

/**
 * @brief Model objects.
 * 
 */
const char* CAR_MODEL_NAME = "data/models/dummy_car/car/Car.obj"; 
const char* CAR_COM_MODEL_NAME = "data/models/chevrolet/Chevrolet_Camaro_SS_Low.obj";
const char* BUILDING1_MODEL_NAME = "data/models/building_complex/building_04.obj";
const char* TOWER01_MODEL_NAME = "data/models/towers/Residential Buildings 001.obj";
const char* TOWER02_MODEL_NAME = "data/models/towers/Residential Buildings 002.obj";
const char* TOWER03_MODEL_NAME = "data/models/towers/Residential Buildings 003.obj";
const char* TOWER04_MODEL_NAME = "data/models/towers/Residential Buildings 004.obj";
const char* TOWER05_MODEL_NAME = "data/models/towers/Residential Buildings 005.obj";
const char* TOWER06_MODEL_NAME = "data/models/towers/Residential Buildings 006.obj";
const char* TOWER07_MODEL_NAME = "data/models/towers/Residential Buildings 007.obj";
const char* TOWER08_MODEL_NAME = "data/models/towers/Residential Buildings 008.obj";
const char* TOWER09_MODEL_NAME = "data/models/towers/Residential Buildings 009.obj";
const char* TOWER10_MODEL_NAME = "data/models/towers/Residential Buildings 010.obj";
const char* REVOLVER_MODEL_NAME = "data/models/revolver/objexport.obj";

/**
 * @brief Textures
 * 
 */
const char* EXPLOSION_TEXTURE_NAME = "data/textures/explode.png";
const char* BLUE_FIRE_TEXTURE_NAME = "data/textures/blue_fire.png";

const char* FOG_TEXTURE_NAME = "data/textures/fog.png";
const char* GROUND_TEXTURE_NAME = "data/textures/ground_texture.jpg";
const char* GRASS_TEXTURE_NAME = "data/textures/grass_texture.jpg";

const char* SKYBOX_CUBE_TEXTURE_FILE_PREFIX = "data/textures/skybox_night/skybox";
const char* SKYBOX_DAY_CUBE_TEXTURE_FILE_PREFIX = "data/textures/skybox_day/skybox"; // doesnt work
const char* SKYBOX_DAY_CUBE2_TEXTURE_FILE_PREFIX = "data/textures/skybox_day/"; // doesnt work

const char* BLOOD_TEXTURE_NAME = "data/textures/blood.png";

const char* FONT_PATH = "data/textures/fonts/";
const char* CROSSHAIR_TEXTURE_NAME = "data/textures/crosshair.png";
const char* BANNER_TEXTURE_NAME = "data/textures/gameOver.png";

#endif //__FILE_DEF