/**
 * @file render_stuff.cpp
 * @author Ondrej Sakači (sakacond@fel.cvut.cz)
 * @brief 
 * @version 0.1
 * @date 2021-05-24
 * 
 * 
 */


#include <map>
#include <string>

#include "render_stuff.h"
#include "data.h"
#include "file_definitions.h"
#include "game_world.h"
#include "spline.h"
#include "shader.h"

// here are defined object models
GameWorldModels models;

// definitions of geometries and textures
MeshGeometry* crosshairGeometry = NULL;
MeshGeometry* bannerGeometry = NULL;
MeshGeometry* explosionGeometry = NULL;
MeshGeometry* blueFireGeometry = NULL;

MeshGeometry* skyboxGeometry = NULL;
MeshGeometry* skyboxDayGeometry = NULL;
MeshGeometry* fogGeometry = NULL;
MeshGeometry* HUDGeometry = NULL;

std::map<char, MeshGeometry*> alphabetCharacters;
MeshGeometry* heart = NULL;
MeshGeometry* ubuntu = NULL;

// common shader program used for most of the rendering
SCommonShaderProgram shaderProgram;

const glm::vec3 crosshairPosition = glm::vec3(0.0f, 0.0f, 0.0f);

bool useLighting = false;

// other shader programs
ExplosionShaderProgram explosionShaderProgram;
BlueFireShaderProgram blueFireShaderProgram;
CrosshairShaderProgram crosshairShaderProgram;
BannerShaderProgram bannerShaderProgram;
SkyboxFarPlaneShaderProgram skyboxFarPlaneShaderProgram;
FogShaderProgram fogShaderProgram;
HUDShaderProgram hudShaderProgram;

/**
 * @brief Helper for parser of the config file.
 * 
 */
enum Reading {
  NOTHING = -1,
  GROUND_X_FRAGS = 0,
  GROUND_Y_FRAGS = 1,
  GROUND_COLOR = 2,
  // OTHER THINGS
};

/**
 * @brief Reads config file and generates ground accordingly.
 * 
 * @param file_name 
 * @param gameObjects 
 */
Ground* createGroundFromConfigFile(const std::string& file_name) {
  Reading read = Reading::NOTHING;
  
  int fragX = GROUND_FRAGMENTS_X;
  int fragY = GROUND_FRAGMENTS_Y;

  float r = 0.0f;
  float g = 1.0f;
  float b = 0.0f;

  std::fstream stream(file_name);
  std::string line;
  while (getline(stream, line)) {
    //DEBUG
    printf(line.c_str());
    printf("\n");

      if(line.find("#GROUND") != std::string::npos) {
        if(line.find("FRAGS_X") != std::string::npos)
          read = Reading::GROUND_X_FRAGS;
        else if(line.find("FRAGS_Y") != std::string::npos)
          read = Reading::GROUND_Y_FRAGS;
        else if(line.find("COLOR") != std::string::npos)
          read = Reading::GROUND_COLOR;
      }
      else {
        if(read != Reading::NOTHING) {
          if(read == Reading::GROUND_COLOR) {
            std::istringstream in(line);
            in >> r >> g >> b;
          }
          else if(read == Reading::GROUND_X_FRAGS){
            fragX = std::stoi(line);
          }
          else {
            fragY = std::stoi(line);
          }
          read = Reading::NOTHING;
        }
      }
  }

  Ground* result = generateGround(fragX, fragY);
  (*result).color = glm::vec3(r, g, b);

  return result;
}

/**
 * @brief Compiles new shader.
 * 
 * @param type 
 * @param shader 
 * @return unsigned int 
 */
static unsigned int compileShader(unsigned int type, const std::string& shader) {
  unsigned int id = glCreateShader(type);
  const char* source = shader.c_str();
  
  glShaderSource(id, 1, &source, nullptr);
  glCompileShader(id);

  // simple error handling
  int result;
  glGetShaderiv(id, GL_COMPILE_STATUS, &result);
  if(result == GL_FALSE) {
    
    int length;
    glGetShaderiv(id, GL_INFO_LOG_LENGTH, &length);
    char* message = (char*)valloc(length * sizeof(char));
    glGetShaderInfoLog(id, length, &length, message);

    std::cout << "Failed to compile " << ((type = GL_VERTEX_SHADER) ? "vertex." : "fragment.") << std::endl;
    std::cout << message << std::endl;

    glDeleteShader(id);
    return 0;
  }

  return id;
}

/**
 * @brief Create a Shader Program from sources
 * 
 * @param vertexShader 
 * @param fragmentShader 
 * @return unsigned int 
 */
static unsigned int createShaderProgram(const std::string& vertexShader, const std::string fragmentShader) {
  unsigned int program = glCreateProgram();
  unsigned int vShader = compileShader(GL_VERTEX_SHADER, vertexShader);
  unsigned int fShader = compileShader(GL_FRAGMENT_SHADER, fragmentShader);

  glAttachShader(program, vShader);
  glAttachShader(program, fShader);

  glLinkProgram(program);
  glValidateProgram(program);

  return program;
}

/**
 * @brief checks bounds
 * 
 * @param position 
 * @param objectSize 
 * @return std::tuple<glm::vec3, bool> 
 */
std::tuple<glm::vec3, bool> checkBounds(const glm::vec3 &position, float objectSize) {
  glm::vec3 newPosition = position;
  bool collided = false;

  if (position.x < -(SCENE_WIDTH + objectSize)) {
    newPosition.x = -(SCENE_WIDTH + objectSize);
    collided = true;
  }
  if (position.x > (SCENE_WIDTH - objectSize)) {
    newPosition.x = (SCENE_WIDTH - objectSize);
    collided = true;
  }

  if (position.y < -(SCENE_HEIGHT + objectSize)) {
    newPosition.y = -(SCENE_HEIGHT + objectSize);
    collided = true;
  }
  if (position.y > (SCENE_HEIGHT - objectSize)) {
    newPosition.y = (SCENE_HEIGHT - objectSize);
    collided = true;
  }

  if (position.z < -(SCENE_DEPTH + objectSize)) {
    newPosition.z = -(SCENE_DEPTH + objectSize); 
    collided = true;
  }
  if (position.z > (SCENE_DEPTH - objectSize)) {
    newPosition.z = (SCENE_DEPTH - objectSize); 
    collided = true;
  }

  return std::forward_as_tuple(newPosition, collided);
}

/**
 * @brief Checks if model has hitbox.
 * 
 * @param type 
 * @return true 
 * @return false 
 */
bool hasHitbox(ModelType type) {
  switch(type) {
    case CAR:
      return models.car.hasHitbox;
    case SIMPLE_CAR:
      return models.simple_car.hasHitbox;
    case BUILDING:
      return models.building1.hasHitbox;
    case TOWER01:
      return models.tower01.hasHitbox;
    case TOWER02:
      return models.tower02.hasHitbox;
    case TOWER03:
      return models.tower03.hasHitbox;
    case TOWER04:
      return models.tower04.hasHitbox;
    case TOWER05:
      return models.tower05.hasHitbox;
    case TOWER06:
      return models.tower06.hasHitbox;
    case TOWER07:
      return models.tower07.hasHitbox;
    case TOWER08:
      return models.tower08.hasHitbox;
    case TOWER09:
      return models.tower09.hasHitbox;
    case TOWER10:
      return models.tower10.hasHitbox;
    case BILLBOARD:
      return models.billboard.hasHitbox;
    default:
      return false;
  }
}

HitBox getObjectHitbox(ModelType type) {
  switch(type) {
    case SIMPLE_CAR:
      return models.simple_car.hitbox;
    case BUILDING:
      return models.building1.hitbox;
    case TOWER01:
      return models.tower01.hitbox;
    case TOWER02:
      return models.tower02.hitbox;
    case CAR:
    default:
      return models.car.hitbox;
  }
}

bool hasCollided(CarObject* car, float angle1, Object* object, float angle2) {
  if(!hasHitbox(car->modelType) || !hasHitbox(object->modelType))
    return false;

  float deltaX = abs(car->position.x - object->position.x);
  float deltaY = abs(car->position.y - object->position.y);

  HitBox carHitbox = getObjectHitbox(car->modelType);
  HitBox objectHitbox = getObjectHitbox(object->modelType);

  glm::vec3 carNormalizedDirection = glm::normalize(car->direction);
  glm::vec3 objectNormalizedDirection = glm::normalize(object->direction);

  float hitboxesSumX = (
    abs(carHitbox.width / 2 * carNormalizedDirection.x * cos(angle1)) + abs(objectHitbox.width / 2 * objectNormalizedDirection.x * cos(angle2))
  );
  float hitboxesSumY = (
    abs(carHitbox.length / 2 * carNormalizedDirection.y * cos(angle1)) + abs(objectHitbox.length / 2 * objectNormalizedDirection.y * cos(angle2))
  );

  return (deltaX < hitboxesSumX && deltaY < hitboxesSumY); 
}

/**
 * @brief Get the Nearest Ground Vertices based off of position.
 * 
 * @param position 
 * @return std::tuple<glm::vec3, glm::vec3, glm::vec3> 
 */
std::tuple<glm::vec3, glm::vec3, glm::vec3> getNearestGroundVertices(const glm::vec3 &position, const bool configGround) {
  Ground* groundVertices = (configGround) ? models.groundVerticesConfigurable : models.groundVertices;

  int x = floor(position.x);
  int y = ceil(position.y);
  int threeY = (groundVertices->fragX + 1) * 3;
  int index = ((x + 10) * 3) + ((10 - y) * (groundVertices->fragY + 1) * 3);

  glm::vec3 leftUpperVec = glm::vec3(
    groundVertices->vertices[index],
    groundVertices->vertices[index + 1],
    groundVertices->vertices[index + 2] 
  );  
  
  glm::vec3 rightUpperVec = glm::vec3(
    groundVertices->vertices[index + 3],
    groundVertices->vertices[index + 4],
    groundVertices->vertices[index + 5]
  );

  glm::vec3 leftLowerVec = glm::vec3(
    groundVertices->vertices[index + threeY],
    groundVertices->vertices[index + threeY + 1],
    groundVertices->vertices[index + threeY + 2]
  );

  glm::vec3 rightLowerVec = glm::vec3(
    groundVertices->vertices[index + threeY + 3],
    groundVertices->vertices[index + threeY + 4],
    groundVertices->vertices[index + threeY + 5]
  );

  float distRU = (position.x - rightUpperVec.x) * (position.x - rightUpperVec.x) + (position.y - rightUpperVec.y) * (position.y - rightUpperVec.y);
  float distLL = (position.x - leftLowerVec.x) * (position.x - leftLowerVec.x) + (position.y - leftLowerVec.y) * (position.y - leftLowerVec.y);

  if (distRU < distLL)
    return std::forward_as_tuple(leftUpperVec, rightLowerVec, rightUpperVec);

  if (distLL < distRU)
    return std::forward_as_tuple(leftUpperVec, leftLowerVec, rightLowerVec);

  glm::vec3 imaginaryUpperVec = glm::vec3(
    rightLowerVec.x + 1.0f,
    rightLowerVec.y + 1.0f,
    rightLowerVec.z
  );

  return std::forward_as_tuple(leftUpperVec, rightLowerVec, imaginaryUpperVec);
}

/**
 * @brief Get the Normal And Height of of current position on polygon
 * 
 * @param position 
 * @param nearestPoints 
 * @return std::tuple<glm::vec3, float> 
 */
std::tuple<glm::vec3, float> getNormalAndHeight(glm::vec3 &position, std::tuple<glm::vec3, glm::vec3, glm::vec3> &nearestPoints) {
  glm::vec3 cPoint = std::get<0>(nearestPoints);
  glm::vec3 vector1 = std::get<1>(nearestPoints) - cPoint;
  glm::vec3 vector2 = std::get<2>(nearestPoints) - cPoint;

  glm::vec3 groundNormal = glm::normalize(glm::cross(vector1, vector2));

  float d = -(groundNormal.x*cPoint.x + groundNormal.y*cPoint.y + groundNormal.z*cPoint.z);
  float height = - (position.x * groundNormal.x + position.y * groundNormal.y + d) / groundNormal.z;

  return std::forward_as_tuple(groundNormal, height);
}

/**
 * @brief Set the Transform Uniforms in opengl
 * 
 * @param modelMatrix 
 * @param viewMatrix 
 * @param projectionMatrix 
 */
void setTransformUniforms(const glm::mat4 &modelMatrix, const glm::mat4 &viewMatrix, const glm::mat4 &projectionMatrix) {
  glm::mat4 PVM = projectionMatrix * viewMatrix * modelMatrix;
  glUniformMatrix4fv(shaderProgram.PVMmatrixLocation, 1, GL_FALSE, glm::value_ptr(PVM));
  glUniformMatrix4fv(shaderProgram.VmatrixLocation, 1, GL_FALSE, glm::value_ptr(viewMatrix));
  glUniformMatrix4fv(shaderProgram.MmatrixLocation, 1, GL_FALSE, glm::value_ptr(modelMatrix));

  // just take 3x3 rotation part of the modelMatrix
  // we presume the last row contains 0,0,0,1
  const glm::mat4 modelRotationMatrix = glm::mat4(
    modelMatrix[0],
    modelMatrix[1],
    modelMatrix[2],
    glm::vec4(0.0f, 0.0f, 0.0f, 1.0f)
  );
  glm::mat4 normalMatrix = glm::transpose(glm::inverse(modelRotationMatrix));

  glUniformMatrix4fv(shaderProgram.normalMatrixLocation, 1, GL_FALSE, glm::value_ptr(normalMatrix));  // correct matrix for non-rigid transform
}

/**
 * @brief Set the Material Uniforms in openGl
 * 
 * @param ambient 
 * @param diffuse 
 * @param specular 
 * @param shininess 
 * @param texture 
 * @param mixTexture 
 */
void setMaterialUniforms(const glm::vec3 &ambient, const glm::vec3 &diffuse, const glm::vec3 &specular, float shininess, GLuint texture, GLuint mixTexture) {
  glUniform3fv(shaderProgram.diffuseLocation,  1, glm::value_ptr(diffuse));  // 2nd parameter must be 1 - it declares number of vectors in the vector array
  glUniform3fv(shaderProgram.ambientLocation,  1, glm::value_ptr(ambient));
  glUniform3fv(shaderProgram.specularLocation, 1, glm::value_ptr(specular));
  glUniform1f(shaderProgram.shininessLocation,    shininess);
  // glUniform1f(shaderProgram.transparencyLocation, transparency);

  if(texture != 0) {
    glUniform1i(shaderProgram.useTextureLocation, 1);  // do texture sampling
    glUniform1i(shaderProgram.texSamplerLocation, 0); 
    glActiveTexture(GL_TEXTURE0);                  // texturing unit 0 -> to be bound [for OpenGL BindTexture]
    glBindTexture(GL_TEXTURE_2D, texture);
  }
  else {
    glUniform1i(shaderProgram.useTextureLocation, 0);  // do not sample the texture
    glActiveTexture(GL_TEXTURE0);                  // texturing unit 0 -> to be bound [for OpenGL BindTexture]
    glBindTexture(GL_TEXTURE_2D, 0);
  }

  glUniform1i(shaderProgram.fogSamplerLocation, 1);
  glActiveTexture(GL_TEXTURE0 + 1);
  glBindTexture(GL_TEXTURE_2D, fogGeometry->texture); 

  if(mixTexture != 0) {
    glUniform1i(shaderProgram.mixTextureLocation, 1);
    glUniform1i(shaderProgram.mixSamplerLocation, 2);
    glActiveTexture(GL_TEXTURE0 + 2);
    glBindTexture(GL_TEXTURE_2D, mixTexture);
  }
  else {
    glUniform1i(shaderProgram.mixTextureLocation, 0);
    glActiveTexture(GL_TEXTURE0 + 2);               // texturing unit 0 -> to be bound [for OpenGL BindTexture]
    glBindTexture(GL_TEXTURE_2D, 0);
  }
}

/**
 * @brief Draws multimeshed geometry with original textures and materials.
 * 
 * @param geometry 
 * @param PVMmatrix 
 */
void drawComplexGeometry(std::vector<MeshGeometry> geometry, const glm::mat4& PVMmatrix) {
  for(int i = 0; i < geometry.size(); ++i) {
    setMaterialUniforms(
        geometry[i].ambient,
        geometry[i].diffuse,
        geometry[i].specular,
        geometry[i].shininess,
        geometry[i].texture,
        geometry[i].texture
    );
    glUniformMatrix4fv(shaderProgram.PVMmatrixLocation, 1, GL_FALSE, glm::value_ptr(PVMmatrix));

    // draw geometry
    glBindVertexArray(geometry[i].vertexArrayObject);
    glDrawElements(GL_TRIANGLES, geometry[i].numTriangles * 3, GL_UNSIGNED_INT, 0);
  }

  glBindVertexArray(0);
  glUseProgram(0);
  
  return;
}

/**
 * @brief Draws complex geometry with Custom Materials from author.
 * 
 * @param geometry 
 * @param PVMmatrix 
 * @param material 
 */
void drawComplexGeometryM(
  std::vector<MeshGeometry> geometry, 
  const glm::mat4& PVMmatrix, 
  Material* material
) {
  for(int i = 0; i < geometry.size(); ++i) {
    setMaterialUniforms(
        material->ambient,
        material->diffuse,
        material->specular,
        material->shinines,
        geometry[i].texture,
        geometry[i].mixTexture
    );
    glUniformMatrix4fv(shaderProgram.PVMmatrixLocation, 1, GL_FALSE, glm::value_ptr(PVMmatrix));

    // draw geometry
    glBindVertexArray(geometry[i].vertexArrayObject);
    glDrawElements(GL_TRIANGLES, geometry[i].numTriangles * 3, GL_UNSIGNED_INT, 0);
  }

  glBindVertexArray(0);
  glUseProgram(0);
  
  return;
}

Model getModel(ModelType type) {
  switch (type)
  {
    case CAR:
      return models.car;
    case SIMPLE_CAR:
      return models.simple_car;
    case BUILDING:
      return models.building1;
    case TOWER01:
      return models.tower01;
    case TOWER02:
      return models.tower02;
    case TOWER03:
      return models.tower03;
    case TOWER04:
      return models.tower04;
    case TOWER05:
      return models.tower05;
    case TOWER06:
      return models.tower06;
    case TOWER07:
      return models.tower07;
    case TOWER08:
      return models.tower08;
    case TOWER09:
      return models.tower09;
    case TOWER10:
      return models.tower10;
    case BILLBOARD:
      return models.billboard;
    case REVOLVER:
      return models.revolver;
    default:
      break;
  }
}

/**
 * @brief Function for drawing specifically car objects, that can travel.
 * 
 * @param car 
 * @param viewMatrix 
 * @param projectionMatrix 
 * @param upVector 
 * @param height 
 * @param drawHitboxes 
 */
void drawCarObject(
  Object* car,
  const glm::vec3& cameraPosition, 
  const glm::mat4& viewMatrix, 
  const glm::mat4& projectionMatrix, 
  const glm::vec3 &upVector, 
  float height,
  bool reflectorOn,
  bool drawHitboxes
) {
  glUseProgram(shaderProgram.program);

  Model model = getModel(car->modelType);

  (*car).position.z = height + (car->size + (model.size - car->size) / 2);

  glm::vec3 modelNormal = glm::vec3(0, 1, 0);
  glm::vec3 modelDirection = glm::vec3(0, 0, -1);

  glm::vec3 yVect = glm::vec3(
    -car->direction.y,
    car->direction.x,
    0.0f
  );

  float xAngle = glm::dot(upVector, car->direction) * 90.0f;
  float yAngle = glm::dot(upVector, yVect) * 90.0f;

  // glm::mat4 modelMatrix = alignObject(car->position, -car->direction, upVector);
  glm::mat4 modelMatrix = glm::translate(glm::mat4(1.0f), car->position);
  modelMatrix = glm::rotate(modelMatrix, glm::radians(90.f), glm::vec3(1.0f, 0.0f, 0.0f));
  modelMatrix = glm::rotate(modelMatrix, glm::radians(car->viewAngle + 90.0f), glm::vec3(0, 1, 0));
  modelMatrix = glm::rotate(modelMatrix, glm::radians(xAngle), glm::vec3(1.0f, 0.0f, 0.0f));
  modelMatrix = glm::rotate(modelMatrix, glm::radians(-yAngle), glm::vec3(0.0f, 0.0f, 1.0f));
  modelMatrix = glm::scale(modelMatrix, glm::vec3(car->size, car->size, car->size));

  glm::mat4 PVMmatrix = projectionMatrix * viewMatrix * modelMatrix;

  // send matrices to the vertex & fragment shader)
  setTransformUniforms(modelMatrix, viewMatrix, projectionMatrix);
  glUniformMatrix4fv(shaderProgram.PVMmatrixLocation, 1, GL_FALSE, glm::value_ptr(PVMmatrix));
  glUniform3f(shaderProgram.cameraPositionLocation, cameraPosition.x, cameraPosition.y, cameraPosition.z);
  glUniform1i(skyboxFarPlaneShaderProgram.foggyLocation, 0);
  glUniform1i(shaderProgram.reflectorOnLocation, reflectorOn);
  glUniform1i(shaderProgram.transformationCheckLocation, 0);

  if(car->modelType == BILLBOARD)
    drawBillboard(car, cameraPosition, viewMatrix, projectionMatrix, reflectorOn, false, height);
  else{
    //// DEBUG BECAUSE OF WHITE CAR MODELS
    //
    // printf("Model type = %i", car->modelType);
    // for(int i = 0; i < model.geometry.size(); ++i) {
    //   printf("Ambient: %f %f %f Diffuse: %f %f %f Specular: %f %f %f Shininess: %f\n", 
    //     model.geometry[i].ambient.x, model.geometry[i].ambient.y, model.geometry[i].ambient.z, 
    //     model.geometry[i].diffuse.x, model.geometry[i].diffuse.y, model.geometry[i].diffuse.z, 
    //     model.geometry[i].specular.x, model.geometry[i].specular.y, model.geometry[i].specular.z, 
    //     model.geometry[i].shininess);
    // }
    drawComplexGeometry(model.geometry, PVMmatrix);
  }
  // if(drawHitboxes)
  //   drawHitbox(model.hitboxGeometry);
  
  return;
}

/**
 * @brief Function for drawing any static object
 * 
 * @param object 
 * @param viewMatrix 
 * @param projectionMatrix 
 */
void drawAnyObject(
  Object* object, 
  const glm::vec3& cameraPosition, 
  const glm::mat4& viewMatrix, 
  const glm::mat4& projectionMatrix, 
  float height,
  bool reflectorOn,
  bool transformation,
  bool drawHitboxes
) {
  glUseProgram(shaderProgram.program);
  
  Model model = getModel(object->modelType);

  (*object).position.z = height + ((*object).size) - 0.025f;
  
  // prepare modeling transform matrix
  glm::mat4 modelMatrix = glm::translate(glm::mat4(1.0f), object->position);
  modelMatrix = glm::rotate(
    modelMatrix,
    glm::radians(glm::dot(glm::vec3(1.0f, 0.0f, 0.0f), object->direction) * 90),
    glm::vec3(0, 0, 1)
  );
  modelMatrix = glm::rotate(modelMatrix, glm::radians(90.0f), glm::vec3(1, 0, 0));
  modelMatrix = glm::scale(modelMatrix, glm::vec3(object->size, object->size, object->size));
  
  glm::mat4 PVMmatrix = projectionMatrix * viewMatrix * modelMatrix;

  // send matrices to the vertex & fragment shader
  setTransformUniforms(modelMatrix, viewMatrix, projectionMatrix);
  glUniformMatrix4fv(shaderProgram.PVMmatrixLocation, 1, GL_FALSE, glm::value_ptr(PVMmatrix));
  glUniform3f(shaderProgram.cameraPositionLocation, cameraPosition.x, cameraPosition.y, cameraPosition.z);
  glUniform1i(skyboxFarPlaneShaderProgram.foggyLocation, 0);
  glUniform1i(shaderProgram.reflectorOnLocation, reflectorOn);
  if(transformation == true)
    glUniform1i(shaderProgram.transformationCheckLocation, 1); //transformation
  else
    glUniform1i(shaderProgram.transformationCheckLocation, 0);

  if(object->modelType == BILLBOARD) {
    drawBillboard(object, cameraPosition, viewMatrix, projectionMatrix, reflectorOn, transformation, height);
  }
  else
    drawComplexGeometry(model.geometry, PVMmatrix);

  return;
}

/**
 * @brief Function for drawing any static object with custom textures (not loaded from file)
 * 
 * @param object 
 * @param cameraPosition 
 * @param viewMatrix 
 * @param projectionMatrix 
 * @param height 
 * @param material 
 * @param reflectorOn 
 */
void drawAnyObjectCM(
  Object* object, 
  const glm::vec3& cameraPosition, 
  const glm::mat4& viewMatrix, 
  const glm::mat4& projectionMatrix, 
  float height,
  int material,
  bool reflectorOn
) {
  glUseProgram(shaderProgram.program);
  
  Model model = getModel(object->modelType);

  (*object).position.z = height + ((*object).size) - 0.025f;
  
  // prepare modeling transform matrix
  glm::mat4 modelMatrix = glm::translate(glm::mat4(1.0f), object->position);
  modelMatrix = glm::rotate(
    modelMatrix,
    glm::radians(glm::dot(glm::vec3(1.0f, 0.0f, 0.0f), object->direction) * 90),
    glm::vec3(0, 0, 1)
  );
  modelMatrix = glm::rotate(modelMatrix, glm::radians(90.0f), glm::vec3(1, 0, 0));
  modelMatrix = glm::scale(modelMatrix, glm::vec3(object->size, object->size, object->size));
  
  glm::mat4 PVMmatrix = projectionMatrix * viewMatrix * modelMatrix;

  // send matrices to the vertex & fragment shader
  setTransformUniforms(modelMatrix, viewMatrix, projectionMatrix);
  glUniformMatrix4fv(shaderProgram.PVMmatrixLocation, 1, GL_FALSE, glm::value_ptr(PVMmatrix));
  glUniform3f(shaderProgram.cameraPositionLocation, cameraPosition.x, cameraPosition.y, cameraPosition.z);
  glUniform1i(skyboxFarPlaneShaderProgram.foggyLocation, 0);
  glUniform1i(shaderProgram.reflectorOnLocation, reflectorOn);

  if(material == 3)
    glUniform1i(shaderProgram.transformationCheckLocation, 1);
  else
    glUniform1i(shaderProgram.transformationCheckLocation, 0);

  if(object->modelType == BILLBOARD) {
    drawBillboard(object, cameraPosition, viewMatrix, projectionMatrix, reflectorOn, false, height);
  }
  else
    drawComplexGeometryM(model.geometry, PVMmatrix, models.materials[material]);

  return;
}

/**
 * @brief Draws revolver object.
 * 
 * @param revolver 
 * @param cameraPosition 
 * @param viewMatrix 
 * @param projectionMatrix 
 * @param upVector 
 * @param angleX 
 * @param angleY 
 * @param reflectorOn 
 */
void drawRevolver(
  Object* revolver,
  const glm::vec3& cameraPosition,
  const glm::mat4& viewMatrix, 
  const glm::mat4& projectionMatrix, 
  const glm::vec3& upVector,
  float angleX,
  float angleY,
  bool reflectorOn
) {
  glUseProgram(shaderProgram.program);
  
  // TRANSLATING
  glm::mat4 modelMatrix = glm::translate(
    glm::mat4(1.0f), 
    revolver->position + glm::normalize(revolver->direction) * 0.25f
  );
  modelMatrix = glm::translate(
    modelMatrix,
    glm::normalize(glm::cross(revolver->direction, upVector)) * 0.1f
  );
  modelMatrix = glm::translate(
    modelMatrix,
    -glm::normalize(upVector) * 0.07f
  );

  // ROTATIONS

  modelMatrix = glm::rotate(
    modelMatrix, 
    glm::radians(
      glm::dot(
        revolver->direction, glm::vec3(0.0f, 0.0f, 1.0f)
      ) + 180.0f
    ), 
    glm::cross(revolver->direction, upVector)
  );
  modelMatrix = glm::rotate(
    modelMatrix,
    glm::radians(
      glm::dot(
        glm::cross(revolver->direction, upVector), 
        glm::vec3(0.0f, 1.0f, 0.0f))
      - angleY),
    glm::vec3(1.0f, 0.0f, 0.0f)
  );
  // modelMatrix = glm::rotate(
  //   modelMatrix,
  //   glm::radians(angleX),
  //   glm::vec3(0.0f, 0.0f, 0.0f)
  // );

  // SCALING
  modelMatrix = glm::scale(modelMatrix, glm::vec3(revolver->size, revolver->size, revolver->size));

  glm::mat4 PVMmatrix = projectionMatrix * viewMatrix * modelMatrix;

  // send matrices to the vertex & fragment shader
  setTransformUniforms(modelMatrix, viewMatrix, projectionMatrix);
  glUniform3f(skyboxFarPlaneShaderProgram.cameraPositionLocation, cameraPosition.x, cameraPosition.y, cameraPosition.z);
  glUniform1i(skyboxFarPlaneShaderProgram.foggyLocation, 0);
  glUniform1i(shaderProgram.reflectorOnLocation, reflectorOn);
  glUniform1i(shaderProgram.transformationCheckLocation, 0);
  drawComplexGeometry(models.revolver.geometry, PVMmatrix);
  
  return;
}

void drawCrosshair(const glm::mat4 & viewMatrix, const glm::mat4 & projectionMatrix) {
  glEnable(GL_BLEND);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

  glDisable(GL_DEPTH_TEST);

  glUseProgram(crosshairShaderProgram.program);

  glm::mat4 matrix = glm::translate(glm::mat4(1.0f), crosshairPosition);
  matrix = glm::scale(matrix, glm::vec3(CROSSHAIR_SIZE));

  glm::mat4 PVMmatrix = projectionMatrix * viewMatrix * matrix;
  glUniformMatrix4fv(crosshairShaderProgram.PVMmatrixLocation, 1, GL_FALSE, glm::value_ptr(PVMmatrix)); 
  glUniform1i(crosshairShaderProgram.texSamplerLocation, 0);

  glActiveTexture(GL_TEXTURE0);
  glBindTexture(GL_TEXTURE_2D, crosshairGeometry->texture);
  glBindVertexArray(crosshairGeometry->vertexArrayObject);
  glDrawArrays(GL_TRIANGLE_STRIP, 0, crosshairGeometry->numTriangles);

  CHECK_GL_ERROR();

  glBindVertexArray(0);
  glUseProgram(0);

  glEnable(GL_DEPTH_TEST);
  glDisable(GL_BLEND);

  return;
}

/**
 * @brief Deprecated function. Fog is drawn in lightingPerVertex and farPlaneSkyBox shaders.
 * 
 * @param modelMatrix 
 * @param viewMatrix 
 * @param projectionMatrix 
 * @param elapsedTime 
 */
void drawFog(const glm::mat4& modelMatrix, const glm::mat4 & viewMatrix, const glm::mat4 & projectionMatrix, float elapsedTime) {
  MeshGeometry* model = fogGeometry;
  
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
  glEnable(GL_BLEND);

  glDisable(GL_DEPTH_TEST);

  glUseProgram(fogShaderProgram.program);
  
  glm::mat4 PVMmatrix = projectionMatrix * viewMatrix * modelMatrix;
  glUniformMatrix4fv(fogShaderProgram.PVMmatrixLocation, 1, GL_FALSE, glm::value_ptr(PVMmatrix)); 
  glUniform1i(fogShaderProgram.texSamplerLocation, 0);
  glUniform1f(fogShaderProgram.elapsedTimeLocation, elapsedTime);
  glUniformMatrix4fv(fogShaderProgram.MmatrixLocation, 1, GL_FALSE, glm::value_ptr(modelMatrix));

  glBindTexture(GL_TEXTURE_2D, model->texture);
  glBindVertexArray(model->vertexArrayObject);
  glDrawArrays(GL_TRIANGLE_STRIP, 0, model->numTriangles);

  CHECK_GL_ERROR();

  glBindVertexArray(0);
  glUseProgram(0);

  glEnable(GL_DEPTH_TEST);
  glDisable(GL_BLEND);

  return;  
}

// void drawHitbox(MeshGeometry* geometry) {
//   std::cout << "Drawing hbox..." << std::endl;
//   if(models.car.hasHitbox)
//     std::cout << "Car has hitbox" << std::endl;
  
//   setMaterialUniforms(
//       (*geometry).ambient,
//       (*geometry).diffuse,
//       (*geometry).specular,
//       (*geometry).shininess,
//       (*geometry).texture,
//       (*geometry).mixTexture
//   );

//   // draw geometry
//   glBindVertexArray((*geometry).vertexArrayObject);
//   glDrawElements(GL_TRIANGLES, (*geometry).numTriangles * 3, GL_UNSIGNED_INT, 0);
//   CHECK_GL_ERROR();

//   glBindVertexArray(0);
//   glUseProgram(0);
  
//   return;
// }

/**
 * @brief Function for drawing custom Billboard VAO.
 * 
 * @param object 
 * @param cameraPosition 
 * @param viewMatrix 
 * @param projectionMatrix 
 * @param reflectorOn 
 * @param transformation 
 * @param height 
 */
void drawBillboard(Object* object, const glm::vec3& cameraPosition, const glm::mat4 & viewMatrix, const glm::mat4 & projectionMatrix, bool reflectorOn, bool transformation, float height) {

  glUseProgram(shaderProgram.program);

  (*object).position.z = height + BILLBOARD_HEIGHT / 2;

  // // align billboard coordinate system to match its position and direction 
  glm::mat4 modelMatrix = glm::translate(glm::mat4(1.0f), object->position);
  modelMatrix = glm::rotate(
    modelMatrix,
    glm::radians(glm::dot(glm::vec3(1.0f, 0.0f, 0.0f), object->direction) * 90),
    glm::vec3(0, 0, 1)
  );
  modelMatrix = glm::scale(modelMatrix, glm::vec3(object->size));
  glm::mat4 PVMmatrix = projectionMatrix * viewMatrix * modelMatrix;

  // // send matrices to the vertex & fragment shader
  setTransformUniforms(modelMatrix, viewMatrix, projectionMatrix);
  glUniformMatrix4fv(shaderProgram.PVMmatrixLocation, 1, GL_FALSE, glm::value_ptr(PVMmatrix));
  glUniform1i(shaderProgram.reflectorOnLocation, reflectorOn);
  if(transformation == true)
    glUniform1i(shaderProgram.transformationCheckLocation, 1); //transformation
  else
    glUniform1i(shaderProgram.transformationCheckLocation, 0);
  
  setMaterialUniforms(
    models.billboard.geometry[0].ambient,
    models.billboard.geometry[0].diffuse,
    models.billboard.geometry[0].specular,
    models.billboard.geometry[0].shininess,
    models.billboard.geometry[0].texture,
    models.billboard.geometry[0].mixTexture
  );

  glBindVertexArray(models.billboard.geometry[0].vertexArrayObject);
  glDrawArrays(GL_TRIANGLES, 0, 3*models.billboard.geometry[0].numTriangles);
  CHECK_GL_ERROR();

  glBindVertexArray(0);
  glUseProgram(0);

  return;
}

/**
 * @brief Function for drawing Ground object.
 * 
 * @param cameraPosition 
 * @param viewMatrix 
 * @param projectionMatrix 
 * @param reflectorOn 
 * @param configGround 
 */
void drawGround(
  const glm::vec3& cameraPosition, 
  const glm::mat4 & viewMatrix, 
  const glm::mat4 & projectionMatrix, 
  bool reflectorOn, 
  bool configGround
) {
  MeshGeometry* ground = (configGround) ? models.groundConfigurable : models.ground ;

  glUseProgram(shaderProgram.program);
  glm::mat4 modelMatrix = glm::translate(
    glm::mat4(1.0f),
    glm::vec3(0.0f, 0.0f, 0.0f)
  );

  glm::mat4 PVMmatrix = projectionMatrix * viewMatrix * modelMatrix;

  // send matrices to the vertex & fragment shader
  setTransformUniforms(modelMatrix, viewMatrix, projectionMatrix);
  glUniformMatrix4fv(shaderProgram.PVMmatrixLocation, 1, GL_FALSE, glm::value_ptr(PVMmatrix));
  glUniform1i(skyboxFarPlaneShaderProgram.foggyLocation, 0);
  glUniform1i(shaderProgram.reflectorOnLocation, reflectorOn);
  glUniform1i(shaderProgram.transformationCheckLocation, 0);

  setMaterialUniforms(
    ground->ambient,
    ground->diffuse,
    ground->specular,
    ground->shininess,
    ground->texture,
    ground->mixTexture
  );

  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
  glEnable(GL_BLEND);
  glBindVertexArray(ground->vertexArrayObject);
  glDrawArrays(GL_TRIANGLES, 0, 3 * ground->numTriangles);
  CHECK_GL_ERROR();

  glBindVertexArray(0);
  glUseProgram(0);

  return;
}

/**
 * @brief Draws one character
 * 
 * @param letter 
 * @param position 
 * @param viewMatrix 
 * @param projectionMatrix 
 */
void drawCharacter(const MeshGeometry* letter, const float size, const glm::vec3& position, const glm::mat4 & viewMatrix, const glm::mat4 & projectionMatrix) {
  glEnable(GL_BLEND);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

  glDisable(GL_DEPTH_TEST);

  glUseProgram(crosshairShaderProgram.program);

  glm::mat4 matrix = glm::translate(glm::mat4(1.0f), position);
  matrix = glm::scale(matrix, glm::vec3(size));

  glm::mat4 PVMmatrix = projectionMatrix * viewMatrix * matrix;
  glUniformMatrix4fv(crosshairShaderProgram.PVMmatrixLocation, 1, GL_FALSE, glm::value_ptr(PVMmatrix)); 
  glUniform1i(crosshairShaderProgram.texSamplerLocation, 0);

  glActiveTexture(GL_TEXTURE0);
  glBindTexture(GL_TEXTURE_2D, letter->texture);
  glBindVertexArray(letter->vertexArrayObject);
  glDrawArrays(GL_TRIANGLE_STRIP, 0, letter->numTriangles);

  CHECK_GL_ERROR();

  glBindVertexArray(0);
  glUseProgram(0);

  glEnable(GL_DEPTH_TEST);
  glDisable(GL_BLEND);

  return;
}

/**
 * @brief Reads input text object and draws individual characters.
 * 
 * @param text 
 * @param viewMatrix 
 * @param projectionMatrix 
 */
void drawText(TextObject* text, const glm::mat4 & viewMatrix, const glm::mat4 & projectionMatrix) {
    MeshGeometry* character;
    
    if((*text).text == "#ubuntu")
      drawCharacter(ubuntu, (*text).size, (*text).position, viewMatrix, projectionMatrix);
    
    else if((*text).text == "#heart")
      drawCharacter(heart, (*text).size, (*text).position, viewMatrix, projectionMatrix);
    
    else {
      std::string::const_iterator c;
      glm::vec3 currentPosition = (*text).position;
      for (c = (*text).text.begin(); c != (*text).text.end(); c++) {
        character = alphabetCharacters[*c];
        drawCharacter(character, (*text).size, currentPosition, viewMatrix, projectionMatrix);
        currentPosition.x += ((*text).size * 2 * 10);
      }
    }
}

/**
 * @brief Testing func. Used in production version, because of weird problems in C++.
 * 
 * @param text 
 * @param size 
 * @param position 
 * @param viewMatrix 
 * @param projectionMatrix 
 */
void drawText(const std::string& text, float size, const glm::vec3& position, const glm::mat4 & viewMatrix, const glm::mat4 & projectionMatrix) {
    MeshGeometry* character;
    
    if(text == "#ubuntu")
      drawCharacter(ubuntu, size, position, viewMatrix, projectionMatrix);
    
    else if(text == "#heart")
      drawCharacter(heart, size, position, viewMatrix, projectionMatrix);
    
    else {
      std::string::const_iterator c;
      glm::vec3 currentPosition = position;
      for (c = text.begin(); c != text.end(); c++) {
        character = alphabetCharacters[*c];
        drawCharacter(character, size, currentPosition, viewMatrix, projectionMatrix);
        currentPosition.x += (size * 2 * 10);
      }
    }
}

/**
 * @brief Draws simple HUD elements.
 * 
 * @param health 
 * @param viewMatrix 
 * @param projectionMatrix 
 */
void drawHUD(
  const float health,
  const glm::mat4 & viewMatrix,
  const glm::mat4 & projectionMatrix
) {
  glEnable(GL_BLEND);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

  glDisable(GL_DEPTH_TEST);

  glUseProgram(hudShaderProgram.program);

  glm::mat4 matrix = glm::mat4(1.0f);
  matrix = glm::scale(matrix, glm::vec3(BLOOD_SIZE));
  glm::mat4 PVMmatrix = projectionMatrix * viewMatrix * matrix;

  glUniform1f(hudShaderProgram.healthLocation, health);
  glUniformMatrix4fv(hudShaderProgram.PVMmatrixLocation, 1, GL_FALSE, glm::value_ptr(PVMmatrix)); 
  glUniform1i(hudShaderProgram.texSamplerLocation, 0);

  glActiveTexture(GL_TEXTURE0);
  glBindTexture(GL_TEXTURE_2D, HUDGeometry->texture);
  glBindVertexArray(HUDGeometry->vertexArrayObject);
  glDrawArrays(GL_TRIANGLE_STRIP, 0, HUDGeometry->numTriangles);

  CHECK_GL_ERROR();
  glBindTexture(GL_TEXTURE_2D, 0);
  glBindVertexArray(0);
  glUseProgram(0);

  glEnable(GL_DEPTH_TEST);
  glDisable(GL_BLEND);

  return;
}

void drawExplosion(ExplosionObject* explosion, const glm::vec3& cameraPosition, const glm::mat4 & viewMatrix, const glm::mat4 & projectionMatrix) {
  glEnable(GL_BLEND);
  glBlendFunc(GL_ONE, GL_ONE);

  glUseProgram(explosionShaderProgram.program);

 // just take 3x3 rotation part of the view transform
  glm::mat4 billboardRotationMatrix = glm::mat4(
    viewMatrix[0],
    viewMatrix[1],
    viewMatrix[2],
    glm::vec4(0.0f, 0.0f, 0.0f, 1.0f)
  );
  // inverse view rotation
  billboardRotationMatrix = glm::transpose(billboardRotationMatrix);

  glm::mat4 matrix = glm::translate(glm::mat4(1.0f), explosion->position);
  matrix = glm::scale(matrix, glm::vec3(explosion->size));
  matrix = matrix*billboardRotationMatrix; // make billboard to face the camera

  glm::mat4 PVMmatrix = projectionMatrix * viewMatrix * matrix;
  glUniformMatrix4fv(explosionShaderProgram.PVMmatrixLocation, 1, GL_FALSE, glm::value_ptr(PVMmatrix));  // model-view-projection
  glUniformMatrix4fv(explosionShaderProgram.VmatrixLocation, 1, GL_FALSE, glm::value_ptr(viewMatrix));   // view
  glUniform1f(explosionShaderProgram.timeLocation, explosion->currentTime - explosion->startTime);
  
  glActiveTexture(GL_TEXTURE0);
  glUniform1i(explosionShaderProgram.texSamplerLocation, 0);
  glUniform1f(explosionShaderProgram.frameDurationLocation, explosion->frameDuration);

  glBindVertexArray(explosionGeometry->vertexArrayObject);
  glBindTexture(GL_TEXTURE_2D, explosionGeometry->texture);
  glDrawArrays(GL_TRIANGLE_STRIP, 0, explosionGeometry->numTriangles);

  glBindVertexArray(0);
  glUseProgram(0);

  glDisable(GL_BLEND);

  return;
}

void drawBlueFire(BlueFireObject* blueFire, const glm::vec3& cameraPosition, const glm::mat4 & viewMatrix, const glm::mat4 & projectionMatrix) {
  glEnable(GL_BLEND);
  glBlendFunc(GL_ONE, GL_ONE);

  glUseProgram(blueFireShaderProgram.program);

  glm::mat4 billboardRotationMatrix = glm::mat4(
    viewMatrix[0],
    viewMatrix[1],
    viewMatrix[2],
    glm::vec4(0.0f, 0.0f, 0.0f, 1.0f)
  );
  
  // inverse view rotation
  billboardRotationMatrix = glm::transpose(billboardRotationMatrix);

  glm::mat4 matrix = glm::translate(glm::mat4(1.0f), blueFire->position);
  matrix = glm::scale(matrix, glm::vec3(blueFire->size));
  matrix = matrix * billboardRotationMatrix; // make billboard to face the camera

  glm::mat4 PVMmatrix = projectionMatrix * viewMatrix * matrix;
  glUniformMatrix4fv(blueFireShaderProgram.PVMmatrixLocation, 1, GL_FALSE, glm::value_ptr(PVMmatrix));  // model-view-projection
  glUniformMatrix4fv(blueFireShaderProgram.VmatrixLocation, 1, GL_FALSE, glm::value_ptr(viewMatrix));   // view
  glUniform1f(blueFireShaderProgram.timeLocation, blueFire->currentTime - blueFire->startTime);
  
  glActiveTexture(GL_TEXTURE0);
  glUniform1i(blueFireShaderProgram.texSamplerLocation, 0);
  glUniform1f(blueFireShaderProgram.frameDurationLocation, blueFire->frameDuration);

  glBindVertexArray(blueFireGeometry->vertexArrayObject);
  glBindTexture(GL_TEXTURE_2D, blueFireGeometry->texture);
  glDrawArrays(GL_TRIANGLE_STRIP, 0, blueFireGeometry->numTriangles);

  glBindVertexArray(0);
  glUseProgram(0);

  glDisable(GL_BLEND);

  return;
}

void drawBanner(BannerObject* banner, const glm::mat4 & viewMatrix, const glm::mat4 & projectionMatrix) {
  glEnable(GL_BLEND);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

  glDisable(GL_DEPTH_TEST);

  glUseProgram(bannerShaderProgram.program);

  glm::mat4 matrix = glm::translate(glm::mat4(1.0f), banner->position);
  matrix = glm::scale(matrix, glm::vec3(banner->size));

  glm::mat4 PVMmatrix = projectionMatrix * viewMatrix * matrix;
  glUniformMatrix4fv(bannerShaderProgram.PVMmatrixLocation, 1, GL_FALSE, glm::value_ptr(PVMmatrix));        // model-view-projection
  glUniform1f(bannerShaderProgram.timeLocation, banner->currentTime - banner->startTime);
  
  glActiveTexture(GL_TEXTURE0);
  glBindTexture(GL_TEXTURE_2D, bannerGeometry->texture);
  glUniform1i(bannerShaderProgram.texSamplerLocation, 0);
  
  glBindVertexArray(bannerGeometry->vertexArrayObject);
  glDrawArrays(GL_TRIANGLE_STRIP, 0, bannerGeometry->numTriangles);

  CHECK_GL_ERROR();

  glBindVertexArray(0);
  glUseProgram(0);

  glEnable(GL_DEPTH_TEST);
  glDisable(GL_BLEND);

  return;
}

/**
 * @brief Function for drawing skybox with fog.
 * 
 * @param cameraPosition 
 * @param viewMatrix 
 * @param projectionMatrix 
 * @param elapsedTime 
 * @param day 
 */
void drawSkybox(
  const glm::vec3& cameraPosition,
  const glm::mat4 & viewMatrix, 
  const glm::mat4 & projectionMatrix, 
  float elapsedTime,
  bool day
) {
  glEnable(GL_BLEND);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

  glUseProgram(skyboxFarPlaneShaderProgram.program);

  // compose transformations
  glm::mat4 matrix = projectionMatrix * viewMatrix;

  // create view rotation matrix by using view matrix with cleared translation
  glm::mat4 viewRotation = viewMatrix;
  viewRotation[3] = glm::vec4(0.0f, 0.0f, 0.0f, 1.0f);

  // vertex shader will translate screen space coordinates (NDC) using inverse PV matrix
  glm::mat4 inversePVmatrix = glm::inverse(projectionMatrix * viewRotation);

  glUniformMatrix4fv(skyboxFarPlaneShaderProgram.inversePVmatrixLocation, 1, GL_FALSE, glm::value_ptr(inversePVmatrix));
  glUniform1i(skyboxFarPlaneShaderProgram.skyboxSamplerLocation, 0);
  glUniform3f(skyboxFarPlaneShaderProgram.cameraPositionLocation, cameraPosition.x, cameraPosition.y, cameraPosition.z);
  glUniform1i(skyboxFarPlaneShaderProgram.foggyLocation, 0);
  glUniform1i(skyboxFarPlaneShaderProgram.fogSamplerLocation, 1);
  glUniform1f(skyboxFarPlaneShaderProgram.timeLocation, elapsedTime);

  if(day) {
    glBindVertexArray(skyboxDayGeometry->vertexArrayObject);
    glBindTexture(GL_TEXTURE_CUBE_MAP, skyboxDayGeometry->texture);
    glDrawArrays(GL_TRIANGLE_STRIP, 0, skyboxDayGeometry->numTriangles + 2);
  }
  else {
    glBindVertexArray(skyboxGeometry->vertexArrayObject);
    glBindTexture(GL_TEXTURE_CUBE_MAP, skyboxGeometry->texture);
    glDrawArrays(GL_TRIANGLE_STRIP, 0, skyboxGeometry->numTriangles + 2);
  }

  glUniform1i(shaderProgram.fogSamplerLocation, 1);
  glActiveTexture(GL_TEXTURE1);
  glBindTexture(GL_TEXTURE_2D, fogGeometry->texture); 
  
  glBindTexture(GL_TEXTURE_CUBE_MAP, 0);
  glBindTexture(GL_TEXTURE_2D, 0);
  glBindVertexArray(0);
  glUseProgram(0);

  glDisable(GL_BLEND);

  return;
}

void cleanupShaderPrograms(void) {
  pgr::deleteProgramAndShaders(shaderProgram.program);
  pgr::deleteProgramAndShaders(explosionShaderProgram.program);
  pgr::deleteProgramAndShaders(blueFireShaderProgram.program);
  pgr::deleteProgramAndShaders(bannerShaderProgram.program);
  pgr::deleteProgramAndShaders(skyboxFarPlaneShaderProgram.program);
  pgr::deleteProgramAndShaders(crosshairShaderProgram.program);
}

void initializeShaderPrograms(void) {
  std::vector<GLuint> shaderList;

  if(useLighting == true) {
    // push vertex shader and fragment shader
    shaderList.push_back(pgr::createShaderFromFile(GL_VERTEX_SHADER, "shaders/lightingPerVertex.vert"));
    shaderList.push_back(pgr::createShaderFromFile(GL_FRAGMENT_SHADER, "shaders/lightingPerVertex.frag"));

    // create the shader program with two shaders
    shaderProgram.program = pgr::createProgram(shaderList);

    // get vertex attributes locations, if the shader does not have this uniform -> return -1
    shaderProgram.posLocation      = glGetAttribLocation(shaderProgram.program, "position");
    shaderProgram.normalLocation   = glGetAttribLocation(shaderProgram.program, "normal");
    shaderProgram.texCoordLocation = glGetAttribLocation(shaderProgram.program, "texCoord");
    
    // get uniforms locations
    shaderProgram.PVMmatrixLocation    = glGetUniformLocation(shaderProgram.program, "PVMmatrix");
    shaderProgram.VmatrixLocation      = glGetUniformLocation(shaderProgram.program, "Vmatrix");
    shaderProgram.MmatrixLocation      = glGetUniformLocation(shaderProgram.program, "Mmatrix");
    shaderProgram.normalMatrixLocation = glGetUniformLocation(shaderProgram.program, "normalMatrix");
    shaderProgram.timeLocation         = glGetUniformLocation(shaderProgram.program, "time");
    shaderProgram.cameraPositionLocation = glGetUniformLocation(shaderProgram.program, "cameraPosition");
    
    // material
    shaderProgram.ambientLocation      = glGetUniformLocation(shaderProgram.program, "material.ambient");
    shaderProgram.diffuseLocation      = glGetUniformLocation(shaderProgram.program, "material.diffuse");
    shaderProgram.specularLocation     = glGetUniformLocation(shaderProgram.program, "material.specular");
    shaderProgram.shininessLocation    = glGetUniformLocation(shaderProgram.program, "material.shininess");
    
    // texture
    shaderProgram.texSamplerLocation   = glGetUniformLocation(shaderProgram.program, "texSampler");
    shaderProgram.fogSamplerLocation   = glGetUniformLocation(shaderProgram.program, "fogSampler");
    shaderProgram.mixSamplerLocation   = glGetUniformLocation(shaderProgram.program, "mixSampler");
    shaderProgram.useTextureLocation   = glGetUniformLocation(shaderProgram.program, "material.useTexture");
    shaderProgram.mixTextureLocation   = glGetUniformLocation(shaderProgram.program, "material.useMixTexture");

    // bools
    shaderProgram.foggyLocation = glGetUniformLocation(shaderProgram.program, "foggy");
    shaderProgram.transformationCheckLocation = glGetUniformLocation(shaderProgram.program, "transformationCheck");

    // reflector
    shaderProgram.reflectorPositionLocation  = glGetUniformLocation(shaderProgram.program, "reflectorPosition");
    shaderProgram.reflectorDirectionLocation = glGetUniformLocation(shaderProgram.program, "reflectorDirection");
    shaderProgram.reflectorOnLocation = glGetUniformLocation(shaderProgram.program, "reflectorOn");
  }
  else {
    // push vertex shader and fragment shader
    shaderProgram.program = createShaderProgram(colorVertexShaderSrc, colorFragmentShaderSrc);
    // get position and color attributes locations
    shaderProgram.posLocation   = glGetAttribLocation(shaderProgram.program, "position");
    shaderProgram.colorLocation = glGetAttribLocation(shaderProgram.program, "color");
    shaderProgram.normalLocation = glGetAttribLocation(shaderProgram.program, "normal");
    // get uniforms locations
    shaderProgram.PVMmatrixLocation = glGetUniformLocation(shaderProgram.program, "PVMmatrix");
  }

  // load and compile shader for explosions (dynamic texture)
  shaderList.clear();

  // push vertex shader and fragment shader
  shaderList.push_back(pgr::createShaderFromFile(GL_VERTEX_SHADER, "shaders/explosion.vert"));
  shaderList.push_back(pgr::createShaderFromFile(GL_FRAGMENT_SHADER, "shaders/explosion.frag"));
  explosionShaderProgram.program = pgr::createProgram(shaderList);
  // get position and texture coordinates attributes locations
  explosionShaderProgram.posLocation      = glGetAttribLocation(explosionShaderProgram.program, "position");
  explosionShaderProgram.texCoordLocation = glGetAttribLocation(explosionShaderProgram.program, "texCoord");
  // get uniforms locations
  explosionShaderProgram.PVMmatrixLocation     = glGetUniformLocation(explosionShaderProgram.program, "PVMmatrix");
  explosionShaderProgram.VmatrixLocation       = glGetUniformLocation(explosionShaderProgram.program, "Vmatrix");
  explosionShaderProgram.timeLocation          = glGetUniformLocation(explosionShaderProgram.program, "time");
  explosionShaderProgram.texSamplerLocation    = glGetUniformLocation(explosionShaderProgram.program, "texSampler");
  explosionShaderProgram.frameDurationLocation = glGetUniformLocation(explosionShaderProgram.program, "frameDuration");

  // load and compile shader for explosion (translation of texture coordinates)
  shaderList.clear();


  // Initialize blue flame shader
  // push vertex shader and fragment shader
  shaderList.push_back(pgr::createShaderFromFile(GL_VERTEX_SHADER, "shaders/blueFireShader.vert"));
  shaderList.push_back(pgr::createShaderFromFile(GL_FRAGMENT_SHADER, "shaders/blueFireShader.frag"));
  blueFireShaderProgram.program = pgr::createProgram(shaderList);
  // get position and texture coordinates attributes locations
  blueFireShaderProgram.posLocation      = glGetAttribLocation(blueFireShaderProgram.program, "position");
  blueFireShaderProgram.texCoordLocation = glGetAttribLocation(blueFireShaderProgram.program, "texCoord");
  // get uniforms locations
  blueFireShaderProgram.PVMmatrixLocation     = glGetUniformLocation(blueFireShaderProgram.program, "PVMmatrix");
  blueFireShaderProgram.VmatrixLocation       = glGetUniformLocation(blueFireShaderProgram.program, "Vmatrix");
  blueFireShaderProgram.timeLocation          = glGetUniformLocation(blueFireShaderProgram.program, "time");
  blueFireShaderProgram.texSamplerLocation    = glGetUniformLocation(blueFireShaderProgram.program, "texSampler");
  blueFireShaderProgram.frameDurationLocation = glGetUniformLocation(blueFireShaderProgram.program, "frameDuration");

  // load and compile shader for blue fire (translation of texture coordinates)
  shaderList.clear();


  // Initialize banner shader
  // push vertex shader and fragment shader
  shaderList.push_back(pgr::createShaderFromFile(GL_VERTEX_SHADER, "shaders/banner.vert"));
  shaderList.push_back(pgr::createShaderFromFile(GL_FRAGMENT_SHADER, "shaders/banner.frag"));
  // Create the program with two shaders
  bannerShaderProgram.program = pgr::createProgram(shaderList);
  // get position and color attributes locations
  bannerShaderProgram.posLocation      = glGetAttribLocation(bannerShaderProgram.program, "position");
  bannerShaderProgram.texCoordLocation = glGetAttribLocation(bannerShaderProgram.program, "texCoord");
  // get uniforms locations
  bannerShaderProgram.PVMmatrixLocation  = glGetUniformLocation(bannerShaderProgram.program, "PVMmatrix");
  bannerShaderProgram.timeLocation       = glGetUniformLocation(bannerShaderProgram.program, "time");
  bannerShaderProgram.texSamplerLocation = glGetUniformLocation(bannerShaderProgram.program, "texSampler");

  // load and compile shader for skybox (cube map)
  shaderList.clear();


  // Initialize skybox shader
  // push vertex shader and fragment shader
  skyboxFarPlaneShaderProgram.program = createShaderProgram(skyboxFarPlaneVertexShaderSrc, skyboxFarPlaneFragmentShaderSrc);

  // handles to vertex attributes locations
  skyboxFarPlaneShaderProgram.screenCoordLocation = glGetAttribLocation(skyboxFarPlaneShaderProgram.program, "screenCoord");
  // get uniforms locations
  skyboxFarPlaneShaderProgram.skyboxSamplerLocation   = glGetUniformLocation(skyboxFarPlaneShaderProgram.program, "skyboxSampler");
  skyboxFarPlaneShaderProgram.fogSamplerLocation = glGetUniformLocation(skyboxFarPlaneShaderProgram.program, "fogSampler");
  skyboxFarPlaneShaderProgram.cameraPositionLocation = glGetUniformLocation(skyboxFarPlaneShaderProgram.program, "cameraPosition");
  skyboxFarPlaneShaderProgram.foggyLocation = glGetUniformLocation(skyboxFarPlaneShaderProgram.program, "foggy");
  skyboxFarPlaneShaderProgram.inversePVmatrixLocation = glGetUniformLocation(skyboxFarPlaneShaderProgram.program, "inversePVmatrix");
  skyboxFarPlaneShaderProgram.timeLocation = glGetUniformLocation(skyboxFarPlaneShaderProgram.program, "time");
  
  // load and compile shader for skybox (cube map)
  shaderList.clear();


  // Initialize crosshair shader
  shaderList.push_back(pgr::createShaderFromFile(GL_VERTEX_SHADER, "shaders/crosshair.vert"));
  shaderList.push_back(pgr::createShaderFromFile(GL_FRAGMENT_SHADER, "shaders/crosshair.frag"));
  // Create the program with two shaders
  crosshairShaderProgram.program = pgr::createProgram(shaderList);
  // get position and color attributes locations
  crosshairShaderProgram.posLocation      = glGetAttribLocation(crosshairShaderProgram.program, "position");
  crosshairShaderProgram.texCoordLocation = glGetAttribLocation(crosshairShaderProgram.program, "texCoord");
  // get uniforms locations
  crosshairShaderProgram.PVMmatrixLocation  = glGetUniformLocation(crosshairShaderProgram.program, "PVMmatrix");
  crosshairShaderProgram.texSamplerLocation = glGetUniformLocation(crosshairShaderProgram.program, "texSampler");

  shaderList.clear();


  // Initialize HUD shader
  shaderList.push_back(pgr::createShaderFromFile(GL_VERTEX_SHADER, "shaders/hud.vert"));
  shaderList.push_back(pgr::createShaderFromFile(GL_FRAGMENT_SHADER, "shaders/hud.frag"));
  // Create the program with two shaders
  hudShaderProgram.program = pgr::createProgram(shaderList);
  // get position and color attributes locations
  hudShaderProgram.texCoordLocation = glGetAttribLocation(hudShaderProgram.program, "texCoord");
  // get uniforms locations
  hudShaderProgram.PVMmatrixLocation  = glGetUniformLocation(hudShaderProgram.program, "PVMmatrix");
  hudShaderProgram.texSamplerLocation = glGetUniformLocation(crosshairShaderProgram.program, "texSampler");
  hudShaderProgram.healthLocation = glGetUniformLocation(hudShaderProgram.program, "health");
  
  shaderList.clear();

  printf("Shaders initialized..\n");
}

/**
 * @brief Create a Mesh Geometry object for multimeshed objects
 * 
 * @param mesh 
 * @param material 
 * @param shader 
 * @param fileName 
 * @return MeshGeometry 
 */
MeshGeometry createMeshGeometry(
  aiMesh* mesh,
  aiMaterial* material,
  SCommonShaderProgram& shader,
  const std::string &fileName
  ) {  
  MeshGeometry geometry;
  
  // vertex buffer object, store all vertex positions and normals
  glGenBuffers(1, &((geometry).vertexBufferObject));
  glBindBuffer(GL_ARRAY_BUFFER, (geometry).vertexBufferObject);
  glBufferData(GL_ARRAY_BUFFER, 8*sizeof(float)*mesh->mNumVertices, 0, GL_STATIC_DRAW); // allocate memory for vertices, normals, and texture coordinates
  // first store all vertices
  glBufferSubData(GL_ARRAY_BUFFER, 0, 3*sizeof(float)*mesh->mNumVertices, mesh->mVertices);
  // then store all normals
  glBufferSubData(GL_ARRAY_BUFFER, 3*sizeof(float)*mesh->mNumVertices, 3*sizeof(float)*mesh->mNumVertices, mesh->mNormals);
  
  // just texture 0 for now
  float *textureCoords = new float[2 * mesh->mNumVertices];  // 2 floats per vertex
  float *currentTextureCoord = textureCoords;

  // copy texture coordinates
  aiVector3D vect;
    
  if(mesh->HasTextureCoords(0) ) {
    // we use 2D textures with 2 coordinates and ignore the third coordinate
    for(unsigned int idx=0; idx<mesh->mNumVertices; idx++) {
      vect = (mesh->mTextureCoords[0])[idx];
      *currentTextureCoord++ = vect.x;
      *currentTextureCoord++ = vect.y;
    }
  }

  // finally store all texture coordinates
  glBufferSubData(GL_ARRAY_BUFFER, 6*sizeof(float)*mesh->mNumVertices, 2*sizeof(float)*mesh->mNumVertices, textureCoords);

  // copy all mesh faces into one big array (assimp supports faces with ordinary number of vertices, we use only 3 -> triangles)
  unsigned int *indices = new unsigned int[mesh->mNumFaces * 3];
  for(unsigned int f = 0; f < mesh->mNumFaces; ++f) {
    indices[f*3 + 0] = mesh->mFaces[f].mIndices[0];
    indices[f*3 + 1] = mesh->mFaces[f].mIndices[1];
    indices[f*3 + 2] = mesh->mFaces[f].mIndices[2];
  }

  // copy our temporary index array to OpenGL and free the array
  glGenBuffers(1, &(geometry.elementBufferObject));
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, geometry.elementBufferObject);
  glBufferData(GL_ELEMENT_ARRAY_BUFFER, 3 * sizeof(unsigned) * mesh->mNumFaces, indices, GL_STATIC_DRAW);

  delete [] indices;

  // copy the material info to MeshGeometry structure
  const aiMaterial *mat  = material;
  aiColor4D color;
  aiString name;
  aiReturn retValue = AI_SUCCESS;

  // Get returns: aiReturn_SUCCESS 0 | aiReturn_FAILURE -1 | aiReturn_OUTOFMEMORY -3
  mat->Get(AI_MATKEY_NAME, name); // may be "" after the input mesh processing. Must be aiString type!

  if((retValue = aiGetMaterialColor(mat, AI_MATKEY_COLOR_DIFFUSE, &color)) != AI_SUCCESS)
    color = aiColor4D(0.0f, 0.0f, 0.0f, 0.0f);
  geometry.diffuse = glm::vec3(color.r, color.g, color.b);

  if ((retValue = aiGetMaterialColor(mat, AI_MATKEY_COLOR_AMBIENT, &color)) != AI_SUCCESS) {
    color = aiColor4D(0.0f, 0.0f, 0.0f, 0.0f);
  }
  geometry.ambient = glm::vec3(color.r / 2, color.g / 8, color.b / 8);

  if ((retValue = aiGetMaterialColor(mat, AI_MATKEY_COLOR_SPECULAR, &color)) != AI_SUCCESS)
    color = aiColor4D(0.0f, 0.0f, 0.0f, 0.0f);
  geometry.specular = glm::vec3(color.r, color.g, color.b);

  ai_real shininess, strength;
  unsigned int max;	// changed: to unsigned

  max = 1;	
  if ((retValue = aiGetMaterialFloatArray(mat, AI_MATKEY_SHININESS, &shininess, &max)) != AI_SUCCESS)
    shininess = 0.2f;
  max = 1;
  if((retValue = aiGetMaterialFloatArray(mat, AI_MATKEY_SHININESS_STRENGTH, &strength, &max)) != AI_SUCCESS)
    strength = 0.4f;
  geometry.shininess = shininess * strength;

  geometry.texture = 0;

  // load texture image
  if (mat->GetTextureCount(aiTextureType_DIFFUSE) > 0) {
    // get texture name 
    aiString path; // filename

    aiReturn texFound = mat->GetTexture(aiTextureType_DIFFUSE, 0, &path);
    std::string textureName = path.data;

    size_t found = fileName.find_last_of("/\\");
    // insert correct texture file path 
    if(found != std::string::npos) { // not found
      //subMesh_p->textureName.insert(0, "/");
      textureName.insert(0, fileName.substr(0, found+1));
    }

    std::cout << "Loading texture file: " << textureName << std::endl;
    geometry.texture = pgr::createTexture(textureName);
  }
  CHECK_GL_ERROR();

  glGenVertexArrays(1, &(geometry.vertexArrayObject));
  glBindVertexArray(geometry.vertexArrayObject);

  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, geometry.elementBufferObject); // bind our element array buffer (indices) to vao
  glBindBuffer(GL_ARRAY_BUFFER, geometry.vertexBufferObject);

  glEnableVertexAttribArray(shader.posLocation);
  glVertexAttribPointer(shader.posLocation, 3, GL_FLOAT, GL_FALSE, 0, 0);

  if(useLighting == true) {
    glEnableVertexAttribArray(shader.normalLocation);
    glVertexAttribPointer(shader.normalLocation, 3, GL_FLOAT, GL_FALSE, 0, (void*)(3 * sizeof(float) * mesh->mNumVertices));
  }
  else {
	  glDisableVertexAttribArray(shader.colorLocation);
	  glVertexAttrib3f(shader.colorLocation, color.r, color.g, color.b);
  }

  glEnableVertexAttribArray(shader.texCoordLocation);
  glVertexAttribPointer(shader.texCoordLocation, 2, GL_FLOAT, GL_FALSE, 0, (void*)(6 * sizeof(float) * mesh->mNumVertices));
  CHECK_GL_ERROR();

  glBindVertexArray(0);

  geometry.numTriangles = mesh->mNumFaces;
  
  return geometry;
}

/** Loader for multimeshed models.
 * 
 * @param fileName 
 * @param shader 
 * @param object 
 * @return true 
 * @return false 
 */
bool loadModel(
    const std::string &fileName, 
    SCommonShaderProgram& shader,
    std::vector<MeshGeometry>* geometryVec) 
{
  Assimp::Importer importer;
  importer.SetPropertyInteger(AI_CONFIG_PP_PTV_NORMALIZE, 1);

  const aiScene* scene = importer.ReadFile(fileName.c_str(), 0
    | aiProcess_Triangulate
    | aiProcess_PreTransformVertices
    | aiProcess_GenSmoothNormals
    | aiProcess_JoinIdenticalVertices);

  if(scene == NULL) {
      std::cerr << "Assimp error: " << importer.GetErrorString() << std::endl;
      // object.meshVec* = NULL;
      return false;
  }

  std::cout << "Meshes " << scene->mNumMeshes << std::endl;

  for(int i = 0; i < scene->mNumMeshes; ++i) {
    geometryVec->push_back(
      createMeshGeometry(
        scene->mMeshes[i], 
        scene->mMaterials[(scene->mMeshes[i])->mMaterialIndex],
        shader,
        fileName
      )
    );
  }
  return true;
}

/** Load mesh using assimp library
 *  Vertex, normals and texture coordinates data are stored without interleaving |VVVVV...|NNNNN...|tttt
 * \param fileName [in] file to open/load
 * \param shader [in] vao will connect loaded data to shader
 * \param geometry
 */
bool loadSingleMesh(const std::string &fileName, SCommonShaderProgram& shader, MeshGeometry** geometry) {
  Assimp::Importer importer;
  importer.SetPropertyInteger(AI_CONFIG_PP_PTV_NORMALIZE, 1);

  // Load asset from the file - you can play with various processing steps
  const aiScene * scn = importer.ReadFile(fileName.c_str(), 0
      | aiProcess_Triangulate             // Triangulate polygons (if any).
      | aiProcess_PreTransformVertices    // Transforms scene hierarchy into one root with geometry-leafs only. For more see Doc.
      | aiProcess_GenSmoothNormals        // Calculate normals per vertex.
      | aiProcess_JoinIdenticalVertices);

  if(scn == NULL) {
    std::cerr << "assimp error: " << importer.GetErrorString() << std::endl;
    *geometry = NULL;
    return false;
  }

  if(scn->mNumMeshes != 1) {
    std::cerr << "this simplified loader can only process files with only one mesh" << std::endl;
    *geometry = NULL;
    return false;
  }

  // in this phase we know we have one mesh in our loaded scene, we can directly copy its data to OpenGL ...
  const aiMesh * mesh = scn->mMeshes[0];

  *geometry = new MeshGeometry;

  // vertex buffer object, store all vertex positions and normals
  glGenBuffers(1, &((*geometry)->vertexBufferObject));
  glBindBuffer(GL_ARRAY_BUFFER, (*geometry)->vertexBufferObject);
  glBufferData(GL_ARRAY_BUFFER, 8*sizeof(float)*mesh->mNumVertices, 0, GL_STATIC_DRAW); // allocate memory for vertices, normals, and texture coordinates
  // first store all vertices
  glBufferSubData(GL_ARRAY_BUFFER, 0, 3*sizeof(float)*mesh->mNumVertices, mesh->mVertices);
  // then store all normals
  glBufferSubData(GL_ARRAY_BUFFER, 3*sizeof(float)*mesh->mNumVertices, 3*sizeof(float)*mesh->mNumVertices, mesh->mNormals);
  
  // just texture 0 for now
  float *textureCoords = new float[2 * mesh->mNumVertices];  // 2 floats per vertex
  float *currentTextureCoord = textureCoords;

  // copy texture coordinates
  aiVector3D vect;
    
  if(mesh->HasTextureCoords(0) ) {
    // we use 2D textures with 2 coordinates and ignore the third coordinate
    for(unsigned int idx=0; idx<mesh->mNumVertices; idx++) {
      vect = (mesh->mTextureCoords[0])[idx];
      *currentTextureCoord++ = vect.x;
      *currentTextureCoord++ = vect.y;
    }
  }
    
  // finally store all texture coordinates
  glBufferSubData(GL_ARRAY_BUFFER, 6*sizeof(float)*mesh->mNumVertices, 2*sizeof(float)*mesh->mNumVertices, textureCoords);

  // copy all mesh faces into one big array (assimp supports faces with ordinary number of vertices, we use only 3 -> triangles)
  unsigned int *indices = new unsigned int[mesh->mNumFaces * 3];
  for(unsigned int f = 0; f < mesh->mNumFaces; ++f) {
    indices[f*3 + 0] = mesh->mFaces[f].mIndices[0];
    indices[f*3 + 1] = mesh->mFaces[f].mIndices[1];
    indices[f*3 + 2] = mesh->mFaces[f].mIndices[2];
  }

  // copy our temporary index array to OpenGL and free the array
  glGenBuffers(1, &((*geometry)->elementBufferObject));
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, (*geometry)->elementBufferObject);
  glBufferData(GL_ELEMENT_ARRAY_BUFFER, 3 * sizeof(unsigned) * mesh->mNumFaces, indices, GL_STATIC_DRAW);

  delete [] indices;

  // copy the material info to MeshGeometry structure
  const aiMaterial *mat  = scn->mMaterials[mesh->mMaterialIndex];
  aiColor4D color;
  aiString name;
  aiReturn retValue = AI_SUCCESS;

  // Get returns: aiReturn_SUCCESS 0 | aiReturn_FAILURE -1 | aiReturn_OUTOFMEMORY -3
  mat->Get(AI_MATKEY_NAME, name); // may be "" after the input mesh processing. Must be aiString type!

  if((retValue = aiGetMaterialColor(mat, AI_MATKEY_COLOR_DIFFUSE, &color)) != AI_SUCCESS)
    color = aiColor4D(0.0f, 0.0f, 0.0f, 0.0f);
  (*geometry)->diffuse = glm::vec3(color.r, color.g, color.b);

  if ((retValue = aiGetMaterialColor(mat, AI_MATKEY_COLOR_AMBIENT, &color)) != AI_SUCCESS)
    color = aiColor4D(0.0f, 0.0f, 0.0f, 0.0f);
  (*geometry)->ambient = glm::vec3(color.r, color.g, color.b);

  if ((retValue = aiGetMaterialColor(mat, AI_MATKEY_COLOR_SPECULAR, &color)) != AI_SUCCESS)
    color = aiColor4D(0.0f, 0.0f, 0.0f, 0.0f);
  (*geometry)->specular = glm::vec3(color.r, color.g, color.b);

  ai_real shininess, strength;
  unsigned int max;	// changed: to unsigned

  max = 1;	
  if ((retValue = aiGetMaterialFloatArray(mat, AI_MATKEY_SHININESS, &shininess, &max)) != AI_SUCCESS)
    shininess = 1.0f;
  max = 1;
  if((retValue = aiGetMaterialFloatArray(mat, AI_MATKEY_SHININESS_STRENGTH, &strength, &max)) != AI_SUCCESS)
    strength = 1.0f;
  (*geometry)->shininess = shininess * strength;

  (*geometry)->texture = 0;

  // load texture image
  if (mat->GetTextureCount(aiTextureType_DIFFUSE) > 0) {
    // get texture name 
    aiString path; // filename

    aiReturn texFound = mat->GetTexture(aiTextureType_DIFFUSE, 0, &path);
    std::string textureName = path.data;

    size_t found = fileName.find_last_of("/\\");
    // insert correct texture file path 
    if(found != std::string::npos) { // not found
      //subMesh_p->textureName.insert(0, "/");
      textureName.insert(0, fileName.substr(0, found+1));
    }

    std::cout << "Loading texture file: " << textureName << std::endl;
    (*geometry)->texture = pgr::createTexture(textureName);
  }
  CHECK_GL_ERROR();

  glGenVertexArrays(1, &((*geometry)->vertexArrayObject));
  glBindVertexArray((*geometry)->vertexArrayObject);

  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, (*geometry)->elementBufferObject); // bind our element array buffer (indices) to vao
  glBindBuffer(GL_ARRAY_BUFFER, (*geometry)->vertexBufferObject);

  glEnableVertexAttribArray(shader.posLocation);
  glVertexAttribPointer(shader.posLocation, 3, GL_FLOAT, GL_FALSE, 0, 0);

  if(useLighting == true) {
    glEnableVertexAttribArray(shader.normalLocation);
    glVertexAttribPointer(shader.normalLocation, 3, GL_FLOAT, GL_FALSE, 0, (void*)(3 * sizeof(float) * mesh->mNumVertices));
  }
  else {
	  glDisableVertexAttribArray(shader.colorLocation);
	  // following line is problematic on AMD/ATI graphic cards
	  // -> if you see black screen (no objects at all) than try to set color manually in vertex shader to see at least something
    glVertexAttrib3f(shader.colorLocation, color.r, color.g, color.b);
  }

  glEnableVertexAttribArray(shader.texCoordLocation);
  glVertexAttribPointer(shader.texCoordLocation, 2, GL_FLOAT, GL_FALSE, 0, (void*)(6 * sizeof(float) * mesh->mNumVertices));
  CHECK_GL_ERROR();

  glBindVertexArray(0);

  (*geometry)->numTriangles = mesh->mNumFaces;
  glBindTexture(GL_TEXTURE_2D, 0);

  return true;
}

/**
 * @brief Deprecated function.
 * 
 * @param shader 
 * @param geometry 
 * @param hitbox 
 */
void initHitBoxGeometry(SCommonShaderProgram &shader, MeshGeometry** geometry, HitBox &hitbox) {
  float dx = hitbox.width/2;
  float dy = hitbox.length/2;
  float dz = hitbox.height/2;

  std::cout << dx << " " << dy << " " << dz << std::endl;
  
  float hitboxVertices[] = {
      //TOP
      -dx,  dy,   dz, // 0
      dx,   dy,   dz, // 1
      dx,   -dy,  dz, // 2
      -dx,  -dy,  dz, // 3
      //BOTTOM
      -dx,  dy,   -dz, // 4
      dx,   dy,   -dz, // 5
      dx,   -dy,  -dz, // 6
      -dx,  -dy,  -dz, // 7
  };

  std::cout << hitboxVertices[3 * 6 + 1] << std::endl;

  float hitVertices[6 * 2 * 3 * 3 * 3];

  std::cout << sizeof(hitVertices) << " ? " << sizeof(hitboxTriangles) << std::endl;

  for(int i = 0; i < sizeof(hitboxTriangles); ++i) {
      int index = hitboxTriangles[i];
      int mod = i % 3;
      int curIndex = 3 * i;
      int curIndexIndex = 3 * index;

      if(mod == 0) {
          hitVertices[curIndex] = hitboxVertices[curIndexIndex];
          hitVertices[curIndex + 1] = hitboxVertices[curIndexIndex + 1];
          hitVertices[curIndex + 2] = hitboxVertices[curIndexIndex + 2];
      }

      if(mod == 1) {
          hitVertices[curIndex] = colors[curIndexIndex];
          hitVertices[curIndex + 1] = colors[curIndexIndex + 1];
          hitVertices[curIndex + 2] = colors[curIndexIndex + 2];
      }
      
      else {
          hitVertices[curIndex] = normals[curIndexIndex];
          hitVertices[curIndex + 1] = normals[curIndexIndex + 1];
          hitVertices[curIndex + 2] = normals[curIndexIndex + 2];
      }

      std::cout << "hitboxGeom " << hitVertices[curIndex] << " " << hitVertices[curIndex + 1] << " " << hitVertices[curIndex + 2] << std::endl;
  }
  
  glGenVertexArrays(1, &((*geometry)->vertexArrayObject));
  glBindVertexArray((*geometry)->vertexArrayObject);

  glGenBuffers(1, &((*geometry)->vertexBufferObject));
  glBindBuffer(GL_ARRAY_BUFFER, (*geometry)->vertexBufferObject);
  glBufferData(GL_ARRAY_BUFFER, sizeof(hitVertices), hitVertices, GL_STATIC_DRAW);

  glEnableVertexAttribArray(shader.posLocation);
  // vertices of triangles - start at the beginning of the array
  glVertexAttribPointer(shader.posLocation, 3, GL_FLOAT, GL_FALSE, 9 * sizeof(float), 0);

  if(useLighting == false) {
    glEnableVertexAttribArray(shader.colorLocation);
    // color of vertex starts after the position (interlaced arrays)
    glVertexAttribPointer(shader.colorLocation, 3, GL_FLOAT, GL_FALSE, 9 * sizeof(float), (void*)(3 * sizeof(float)));
  }
  else {
    glEnableVertexAttribArray(shader.normalLocation);
    // normal of vertex starts after the color (interlaced array)
    glVertexAttribPointer(shader.normalLocation, 3, GL_FLOAT, GL_FALSE, 9 * sizeof(float), (void*)(6 * sizeof(float)));
  }

  (*geometry)->ambient = glm::vec3(1.0f, 0.0f, 1.0f);
  (*geometry)->diffuse = glm::vec3(1.0f, 0.0f, 1.0f);
  (*geometry)->specular = glm::vec3(1.0f, 0.0f, 1.0f);
  (*geometry)->shininess = 5.0f;
  (*geometry)->texture = 0;

  glBindVertexArray(0);

  (*geometry)->numTriangles = hitboxTriangleCount;
  
  return;
}

/**
 * @brief Initialize ground geometry.
 * 
 * @param shader 
 * @param geometry 
 * @param ground 
 */
void initGroundGeometry(SCommonShaderProgram &shader, MeshGeometry** geometry, Ground* ground) {
  *geometry = new MeshGeometry;
  float groundVertices[ground->getTriangleCount() * 9 * 3];

  //Generates triangles out of the Ground vertexes
  int g = 0;
  int threeY = (ground->fragY + 1) * 3;
  int redFY = ground->fragY;
  int redFX = ground->fragX;

  for (int i = 0; i < redFY; ++i) {
    for (int j = 0; j < redFX; ++j) {
      
      int index = (j * 3) + (i * (ground->fragY + 1) * 3);

      // Create close triangle
        //Right upper point
      groundVertices[g++] = ground->vertices[index + 3]; //std::cout << ground->vertices[index + 3] << " ";
      groundVertices[g++] = ground->vertices[index + 4]; //std::cout << ground->vertices[index + 4] << " ";
      groundVertices[g++] = ground->vertices[index + 5]; //std::cout << ground->vertices[index + 5] << std::endl;
      groundVertices[g++] = ground->color[0];
      groundVertices[g++] = ground->color[1];
      groundVertices[g++] = ground->color[2];
      groundVertices[g++] = 0.0f;
      groundVertices[g++] = 0.0f;
      groundVertices[g++] = 1.0f;
        //Left upper point
      groundVertices[g++] = ground->vertices[index]; //std::cout << "Triangle: " << ground->vertices[index] << " ";
      groundVertices[g++] = ground->vertices[index + 1]; //std::cout << ground->vertices[index + 1] << " ";
      groundVertices[g++] = ground->vertices[index + 2]; //std::cout << ground->vertices[index + 2] << std::endl;
      groundVertices[g++] = ground->color[0];
      groundVertices[g++] = ground->color[1];
      groundVertices[g++] = ground->color[2];
      groundVertices[g++] = 0.0f;
      groundVertices[g++] = 0.0f;
      groundVertices[g++] = 1.0f;
        //Right lower point
      groundVertices[g++] = ground->vertices[index + threeY + 3]; //std::cout << ground->vertices[index + threeY + 3] << " ";
      groundVertices[g++] = ground->vertices[index + threeY + 4]; //std::cout << ground->vertices[index + threeY + 4] << " ";
      groundVertices[g++] = ground->vertices[index + threeY + 5]; //std::cout << ground->vertices[index + threeY + 5] << std::endl;
      groundVertices[g++] = ground->color[0];
      groundVertices[g++] = ground->color[1];
      groundVertices[g++] = ground->color[2];
      groundVertices[g++] = 0.0f;
      groundVertices[g++] = 0.0f;
      groundVertices[g++] = 1.0f;

      // Create further triangle
        //Left upper point
      groundVertices[g++] = ground->vertices[index]; //std::cout << "Triangle: " << ground->vertices[index] << " ";
      groundVertices[g++] = ground->vertices[index + 1]; //std::cout << ground->vertices[index + 1] << " ";
      groundVertices[g++] = ground->vertices[index + 2]; //std::cout << ground->vertices[index + 2] << std::endl;
      groundVertices[g++] = ground->color[0];
      groundVertices[g++] = ground->color[1];
      groundVertices[g++] = ground->color[2];
      groundVertices[g++] = 0.0f;
      groundVertices[g++] = 0.0f;
      groundVertices[g++] = 1.0f;
        //Left lower point
      groundVertices[g++] = ground->vertices[index + threeY]; //std::cout << ground->vertices[index + threeY] << " ";
      groundVertices[g++] = ground->vertices[index + threeY + 1]; //std::cout << ground->vertices[index + threeY + 1] << " ";
      groundVertices[g++] = ground->vertices[index + threeY + 2]; //std::cout << ground->vertices[index + threeY + 2] << std::endl;
      groundVertices[g++] = ground->color[0];
      groundVertices[g++] = ground->color[1];
      groundVertices[g++] = ground->color[2];
      groundVertices[g++] = 0.0f;
      groundVertices[g++] = 0.0f;
      groundVertices[g++] = 1.0f;
        //Right lower point
      groundVertices[g++] = ground->vertices[index + threeY + 3]; //std::cout << ground->vertices[index + threeY + 3] << " ";
      groundVertices[g++] = ground->vertices[index + threeY + 4]; //std::cout << ground->vertices[index + threeY + 4] << " ";
      groundVertices[g++] = ground->vertices[index + threeY + 5]; //std::cout << ground->vertices[index + threeY + 5] << std::endl;
      groundVertices[g++] = ground->color[0];
      groundVertices[g++] = ground->color[1];
      groundVertices[g++] = ground->color[2];
      groundVertices[g++] = 0.0f;
      groundVertices[g++] = 0.0f;
      groundVertices[g++] = 1.0f;
    }
  }

  glGenVertexArrays(1, &((*geometry)->vertexArrayObject));
  glBindVertexArray((*geometry)->vertexArrayObject);

  glGenBuffers(1, &((*geometry)->vertexBufferObject));
  glBindBuffer(GL_ARRAY_BUFFER, (*geometry)->vertexBufferObject);
  glBufferData(GL_ARRAY_BUFFER, sizeof(groundVertices), groundVertices, GL_STATIC_DRAW);

  glEnableVertexAttribArray(shader.posLocation);
  // vertices of triangles - start at the beginning of the array
  glVertexAttribPointer(shader.posLocation, 3, GL_FLOAT, GL_FALSE, 9 * sizeof(float), 0);
  
  if(useLighting == false) {
    glEnableVertexAttribArray(shader.colorLocation);
    // color of vertex starts after the position (interlaced arrays)
    glVertexAttribPointer(shader.colorLocation, 3, GL_FLOAT, GL_FALSE, 9 * sizeof(float), (void*)(3 * sizeof(float)));
  }
  else {
    glEnableVertexAttribArray(shader.normalLocation);
    // normal of vertex starts after the color (interlaced array)
    glVertexAttribPointer(shader.normalLocation, 3, GL_FLOAT, GL_FALSE, 9 * sizeof(float), (void*)(6 * sizeof(float)));
  }
  
  glEnableVertexAttribArray(shader.texCoordLocation);
  glVertexAttribPointer(shaderProgram.texCoordLocation, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)(3 * sizeof(float)));
  CHECK_GL_ERROR();

  (*geometry)->ambient = glm::vec3(0.2f);
  (*geometry)->diffuse = glm::vec3(0.5f);
  (*geometry)->specular = glm::vec3(0.4f);
  (*geometry)->shininess = 0.6f;
  (*geometry)->texture = pgr::createTexture(GROUND_TEXTURE_NAME);
  (*geometry)->mixTexture = pgr::createTexture(GRASS_TEXTURE_NAME);

  glBindVertexArray(0);

  (*geometry)->numTriangles = ground->getTriangleCount();

  glBindTexture(GL_TEXTURE_2D, 0);
  
  return;
}

/**
 * @brief Initializes Billboard geometry.
 * 
 * @param shader 
 * @param geometryVec 
 */
void initBillboardGeometry(SCommonShaderProgram &shader, std::vector<MeshGeometry> *geometryVec) {
  MeshGeometry geometry;
  float bbVertices[billboardTrianglesCount * 9 * 3];

  //Parses vertices info from data.h
  for (int i = 0; i < billboardTrianglesCount * 9; ++i) {
    int index = billboardTriangles[i];
    int mod = i % 3;
    int curIndex = 3 * i;
    int curIndexIndex = 3 * index;

    if(mod == 0) {
      bbVertices[curIndex] = billboardVertices[curIndexIndex];
      bbVertices[curIndex + 1] = billboardVertices[curIndexIndex + 1];
      bbVertices[curIndex + 2] = billboardVertices[curIndexIndex + 2];
    }

    else if(mod == 1) {
      bbVertices[curIndex] = colors[curIndexIndex];
      bbVertices[curIndex + 1] = colors[curIndexIndex + 1];
      bbVertices[curIndex + 2] = colors[curIndexIndex + 2];
    }
    
    else {
      bbVertices[curIndex] = normals[curIndexIndex];
      bbVertices[curIndex + 1] = normals[curIndexIndex + 1];
      bbVertices[curIndex + 2] = normals[curIndexIndex + 2];
    }
  }

  glGenVertexArrays(1, &((geometry).vertexArrayObject));
  glBindVertexArray((geometry).vertexArrayObject);

  glGenBuffers(1, &((geometry).vertexBufferObject));
  glBindBuffer(GL_ARRAY_BUFFER, (geometry).vertexBufferObject);
  glBufferData(GL_ARRAY_BUFFER, sizeof(bbVertices), bbVertices, GL_STATIC_DRAW);

  glEnableVertexAttribArray(shader.posLocation);
  // vertices of triangles - start at the beginning of the array
  glVertexAttribPointer(shader.posLocation, 3, GL_FLOAT, GL_FALSE, 9 * sizeof(float), 0);

  if(useLighting == false) {
    glEnableVertexAttribArray(shader.colorLocation);
    // color of vertex starts after the position (interlaced arrays)
    glVertexAttribPointer(shader.colorLocation, 3, GL_FLOAT, GL_FALSE, 9 * sizeof(float), (void*)(3 * sizeof(float)));
  }
  else {
    glEnableVertexAttribArray(shader.normalLocation);
    // normal of vertex starts after the color (interlaced array)
    glVertexAttribPointer(shader.normalLocation, 3, GL_FLOAT, GL_FALSE, 9 * sizeof(float), (void*)(6 * sizeof(float)));
  }

  (geometry).ambient = glm::vec3(0.1f);
  (geometry).diffuse = glm::vec3(0.2f, 0.4f, 0.2f);
  (geometry).specular = glm::vec3(0.7f, 0.4f, 0.0f);
  (geometry).shininess = 0.25f;
  (geometry).texture = 0;
  (geometry).mixTexture = 0;

  glBindVertexArray(0);

  (geometry).numTriangles = billboardTrianglesCount;

  geometryVec->push_back(geometry);
}

void initBannerGeometry(GLuint shader, MeshGeometry **geometry) {

  *geometry = new MeshGeometry;
  
  (*geometry)->texture = pgr::createTexture(BANNER_TEXTURE_NAME);
  (*geometry)->mixTexture = 0;

  glUniform1i(glGetUniformLocation(bannerShaderProgram.program, "texSampler"), 0);
  glBindTexture(GL_TEXTURE_2D, (*geometry)->texture);

  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);

  glGenVertexArrays(1, &((*geometry)->vertexArrayObject));
  glBindVertexArray((*geometry)->vertexArrayObject);

  glGenBuffers(1, &((*geometry)->vertexBufferObject));
  glBindBuffer(GL_ARRAY_BUFFER, (*geometry)->vertexBufferObject);
  glBufferData(GL_ARRAY_BUFFER, sizeof(bannerVertexData), bannerVertexData, GL_STATIC_DRAW);

  glEnableVertexAttribArray(bannerShaderProgram.posLocation);
  glEnableVertexAttribArray(bannerShaderProgram.texCoordLocation);
  // vertices of triangles - start at the beginning of the interlaced array
  glVertexAttribPointer(bannerShaderProgram.posLocation, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(float), 0);
  // texture coordinates of each vertices are stored just after its position
  glVertexAttribPointer(bannerShaderProgram.texCoordLocation, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)(3 * sizeof(float)));

  glBindVertexArray(0);

  (*geometry)->numTriangles = bannerNumQuadVertices;
}

void initExplosionGeometry(GLuint shader, MeshGeometry **geometry) {

  *geometry = new MeshGeometry;

  (*geometry)->texture = pgr::createTexture(EXPLOSION_TEXTURE_NAME);
  (*geometry)->mixTexture = 0;

  glGenVertexArrays(1, &((*geometry)->vertexArrayObject));
  glBindVertexArray((*geometry)->vertexArrayObject);

  glGenBuffers(1, &((*geometry)->vertexBufferObject));\
  glBindBuffer(GL_ARRAY_BUFFER, (*geometry)->vertexBufferObject);
  glBufferData(GL_ARRAY_BUFFER, sizeof(explosionVertexData), explosionVertexData, GL_STATIC_DRAW);

  glEnableVertexAttribArray(explosionShaderProgram.posLocation);
  // vertices of triangles - start at the beginning of the array (interlaced array)
  glVertexAttribPointer(explosionShaderProgram.posLocation, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(float), 0);

  glEnableVertexAttribArray(explosionShaderProgram.texCoordLocation);
  // texture coordinates are placed just after the position of each vertex (interlaced array)
  glVertexAttribPointer(explosionShaderProgram.texCoordLocation, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)(3 * sizeof(float)));

  glBindVertexArray(0);

  (*geometry)->numTriangles = explosionNumQuadVertices;
}

void initBlueFireGeometry(GLuint shader, MeshGeometry **geometry) {

  *geometry = new MeshGeometry;

  (*geometry)->texture = pgr::createTexture(BLUE_FIRE_TEXTURE_NAME);
  (*geometry)->mixTexture = 0;

  glGenVertexArrays(1, &((*geometry)->vertexArrayObject));
  glBindVertexArray((*geometry)->vertexArrayObject);

  glGenBuffers(1, &((*geometry)->vertexBufferObject));\
  glBindBuffer(GL_ARRAY_BUFFER, (*geometry)->vertexBufferObject);
  glBufferData(GL_ARRAY_BUFFER, sizeof(blueFireVertexData), blueFireVertexData, GL_STATIC_DRAW);

  glEnableVertexAttribArray(blueFireShaderProgram.posLocation);
  // vertices of triangles - start at the beginning of the array (interlaced array)
  glVertexAttribPointer(blueFireShaderProgram.posLocation, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(float), 0);

  glEnableVertexAttribArray(blueFireShaderProgram.texCoordLocation);
  // texture coordinates are placed just after the position of each vertex (interlaced array)
  glVertexAttribPointer(blueFireShaderProgram.texCoordLocation, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)(3 * sizeof(float)));

  glBindVertexArray(0);

  (*geometry)->numTriangles = blueFireNumQuadVertices;
}


void initSkyboxGeometry(GLuint shader, MeshGeometry **geometry, bool day) {
  *geometry = new MeshGeometry;

  // 2D coordinates of 2 triangles covering the whole screen (NDC), draw using triangle strip    
  static const float screenCoords[] = {
   -1.0f, -1.0f,
    1.0f, -1.0f,
   -1.0f,  1.0f,
    1.0f,  1.0f
  };

  glGenVertexArrays(1, &((*geometry)->vertexArrayObject));
  glBindVertexArray((*geometry)->vertexArrayObject);

  // buffer for far plane rendering
  glGenBuffers(1, &((*geometry)->vertexBufferObject));
  glBindBuffer(GL_ARRAY_BUFFER, (*geometry)->vertexBufferObject);
  glBufferData(GL_ARRAY_BUFFER, sizeof(screenCoords), screenCoords, GL_STATIC_DRAW);

  glEnableVertexAttribArray(skyboxFarPlaneShaderProgram.screenCoordLocation);
  glVertexAttribPointer(skyboxFarPlaneShaderProgram.screenCoordLocation, 2, GL_FLOAT, GL_FALSE, 0, 0);

  glBindVertexArray(0);
  glUseProgram(0);
  CHECK_GL_ERROR();

  (*geometry)->numTriangles = 2;

  glGenTextures(1, &((*geometry)->texture));
  glBindTexture(GL_TEXTURE_CUBE_MAP, (*geometry)->texture);

  const char * suffixes[] = { "posx", "negx", "posy", "negy", "posz", "negz" };
  GLuint targets[] = {
    GL_TEXTURE_CUBE_MAP_POSITIVE_X, GL_TEXTURE_CUBE_MAP_NEGATIVE_X,
    GL_TEXTURE_CUBE_MAP_POSITIVE_Y, GL_TEXTURE_CUBE_MAP_NEGATIVE_Y,
    GL_TEXTURE_CUBE_MAP_POSITIVE_Z, GL_TEXTURE_CUBE_MAP_NEGATIVE_Z
  };

  for(int i = 0; i < 6; i++ ) {
    std::string texName = (!day) ? std::string(SKYBOX_CUBE_TEXTURE_FILE_PREFIX) + "_" + suffixes[i] + ".jpg" :
      std::string(SKYBOX_DAY_CUBE2_TEXTURE_FILE_PREFIX) + suffixes[i] + ".png";
    std::cout << "Loading cube map texture: " << texName << std::endl;
    if(!pgr::loadTexImage2D(texName, targets[i])) {
      pgr::dieWithError("Skybox cube map loading failed!");
    }
  }

  glTexParameterf(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  glTexParameterf(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
  glGenerateMipmap(GL_TEXTURE_CUBE_MAP);

  // unbind the texture (just in case someone will mess up with texture calls later)
  glBindTexture(GL_TEXTURE_CUBE_MAP, 0);
  CHECK_GL_ERROR();
}

void initCrosshairGeometry(GLuint shader, MeshGeometry **geometry) {
  *geometry = new MeshGeometry;
  
  (*geometry)->texture = pgr::createTexture(CROSSHAIR_TEXTURE_NAME);

  glBindTexture(GL_TEXTURE_2D, (*geometry)->texture);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);

  (*geometry)->mixTexture = 0;

  glGenVertexArrays(1, &((*geometry)->vertexArrayObject));
  glBindVertexArray((*geometry)->vertexArrayObject);

  glGenBuffers(1, &((*geometry)->vertexBufferObject));
  glBindBuffer(GL_ARRAY_BUFFER, (*geometry)->vertexBufferObject);
  glBufferData(GL_ARRAY_BUFFER, sizeof(crosshairVertexData), crosshairVertexData, GL_STATIC_DRAW);

  glEnableVertexAttribArray(crosshairShaderProgram.posLocation);
  glEnableVertexAttribArray(crosshairShaderProgram.texCoordLocation);
  // vertices of triangles - start at the beginning of the interlaced array
  glVertexAttribPointer(crosshairShaderProgram.posLocation, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(float), 0);
  // texture coordinates of each vertices are stored just after its position
  glVertexAttribPointer(crosshairShaderProgram.texCoordLocation, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)(3 * sizeof(float)));

  glBindVertexArray(0);

  (*geometry)->numTriangles = crosshairNumQuadVertices;
  glBindTexture(GL_TEXTURE_2D, 0);
}

void initHUDGeometry(GLuint shader, MeshGeometry **geometry) {
  *geometry = new MeshGeometry;
  
  (*geometry)->texture = pgr::createTexture(BLOOD_TEXTURE_NAME);

  glBindTexture(GL_TEXTURE_2D, (*geometry)->texture);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);

  (*geometry)->mixTexture = 0;

  glGenVertexArrays(1, &((*geometry)->vertexArrayObject));
  glBindVertexArray((*geometry)->vertexArrayObject);

  glEnableVertexAttribArray(hudShaderProgram.posLocation);
  glGenBuffers(1, &((*geometry)->vertexBufferObject));
  glBindBuffer(GL_ARRAY_BUFFER, (*geometry)->vertexBufferObject);
  glBufferData(GL_ARRAY_BUFFER, sizeof(bloodVertexData), bloodVertexData, GL_STATIC_DRAW);

  glEnableVertexAttribArray(hudShaderProgram.texCoordLocation);
  // texture coordinates of each vertices are stored just after its position
  glVertexAttribPointer(hudShaderProgram.texCoordLocation, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)(3 * sizeof(float)));

  glBindVertexArray(0);

  (*geometry)->numTriangles = bloodNumQuadVertices;
  glBindTexture(GL_TEXTURE_2D, 0);
}

void initAlphabetGeometries(
  GLuint shader,
  std::map<char, MeshGeometry*> *alphabetCharacters, 
  MeshGeometry **ubuntu, 
  MeshGeometry **heart
) {
  MeshGeometry* geometry;
  (*ubuntu) = new MeshGeometry;
  (*heart) = new MeshGeometry;

  // LOAD ALPHABET
  std::string alphabet = "abcdefghijklmnopqrstuvwxyz";
  std::string::const_iterator c;
  
  for (c = alphabet.begin(); c != alphabet.end(); c++) {
    geometry = new MeshGeometry;

    (*geometry).texture = pgr::createTexture(std::string(FONT_PATH) + (*c) + ".png");

    glBindTexture(GL_TEXTURE_2D, (*geometry).texture);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);

    glGenVertexArrays(1, &((*geometry).vertexArrayObject));
    glBindVertexArray((*geometry).vertexArrayObject);

    glGenBuffers(1, &((*geometry).vertexBufferObject));
    glBindBuffer(GL_ARRAY_BUFFER, (*geometry).vertexBufferObject);
    glBufferData(GL_ARRAY_BUFFER, sizeof(crosshairVertexData), crosshairVertexData, GL_STATIC_DRAW);

    glEnableVertexAttribArray(crosshairShaderProgram.posLocation);
    glEnableVertexAttribArray(crosshairShaderProgram.texCoordLocation);
    // vertices of triangles - start at the beginning of the interlaced array
    glVertexAttribPointer(crosshairShaderProgram.posLocation, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(float), 0);
    // texture coordinates of each vertices are stored just after its position
    glVertexAttribPointer(crosshairShaderProgram.texCoordLocation, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)(3 * sizeof(float)));

    glBindVertexArray(0);

    (*geometry).numTriangles = crosshairNumQuadVertices;

    (*alphabetCharacters).insert(std::pair<char, MeshGeometry*>(*c, geometry));

    glBindTexture(GL_TEXTURE_2D, 0);
  }

  // LOAD UBUNTU SYMBOL
  (*ubuntu)->texture = pgr::createTexture(std::string(FONT_PATH) + "ubuntu.png");

  glBindTexture(GL_TEXTURE_2D, (*ubuntu)->texture);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
  glGenVertexArrays(1, &((*ubuntu)->vertexArrayObject));
  glBindVertexArray((*ubuntu)->vertexArrayObject);

  glGenBuffers(1, &((*ubuntu)->vertexBufferObject));
  glBindBuffer(GL_ARRAY_BUFFER, (*ubuntu)->vertexBufferObject);
  glBufferData(GL_ARRAY_BUFFER, sizeof(crosshairVertexData), crosshairVertexData, GL_STATIC_DRAW);

  glEnableVertexAttribArray(crosshairShaderProgram.posLocation);
  glEnableVertexAttribArray(crosshairShaderProgram.texCoordLocation);
  
  glVertexAttribPointer(crosshairShaderProgram.posLocation, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(float), 0);
  glVertexAttribPointer(crosshairShaderProgram.texCoordLocation, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)(3 * sizeof(float)));

  glBindVertexArray(0);

  (*ubuntu)->numTriangles = crosshairNumQuadVertices;
  glBindTexture(GL_TEXTURE_2D, 0);

  // LOAD HEART SYMBOL
  (*heart)->texture = pgr::createTexture(std::string(FONT_PATH) + "heart.png");

  glBindTexture(GL_TEXTURE_2D, (*heart)->texture);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);

  glGenVertexArrays(1, &((*heart)->vertexArrayObject));
  glBindVertexArray((*heart)->vertexArrayObject);

  glGenBuffers(1, &((*heart)->vertexBufferObject));
  glBindBuffer(GL_ARRAY_BUFFER, (*heart)->vertexBufferObject);
  glBufferData(GL_ARRAY_BUFFER, sizeof(crosshairVertexData), crosshairVertexData, GL_STATIC_DRAW);

  glEnableVertexAttribArray(crosshairShaderProgram.posLocation);
  glEnableVertexAttribArray(crosshairShaderProgram.texCoordLocation);

  glVertexAttribPointer(crosshairShaderProgram.posLocation, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(float), 0);
  glVertexAttribPointer(crosshairShaderProgram.texCoordLocation, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)(3 * sizeof(float)));

  glBindVertexArray(0);

  (*heart)->numTriangles = crosshairNumQuadVertices;
  glBindTexture(GL_TEXTURE_2D, 0);
}

void initFogGeometry(GLuint shader, MeshGeometry **geometry) {
  *geometry = new MeshGeometry;
  
  (*geometry)->texture = pgr::createTexture(FOG_TEXTURE_NAME);
  glBindTexture(GL_TEXTURE_2D, (*geometry)->texture);

  glGenVertexArrays(1, &((*geometry)->vertexArrayObject));
  glBindVertexArray((*geometry)->vertexArrayObject);

  glGenBuffers(1, &((*geometry)->vertexBufferObject));
  glBindBuffer(GL_ARRAY_BUFFER, (*geometry)->vertexBufferObject);
  glBufferData(GL_ARRAY_BUFFER, sizeof(fogVertexData), fogVertexData, GL_STATIC_DRAW);

  glEnableVertexAttribArray(fogShaderProgram.posLocation);
  glEnableVertexAttribArray(fogShaderProgram.texCoordLocation);
  // vertices of triangles - start at the beginning of the interlaced array
  glVertexAttribPointer(fogShaderProgram.posLocation, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(float), 0);
  // texture coordinates of each vertices are stored just after its position
  glVertexAttribPointer(fogShaderProgram.texCoordLocation, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)(3 * sizeof(float)));

  glBindVertexArray(0);

  (*geometry)->numTriangles = fogNumQuadVertices;

  glBindTexture(GL_TEXTURE_2D, 0);
}

void initializeHitboxes() {
  models.car.setHitbox(0.1f, 0.25f, 0.05f);
  models.simple_car.setHitbox(0.1f, 0.25f, 0.05f);
  models.tower01.setHitbox(3.0f, 3.0f, 3.0f);
  models.tower02.setHitbox(3.0f, 3.0f, 3.0f);
  std::cout << "Initializing hitbox geometry" << std::endl;
  // initHitBoxGeometry(shaderProgram, &models.car.hitboxGeometry, models.car.hitbox);

  // initHitBoxGeometry(shaderProgram, &models.building1.hitboxGeometry, models.building1.hitbox);
  // initHitBoxGeometry(shaderProgram, &models.tower10.hitboxGeometry, models.tower10.hitbox);
  // initHitBoxGeometry(shaderProgram, &models.billboard.hitboxGeometry, models.billboard.hitbox);
}

/**
 * @brief Initializer for configurable ground.
 * 
 */
void initConfigurableGround() {
  std::cout << "Init ground configurable ground..." << std::endl;
  
  models.groundVerticesConfigurable = createGroundFromConfigFile(CONFIG_FILE_NAME);
  models.groundVerticesConfigurable->generateVertices();
  
  std::cout << "Ground generated from config file..." << std::endl;
  
  initGroundGeometry(shaderProgram, &models.groundConfigurable, models.groundVerticesConfigurable);
  
  std::cout << "Ground initialized based on config file." << std::endl;
}


/** Initialize vertex buffers and vertex arrays for all objects. 
 */
void initializeModels() {
  if(loadModel(CAR_COM_MODEL_NAME, shaderProgram, &models.car.geometry) != true) {
    std::cerr << "initializeModels(): Load complex model car failed." << std::endl;
  }
  models.car.size = CAR_SIZE;
  CHECK_GL_ERROR();

  if(loadModel(CAR_MODEL_NAME, shaderProgram, &models.simple_car.geometry) != true) {
    std::cerr << "initializeModels(): Simple car model loading failed." << std::endl;
  }
  models.simple_car.size = CAR_SIZE;
  CHECK_GL_ERROR();

  if(loadModel(BUILDING1_MODEL_NAME, shaderProgram, &models.building1.geometry) != true) {
    std::cerr << "initializeModels(): Load complex model house failed." << std::endl;
  }
  models.building1.size = HOUSE_SIZE;
  CHECK_GL_ERROR();
  
  if(loadModel(TOWER01_MODEL_NAME, shaderProgram, &models.tower01.geometry) != true) {
    std::cerr << "initializeModels(): Load complex model tower failed." << std::endl;
  }
  models.tower01.size = TOWER_SIZE;
  CHECK_GL_ERROR();

  if(loadModel(TOWER02_MODEL_NAME, shaderProgram, &models.tower02.geometry) != true) {
    std::cerr << "initializeModels(): Load complex model tower failed." << std::endl;
  }
  models.tower02.size = TOWER_SIZE;
  CHECK_GL_ERROR();

  if(loadModel(TOWER03_MODEL_NAME, shaderProgram, &models.tower03.geometry) != true) {
    std::cerr << "initializeModels(): Load complex model tower failed." << std::endl;
  }
  models.tower03.size = TOWER_SIZE;
  CHECK_GL_ERROR();

  if(loadModel(TOWER04_MODEL_NAME, shaderProgram, &models.tower04.geometry) != true) {
    std::cerr << "initializeModels(): Load complex model tower failed." << std::endl;
  }
  models.tower04.size = TOWER_SIZE;
  CHECK_GL_ERROR();
  
  if(loadModel(TOWER05_MODEL_NAME, shaderProgram, &models.tower05.geometry) != true) {
    std::cerr << "initializeModels(): Load complex model tower failed." << std::endl;
  }
  models.tower05.size = TOWER_SIZE;
  CHECK_GL_ERROR();

  if(loadModel(TOWER06_MODEL_NAME, shaderProgram, &models.tower06.geometry) != true) {
    std::cerr << "initializeModels(): Load complex model tower failed." << std::endl;
  }
  models.tower06.size = TOWER_SIZE;
  CHECK_GL_ERROR();

  if(loadModel(TOWER07_MODEL_NAME, shaderProgram, &models.tower07.geometry) != true) {
    std::cerr << "initializeModels(): Load complex model tower failed." << std::endl;
  }
  models.tower07.size = TOWER_SIZE;
  CHECK_GL_ERROR();

  if(loadModel(TOWER08_MODEL_NAME, shaderProgram, &models.tower08.geometry) != true) {
    std::cerr << "initializeModels(): Load complex model tower failed." << std::endl;
  }
  models.tower08.size = TOWER_SIZE;
  CHECK_GL_ERROR();

  if(loadModel(TOWER09_MODEL_NAME, shaderProgram, &models.tower09.geometry) != true) {
    std::cerr << "initializeModels(): Load complex model tower failed." << std::endl;
  }
  models.tower09.size = TOWER_SIZE;
  CHECK_GL_ERROR();

  if(loadModel(TOWER10_MODEL_NAME, shaderProgram, &models.tower10.geometry) != true) {
    std::cerr << "initializeModels(): Load complex model tower failed." << std::endl;
  }
  models.tower10.size = TOWER_SIZE;
  CHECK_GL_ERROR();

  if(loadModel(REVOLVER_MODEL_NAME, shaderProgram, &models.revolver.geometry) != true) {
    std::cerr << "initializeModels(): Load complex model revolver failed." << std::endl;
  }
  models.revolver.size = REVOLVER_SIZE;
  CHECK_GL_ERROR();

  // initialize ground geometry
  std::cout << "Init ground..." << std::endl;

  models.groundVertices = generateGround();
  models.groundVertices->generateVertices();
  std::cout << "Ground generated..." << std::endl;
  
  initGroundGeometry(shaderProgram, &models.ground, models.groundVertices);
  std::cout << "Ground initialized." << std::endl;
  
  // initialize configurable ground geometry
  initConfigurableGround();

  // initialize billboard geometry
  initBillboardGeometry(shaderProgram, &models.billboard.geometry);
  models.billboard.size = BILLBOARD_SIZE;

  // fill MeshGeometry structure for explosion object
  initExplosionGeometry(explosionShaderProgram.program, &explosionGeometry);

  // fill MeshGeometry structure for explosion object
  initBlueFireGeometry(blueFireShaderProgram.program, &blueFireGeometry);

  // fill MeshGeometry structure for banner object
  initBannerGeometry(bannerShaderProgram.program, &bannerGeometry);

  // fill MeshGeometry structure for skybox object
  initSkyboxGeometry(skyboxFarPlaneShaderProgram.program, &skyboxGeometry, false);

  // fill MeshGeometry structure for skybox day object
  initSkyboxGeometry(skyboxFarPlaneShaderProgram.program, &skyboxDayGeometry, true);

  // fill MeshGeometry structure for crosshair object
  initCrosshairGeometry(crosshairShaderProgram.program, &crosshairGeometry);

  //FIll in the characters for writing text
  initAlphabetGeometries(crosshairShaderProgram.program, &alphabetCharacters, &ubuntu, &heart);

  // initialize FOG texture geometry
  initFogGeometry(fogShaderProgram.program, &fogGeometry);

  // initialize HUD geometry
  initHUDGeometry(hudShaderProgram.program, &HUDGeometry);

  models.initMaterials();
  printf("Custom materials initialized.");
}

void cleanupGeometry(MeshGeometry *geometry) {
  glDeleteVertexArrays(1, &(geometry->vertexArrayObject));
  glDeleteBuffers(1, &(geometry->elementBufferObject));
  glDeleteBuffers(1, &(geometry->vertexBufferObject));

  if(geometry->texture != 0)
    glDeleteTextures(1, &(geometry->texture));
}

void cleanupComplexGeometry(std::vector<MeshGeometry> complexGeometry) {
  for(int i = 0; i < complexGeometry.size(); ++i){
    glDeleteVertexArrays(1, &(complexGeometry[i].vertexArrayObject));
    glDeleteBuffers(1, &(complexGeometry[i].elementBufferObject));
    glDeleteBuffers(1, &(complexGeometry[i].vertexBufferObject));

    if(complexGeometry[i].texture != 0)
      glDeleteTextures(1, &(complexGeometry[i].texture));
  }
}

void cleanupModels() {
  cleanupComplexGeometry(models.car.geometry);
  if(models.car.hasHitbox)
    cleanupGeometry(models.car.hitboxGeometry);

  cleanupComplexGeometry(models.simple_car.geometry);
  if(models.simple_car.hasHitbox)
    cleanupGeometry(models.simple_car.hitboxGeometry);
  
  cleanupComplexGeometry(models.building1.geometry);
  if(models.building1.hasHitbox)
    cleanupGeometry(models.building1.hitboxGeometry);
  
  cleanupComplexGeometry(models.tower01.geometry);
  if(models.tower01.hasHitbox)
    cleanupGeometry(models.tower01.hitboxGeometry);
  
  cleanupComplexGeometry(models.tower02.geometry);
  if(models.tower02.hasHitbox)
    cleanupGeometry(models.tower02.hitboxGeometry);
  
  cleanupComplexGeometry(models.tower03.geometry);
  if(models.tower03.hasHitbox)
    cleanupGeometry(models.tower03.hitboxGeometry);
  
  cleanupComplexGeometry(models.tower04.geometry);
  if(models.tower04.hasHitbox)
    cleanupGeometry(models.tower04.hitboxGeometry);
  
  cleanupComplexGeometry(models.tower05.geometry);
  if(models.tower05.hasHitbox)
    cleanupGeometry(models.tower05.hitboxGeometry);
  
  cleanupComplexGeometry(models.tower06.geometry);
  if(models.tower06.hasHitbox)
    cleanupGeometry(models.tower06.hitboxGeometry);
  
  cleanupComplexGeometry(models.tower07.geometry);
  if(models.tower07.hasHitbox)
    cleanupGeometry(models.tower07.hitboxGeometry);
  
  cleanupComplexGeometry(models.tower08.geometry);
  if(models.tower08.hasHitbox)
    cleanupGeometry(models.tower08.hitboxGeometry);
  
  cleanupComplexGeometry(models.tower09.geometry);
  if(models.tower09.hasHitbox)
    cleanupGeometry(models.tower09.hitboxGeometry);
  
  cleanupComplexGeometry(models.tower10.geometry);
  if(models.tower10.hasHitbox)
    cleanupGeometry(models.tower10.hitboxGeometry);
  
  cleanupComplexGeometry(models.billboard.geometry);
  if(models.billboard.hasHitbox)
    cleanupGeometry(models.billboard.hitboxGeometry);

  cleanupComplexGeometry(models.revolver.geometry);
  if(models.revolver.hasHitbox)
    cleanupGeometry(models.revolver.hitboxGeometry);

  cleanupGeometry(models.ground);
  cleanupGeometry(models.groundConfigurable);
  cleanupGeometry(crosshairGeometry);
  cleanupGeometry(explosionGeometry);
  cleanupGeometry(bannerGeometry);
  cleanupGeometry(skyboxGeometry);
  cleanupGeometry(skyboxDayGeometry);
  cleanupGeometry(ubuntu);
  cleanupGeometry(heart);
  cleanupGeometry(HUDGeometry);
  // TODO ALPHABET CLEANUP
}
