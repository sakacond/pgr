/**
 * @file data.h
 * @author Ondrej Sakači (sakacond@fel.cvut.cz)
 * @brief 
 * @version 0.1
 * @date 2021-05-13
 * 
 * 
 */

#ifndef __DATA_H
#define __DATA_H

#include <string>

#define WINDOW_WIDTH   1080
#define WINDOW_HEIGHT  1080
#define WINDOW_TITLE   "Sakacond_SemestralWork"

#define SCENE_WIDTH  10.0f
#define SCENE_HEIGHT 10.0f
#define SCENE_DEPTH  10.0f

#define PI 3.14159265

// keys used in the key map
enum {
  KEY_LEFT_ARROW, 
  KEY_RIGHT_ARROW, 
  KEY_UP_ARROW, 
  KEY_DOWN_ARROW, 
  KEY_SPACE,
  KEY_CTRL,
  KEY_PLUS,
  KEY_MINUS,
  KEY_DIVIDE,
  KEY_MULTIPLY,
  KEY_Q,
  KEY_E,
  KEY_F,
  KEY_X,
  KEY_C,
  KEY_M,
  KEY_1,
  KEY_2,
  KEY_3,
  KEY_4,
  KEY_5,
  KEY_6,
  KEYS_COUNT
};

#define GROUND_LIMIT 10.0f 
#define GROUND_FRAGMENTS_X 20
#define GROUND_FRAGMENTS_Y 20

#define CAMERA_SPEED 0.04f
#define CAMERA_ROTATION_INCREMENT 2.0f
#define CAMERA_SPEED_MAX 1.2f
#define CAMERA_SPEED_MIN 0.0f

#define CAR_SPEED_MAX 1.0f
#define CAR_SPEED_INCREMENT 0.035f
#define CAR_BRAKE_INCREMENT 0.06f
#define CAR_WEIGHT 0.6f //tons
#define CAR_SIZE   0.05f
#define CAR_VIEW_ANGLE_DELTA_FORCE 4.0f

#define HOUSE_SIZE 1.0f
#define HOUSE_COUNT_MIN 1
#define HOUSE_COUNT_MAX 10

#define TOWER_SIZE 1.0f
#define TOWER_COUNT_MIN 1
#define TOWER_COUNT_MAX 5

#define BILLBOARD_HEIGHT 1.0f
#define BILLBOARD_SIZE   0.125f

#define BANNER_SIZE 1.0f

#define CROSSHAIR_SIZE 0.03f
#define TOP_CHARACTER_SIZE 0.03f
#define BOTTOM_CHARACTER_SIZE 0.05f

#define FOG_SIZE 10.0f
#define BLOOD_SIZE 10.f

#define REVOLVER_SIZE 0.1f

#define CAMERA_ELEVATION_MAX 20.0f

#define EPITROCHOID_A 5.0f
#define EPITROCHOID_B 2.0f
#define EPITROCHOID_C 3.6f

#define EPITROCHOID_A_SMALL 1.0f
#define EPITROCHOID_B_SMALL 0.5f
#define EPITROCHOID_C_SMALL 0.4f

const int billboardTrianglesCount = 32;

const float colors[] = {
  //r, g, b, // NAME
  1.0f, 0.0f, 0.0f, // 0, RED
  0.0f, 1.0f, 0.0f, // 1, GREEN
  0.0f, 0.0f, 1.0f, // 2, BLUE
  0.5f, 0.5f, 0.5f, // 3, GREY
  1.0f, 1.0f, 1.0f, // 4, WHITE
  1.0f, 0.0f, 0.5f, // 5, MAGENTA
};

const float normals[] = {
  1.0f, 0.0f, 0.0f,   // +X 0
  -1.0f, 0.0f, 0.0f,  // -X 1
  0.0f, 1.0f, 0.0f,   // +Y 2
  0.0f, -1.0f, 0.0f,  // -Y 3
  0.0f, 0.0f, 1.0f,   // +Z 4
  0.0f, 0.0f, -1.0f,  // -Z 5
};

const float dy = 0.25f;
const float dx = 5.0f;
const float dz = 4.0f;
const float dxi = 4.75f;

/**
 * @brief Definition of billboard vertices.
 * 
 */
const float billboardVertices[] = {
  //vertices
  //TOP
  -dx,  dy,   dz, // 0
  dx,   dy,   dz, // 1
  dx,   -dy,  dz, // 2
  -dx,  -dy,  dz, // 3
  //MIDDLE
  -dx,  dy,   0.0f, // 4
  -dxi, dy,   0.0f, // 5
  dxi,  dy,   0.0f, // 6
  dx,   dy,   0.0f, // 7
  dx,   -dy,  0.0f, // 8
  dxi,  -dy,  0.0f, // 9
  -dxi, -dy,  0.0f, // 10
  -dx,  -dy,  0.0f, // 11
  //BOTTOM
  -dx,  dy,   -dz, // 12
  -dxi, dy,   -dz, // 13
  dxi,  dy,   -dz, // 14
  dx,   dy,   -dz, // 15
  dx,   -dy,  -dz, // 16
  dxi,  -dy,  -dz, // 17
  -dxi, -dy,  -dz, // 18
  -dx,  -dy,  -dz, // 29
};

//Each triangle is defined by 3 vertex indexes from billboardVertices[], color values from color[]
//and normal from normals[]
const float billboardTriangles[] = {
  //Z+ NORMALS
  0, 2, 4,
  1, 2, 4,
  2, 2, 4,
  
  0, 2, 4, 
  2, 2, 4, 
  3, 2, 4,
  
  //Z- NORMALS
  5, 0, 5,
  10, 0, 5, 
  9, 0, 5,

  5, 0, 5, 
  6, 0, 5, 
  9, 0, 5,
  
  12, 0, 5, 
  13, 0, 5, 
  18, 0, 5,
  
  12, 0, 5, 
  19, 0, 5, 
  18, 0, 5,
  
  14, 0, 5, 
  15, 0, 5, 
  16, 0, 5,
  
  14, 0, 5, 
  17, 0, 5, 
  16, 0, 5,
  
  //Y+ NORMALS
  0, 0, 2, 
  4, 0, 2,  
  7, 0, 2, 
  
  0, 0, 2, 
  1, 0, 2, 
  7, 0, 2,
  
  4, 0, 2, 
  12, 0, 2, 
  13, 0, 2,
  
  4, 0, 2, 
  5, 0, 2, 
  13, 0, 2,
  
  6, 0, 2, 
  14, 0, 2, 
  15, 0, 2,
  
  6, 0, 2, 
  7, 0, 2, 
  15, 0, 2,
  
  //Y- NORMALS
  2, 3, 3, 
  3, 3, 3, 
  11, 3, 3,
  
  2, 3, 3, 
  8, 3, 3, 
  11, 3, 3,
  
  11, 0, 3, 
  19, 0, 3, 
  18, 0, 3,
  
  11, 0, 3, 
  10, 0, 3, 
  18, 0, 3,
  
  9, 0, 3, 
  17, 0, 3, 
  16, 0, 3,
  
  9, 0, 3, 
  8, 0, 3, 
  16, 0, 3,
  
  //X+ NORMALS
  2, 0, 0, 
  8, 0, 0, 
  7, 0, 0,
  
  2, 0, 0, 
  1, 0, 0, 
  7, 0, 0,
  
  8, 0, 0, 
  16, 0, 0, 
  15, 0, 0,
  
  8, 0, 0, 
  7, 0, 0, 
  15, 0, 0,
  
  10, 0, 0, 
  18, 0, 0, 
  13, 0, 0,
  
  10, 0, 0, 
  5, 0, 0, 
  13, 0, 0,
  
  //X- NORMALS
  0, 0, 1, 
  4, 0, 1,
  11, 0, 1,
  
  0, 0, 1,
  3, 0, 1,
  11, 0, 1,
  
  4, 0, 1,
  12, 0, 1,
  19, 0, 1,
  
  4, 0, 1,
  11, 0, 1,
  19, 0, 1,
  
  6, 0, 1,
  14, 0, 1,
  17, 0, 1,
  
  6, 0, 1,
  9, 0, 1,
  17, 0, 1,
};


const int hitboxTriangleCount = 12;

const int hitboxTriangles[] = {
  //X+
  2, 2, 0,
  6, 2, 0,
  5, 2, 0,

  2, 2, 0,
  5, 2, 0,
  1, 2, 0,

  //X-
  0, 2, 1,
  4, 2, 1,
  7, 2, 1,

  0, 2, 1,
  3, 2, 1,
  7, 2, 1,  

  //Y+
  0, 1, 2,
  4, 1, 2,
  5, 1, 2,

  0, 1, 2,
  5, 1, 2,
  1, 1, 2,
  
  //Y-
  3, 1, 3,
  7, 1, 3,
  6, 1, 3,

  3, 1, 3,
  6, 1, 3,
  2, 1, 3,

  //Z+ normal
  0, 0, 4,
  3, 0, 4,
  2, 0, 4,

  0, 0, 4,
  2, 0, 4,
  1, 0, 4,

  //Z- normal
  4, 0, 5,
  7, 0, 5,
  6, 0, 5,

  4, 0, 5,
  6, 0, 5,
  5, 0, 5,
};


//
// explosion billboard geometry definition 
//

const int explosionNumQuadVertices = 4;
const float explosionVertexData[explosionNumQuadVertices * 5] = {

  // x      y     z     u     v
  -1.0f, -1.0f, 0.0f, 0.0f, 0.0f,
   1.0f, -1.0f, 0.0f, 1.0f, 0.0f,
  -1.0f,  1.0f, 0.0f, 0.0f, 1.0f,
   1.0f,  1.0f, 0.0f, 1.0f, 1.0f,
};

//
// explosion billboard geometry definition 
//

const int blueFireNumQuadVertices = 4;
const float blueFireVertexData[blueFireNumQuadVertices * 5] = {

  // x      y     z     u     v
  -1.0f, -1.0f, 0.0f, 0.0f, 0.0f,
   1.0f, -1.0f, 0.0f, 1.0f, 0.0f,
  -1.0f,  1.0f, 0.0f, 0.0f, 1.0f,
   1.0f,  1.0f, 0.0f, 1.0f, 1.0f,
};

//
// "game over" banner geometry definition 
//

const int bannerNumQuadVertices = 4;
const float bannerVertexData[bannerNumQuadVertices * 5] = {

  // x      y      z     u     v
  -10.0f,  1.5f, 0.0f, 0.0f, 1.0f,
  -10.0f, -1.5f, 0.0f, 0.0f, 0.0f,
   10.0f,  1.5f, 0.0f, 3.0f, 1.0f,
   10.0f, -1.5f, 0.0f, 3.0f, 0.0f
};

//
// Crosshair geometry and or other square textures geometry
//

const int crosshairNumQuadVertices = 4;
const float crosshairVertexData[crosshairNumQuadVertices * 5] = {

  // x      y     z     u     v
  -10.0f, -10.0f, 0.0f, 0.0f, 0.0f,
   10.0f, -10.0f, 0.0f, 1.0f, 0.0f,
  -10.0f,  10.0f, 0.0f, 0.0f, 1.0f,
   10.0f,  10.0f, 0.0f, 1.0f, 1.0f
};

//
// Blood geometry
//

const int bloodNumQuadVertices = 4;
const float bloodVertexData[bloodNumQuadVertices * 5] = {

  // x      y     z     u     v
  -10.0f, -10.0f, 0.0f, 0.0f, 0.0f,
   10.0f, -10.0f, 0.0f, 1.0f, 0.0f,
  -10.0f,  10.0f, 0.0f, 0.0f, 1.0f,
   10.0f,  10.0f, 0.0f, 1.0f, 1.0f
};

//
// Fog geometry
//

const int fogNumQuadVertices = 4;
const float fogVertexData[fogNumQuadVertices * 5] = {
  // x      y     z     u     v
  -10.0f, -10.0f, 0.0f, 0.0f, 0.0f,
   10.0f, -10.0f, 0.0f, 1.0f, 0.0f,
  -10.0f,  10.0f, 0.0f, 0.0f, 1.0f,
   10.0f,  10.0f, 0.0f, 1.0f, 1.0f
};

#endif // __DATA_H
