//----------------------------------------------------------------------------------------
/**
 * \file    sceneGraph-demo.cpp
 * \author  Jaroslav Sloup, Tomas Barak
 * \date    2013
 * \brief   Scene graph demo
 */
//----------------------------------------------------------------------------------------

#include <string>
#include <vector>
#include <cstdio>
#include <string.h>
#include <iostream>

#include "pgr.h"   // includes all PGR libraries, like shader, glm, assimp ...

// scene graph includes
#include "sceneGraph/SceneNode.h"       // superclass of all scene graph nodes
#include "sceneGraph/TransformNode.h"   // model transformation
#include "sceneGraph/MeshNode.h"        // model loaded from the file 
#include "sceneGraph/Resources.h"
#include "sceneGraph/MeshGeometry.h"
#include "sceneGraph/AxesNode.h"        // coordinate axes

// nodes derived from scene graph nodes
#include "ScaleAnimNode.h"     // animation - change of scale
#include "InfinityAnimNode.h"  // animation transformation - movement along infinity sign

#include "trackball.h"

#define WINDOW_TITLE "Scene Graph Demo"
const int WINDOW_WIDTH = 800;
const int WINDOW_HEIGHT = 800;

// file name used during the scene graph creation
#define TERRAIN_FILE_NAME "./data/terrain"
#define AIRPLANE_FILE_NAME pgr::frameworkRoot() + "data/MiG25/MiG25.obj"
#define PANDA_FILE_NAME pgr::frameworkRoot() + "data/panda/panda.obj"

// trackball class -> uses quaterninons to rotate the scene
pgr::CQuaternionTrackball trackball;
glm::mat4 trackballRotation;			// trackball rotation matrix

// scene graph root node
pgr::sg::SceneNode* rootNode_p = NULL; // scene root

glm::mat4 projectionMatrix;            // projection transformation matrix

// global variables
int       g_mouseOldX;             // mouse position
int       g_mouseOldY;
int       g_winWidth, g_winHeight; // window size
float     g_translation;


pgr::sg::SceneNode * createAxes(const char* name = "axes", float scale = 1, pgr::sg::SceneNode * parent = NULL) {
  pgr::sg::TransformNode* axesTransform_p = new pgr::sg::TransformNode("axesTransform", parent);

  axesTransform_p->scale(glm::vec3(scale));
  return new pgr::sg::AxesNode(name, axesTransform_p);
}

void initializeScene() {
  // create scene root node - root node represents the whole scene transformation
  rootNode_p = new pgr::sg::SceneNode("root");

  // creation of the terrain transform node
  pgr::sg::TransformNode* transformNode_p = new pgr::sg::TransformNode("terrainTransform", rootNode_p);
  transformNode_p->translate(glm::vec3(0.0f, -10.0f, 0.0f));
  transformNode_p->scale(glm::vec3(80.0f, 20.0f, 80.0f));

  // MeshGeometry holds all the submeshes of the imported model
  pgr::sg::MeshGeometry* mesh_p = NULL;
  // ask model manager for the terrain geometry
  if(!pgr::sg::MeshManager::Instance()->exists(TERRAIN_FILE_NAME))
    pgr::sg::MeshManager::Instance()->insert(TERRAIN_FILE_NAME, pgr::sg::MeshGeometry::LoadRawHeightMap(TERRAIN_FILE_NAME));
  mesh_p = pgr::sg::MeshManager::Instance()->get(TERRAIN_FILE_NAME);

  // creation of the terrain mesh node
  pgr::sg::MeshNode* terrainNode_p = new pgr::sg::MeshNode(TERRAIN_FILE_NAME, transformNode_p);
  terrainNode_p->setGeometry(mesh_p);

  // creation of the airplane transform node
  transformNode_p = new pgr::sg::TransformNode("airplaneTransform", rootNode_p);
  // set up of transformations of the airplane
  transformNode_p->scale(glm::vec3(5.0f, 5.0f, 5.0f));

  // setup the rotation of the airplane
  InfinityAnimNode* rotationNode_p = new InfinityAnimNode("airplaneRotationAnim", transformNode_p);
  rotationNode_p->setAxis(glm::vec3(0.0f, 1.0f, 0.0f));
  rotationNode_p->setSpeed(M_PI / 3.0f);

  // ask model manager for the aiplane geometry
  mesh_p = pgr::sg::MeshManager::Instance()->get(AIRPLANE_FILE_NAME);

  // creation of the airplane mesh node
  pgr::sg::MeshNode* airplaneNode_p = new pgr::sg::MeshNode(AIRPLANE_FILE_NAME, rotationNode_p);
  airplaneNode_p->setGeometry(mesh_p);

  createAxes( "airplaneAxes", 0.5f, rotationNode_p );

  // creation of the panda transform node
  transformNode_p = new pgr::sg::TransformNode("pandaTransform", rootNode_p);
  // set up of transformations of the panda
  transformNode_p->translate(glm::vec3(-19.0f, -6.0f, 18.0f));

  // setup the scale transform of the panda
  ScaleAnimNode* scaleNode_p = new ScaleAnimNode("pandaScaleAnim", transformNode_p);
  scaleNode_p->setRange(1.0f, 2.75f);
  scaleNode_p->setSpeed(0.75f);

  // ask model manager for the panda geometry
  mesh_p = pgr::sg::MeshManager::Instance()->get(PANDA_FILE_NAME);

  // creation of the panda mesh node
  pgr::sg::MeshNode* pandaNode_p = new pgr::sg::MeshNode(PANDA_FILE_NAME, scaleNode_p);
  pandaNode_p->setGeometry(mesh_p);

  // dump our scene graph tree for debug
  rootNode_p->dump();
}

// Called after the window and OpenGL are initialized. Called exactly once, before the main loop.
void init() {

  initializeScene();

  // initialize trackball -> drag from point [0,0] to point [0,0]
  trackball.setRotation(0.0f, 0.0f, 0.0f, 0.0f);

  // build rotation matrix from quaternion
  trackball.getRotationMatrix(trackballRotation);

  g_translation = 65.0f;

  glCullFace(GL_BACK);
  glEnable(GL_CULL_FACE); // draw front faces only
  glEnable(GL_DEPTH_TEST);
  glDepthFunc(GL_LEQUAL);
}

// Called to update the display.
// You should call glutSwapBuffers() after all of your rendering to display what you rendered.
void displayCallback() {

  glClearColor(0.3f, 0.3f, 0.3f, 1.0f);
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

  // initial camera viewpoint
  glm::mat4 viewMatrix = glm::lookAt(glm::vec3(0.0f, 10.0f, g_translation), glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(0.0f, 1.0f, 0.0f));
  // virtual trackball rotation
  viewMatrix *= trackballRotation;

  // draw the whole scene graph
  if(rootNode_p)
    rootNode_p->draw(viewMatrix, projectionMatrix);

  glutSwapBuffers();
}

// Called whenever the window is resized. The new window size is given, in pixels.
// This is an opportunity to call glViewport or glScissor to keep up with the change in size.
void reshapeCallback(int width, int height) {
  float aspectRatio = (float)width / height;

  glViewport(0, 0, (GLsizei) width, (GLsizei) height);
  g_winWidth = width;
  g_winHeight = height;

  // perspective projection setup
  projectionMatrix =  glm::perspective(60.0f, aspectRatio, 1.0f, 1000.0f);
}

// Called whenever a key on the keyboard was pressed.
// The key is given by the ''key'' parameter, which is in ASCII.
// It's often a good idea to have the escape key (ASCII value 27) call glutLeaveMainLoop() to
// exit the program.
void keyboardCallback(unsigned char key, int x, int y) {

  switch (key) {
    case 27:
      glutLeaveMainLoop();
      break;
    default:
      break;
  }
}

void specialKeyboardCallback(int specKey, int x, int y) {

  switch(specKey) {
    case GLUT_KEY_UP:
      if(g_translation > 5.0f)
        g_translation -= 1.0f;
      break;
    case GLUT_KEY_DOWN:
      if(g_translation < 100.0f)
        g_translation += 1.0f;
      break;
  }
  glutPostRedisplay();
}

void timerCallback(int) {
// ELAPSED_TIME is number of milliseconds since glutInit() called
  double timed = 0.001 * (double)glutGet(GLUT_ELAPSED_TIME); // milliseconds => seconds

  if(rootNode_p)
    rootNode_p->update(timed);

  glutTimerFunc(33, timerCallback, 0);
  glutPostRedisplay();
}

void motionCallback(int x, int y) {

  if(g_mouseOldX != x || g_mouseOldY != y) {
    /* get rotation from trackball */
    trackball.addRotation(g_mouseOldX, g_mouseOldY, x, y, g_winWidth, g_winHeight);

    /* build rotation matrix from quaternion */
    trackball.getRotationMatrix(trackballRotation);

    g_mouseOldX = x;
    g_mouseOldY = y;

    glutPostRedisplay();
  }
}

void mouseCallback(int button, int state, int x, int y) {

  if( button == GLUT_LEFT_BUTTON ) {
    if( state == GLUT_DOWN ) {
      g_mouseOldX = x;
      g_mouseOldY = y;
      glutMotionFunc(motionCallback);
    } else {
      glutMotionFunc(NULL);
    }

    glutPostRedisplay();
  }
}

/*
 * Entry point
 */
int main(int argc, char** argv) {

  // initialize window system
  glutInit(&argc, argv);

  glutInitContextVersion(pgr::OGL_VER_MAJOR, pgr::OGL_VER_MINOR);
  glutInitContextFlags(GLUT_FORWARD_COMPATIBLE);

  glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE | GLUT_DEPTH);
  // initial window size
  glutInitWindowSize(WINDOW_WIDTH, WINDOW_HEIGHT);
  glutInitWindowPosition(200,200);
  glutCreateWindow(WINDOW_TITLE);

  // register callback for drawing a window contents
  glutDisplayFunc(displayCallback);
  // register callback for change of window
  glutReshapeFunc(reshapeCallback);
  // register callback for keyboard
  glutKeyboardFunc(keyboardCallback);
  // register callback for keyboard - special function keys
  glutSpecialFunc(specialKeyboardCallback);
  // register callback for mouse button press/release
  glutMouseFunc(mouseCallback);
  glutTimerFunc(33, timerCallback, 0);

  if(!pgr::initialize(pgr::OGL_VER_MAJOR, pgr::OGL_VER_MINOR))
    pgr::dieWithError("pgr init failed, required OpenGL not supported?");

  init();

  glutMainLoop();
  return 0;
}
