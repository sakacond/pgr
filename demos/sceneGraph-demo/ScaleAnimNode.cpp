//----------------------------------------------------------------------------------------
/**
 * \file    ScaleAnimNode.cpp
 * \author  Jaroslav Sloup, Tomas Barak
 * \date    2013
 * \brief   Simple animation node based on object scaling.
 */
//----------------------------------------------------------------------------------------

#include "ScaleAnimNode.h"

ScaleAnimNode::ScaleAnimNode(const std::string &name, pgr::sg::SceneNode* parent):
  pgr::sg::SceneNode(name, parent), m_speed(0), m_minScale(1.0f), m_maxScale(10.0f), m_scaleRange(10.0f) {
}

void ScaleAnimNode::update(double elapsedTime) {
  double pathLength = m_speed * elapsedTime;

  double scale = m_minScale + m_scaleRange * sin(
      M_PI*(pathLength - (int)(pathLength / m_scaleRange)*m_scaleRange)/m_scaleRange);

  // start with identity and then add scale transform
  m_local_mat = glm::scale(glm::mat4(1.0f), glm::vec3(scale));

  /// call inherited update (which calculates global matrix and updates children)
  pgr::sg::SceneNode::update(elapsedTime);
}
