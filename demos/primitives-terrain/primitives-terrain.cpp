/**
 * \file primitives-terrain.cpp
 * \brief Displays a terrain using triangle strips.
 * \author Jaroslav Sloup, Tomas Barak
 */

#include <cmath>
#include <iostream>
#include <string>

#include "trackball.h"
#include "pgr.h"

#if _MSC_VER
#define snprintf _snprintf
#endif

const int WIN_WIDTH = 800;
const int WIN_HEIGHT = 500;
const char* WIN_TITLE = "PGR - Primitives - terrain";
const unsigned int REFRESH_INTERVAL = 33;
const int NLIGHTS = 3;
double elapsedTime = 0.0;
glm::mat4 projection = glm::mat4(1.0f);
glm::mat4 model = glm::mat4(1.0f);

// virtual trackball related stuff
pgr::CQuaternionTrackball trackball;	// trackball class -> uses quaterninons to rotate the scene
int startGrabX, startGrabY;				// trackball starting point
int endGrabX, endGrabY;					// trackball end point
int winWidth, winHeight;				// window width & height
glm::mat4 trackballRotation;			// trackball rotation matrix
glm::mat4 modelRotation;				// model rotation matrix

GLuint terrainShaderProgram = 0;
GLuint terrainVbo = 0;
GLuint terrainTrianglesEao = 0;
GLuint terrainTrianglesVao = 0;
GLenum displayMode = GL_LINE;

#define N 15

// array of cup vertices
const unsigned int nTerrainVertices = N * N;
const unsigned nTerrainAttribsPerVertex = 6; // x,y,z position, nx,ny,nz normal vector
float terrainVertices[nTerrainVertices * nTerrainAttribsPerVertex];

// indices used to draw cube as GL_TRIANGLE_STRIP
const unsigned nTerrainTriangleIdx = 2*N;
unsigned short terrainTriangleIdx[nTerrainTriangleIdx*(N-1)];

// starting indices of the triangle strips
const GLvoid* indicesStartAt[N-1];
// number of vertices in each strip
static GLsizei primsCount[N-1];

GLint colorLoc = -1;
glm::vec3 sphereColor(1.0f, 1.0f, 1.0f);

struct Light {
  Light(float r = 1.0f, float g = 1.0f, float b = 1.0f):
    color(r, g, b, 0.0f), position(0.0f, 0.0f, 0.0f, 1.0f) {}

  glm::vec4 position;
  glm::vec4 color;
};

Light lights[NLIGHTS] = {
  Light(1.0f, 0.5f, 0.5f),
  Light(0.5f, 0.5f, 1.0f),
  Light(0.5f, 1.0f, 0.5f)
};

// vertex shader just sends position and normal to the fragment shader
std::string vertexShaderSrc =
    "#version 140\n"
    "uniform mat4 modelMat;\n"
    "uniform mat4 projMat;\n"
    "in vec3 vertex;\n"
    "in vec3 normal;\n"
    "out vec3 normal_v;\n"
    "out vec3 position_v;\n"
    "void main()\n"
    "{\n"
    "  vec4 nor4 = modelMat * vec4(normal, 0.0f);\n"
    "  normal_v = nor4.xyz;\n"
    "  vec4 pos4 = modelMat * vec4(vertex, 1.0f);\n"
    "  position_v = pos4.xyz / pos4.w;\n"
    "  gl_Position = projMat * pos4;\n"
    "}\n"
    ;

// takes interpolated normal and position and calculates per pixel diffuse lighting
std::string fragmentShaderSrc =
    "#version 140\n"
    "#define NLIGHTS 3\n"
    "struct Light {\n"
    "  vec3 position;\n"
    "  vec3 color;\n"
    "};\n"
    "uniform Light lights[NLIGHTS];\n"
    "in vec3 normal_v;\n"
    "in vec3 position_v;\n"
    "uniform vec3 color;\n"
    "out vec3 color_f;\n"
    "void main()\n"
    "{\n"
    "  vec3 diffuse = color;\n"
    "  vec3 color = vec3(0.0f);\n"
    "  vec3 N = normalize(normal_v);\n"
    "  for(int l = 0; l < NLIGHTS; ++l) {\n"
    "    vec3 L = lights[l].position - position_v;\n"
    "    float dist = length(L);\n"
    "    float attenuation = min(1.0f / (dist * dist), 1.0f);\n"
    "    color += diffuse * lights[l].color * max(dot(N, L) / dist, 0.0f) * attenuation;\n"
    "  }\n"
    "  color_f = 0.85*color + vec3(0.15f, 0.15f, 0.15f);\n"
    "}\n"
    ;

void generateTerrain(float dx, float dz) {

  GLfloat hf[N][N];

  srand(100);

  for(int z=0; z<N; z++)
    for(int x=0; x<N; x++)
      hf[z][x] = -0.15f + 0.2f*rand() / (float)RAND_MAX;

  float pz = -dz*(N-1)*0.5f;

  unsigned int vertIdx = 0;
  unsigned int idx = 0;

  for(int z=0; z<N; z++) {

    if(z != N-1) {
      primsCount[z] = nTerrainTriangleIdx;
      indicesStartAt[z] = (GLvoid *)(z*nTerrainTriangleIdx*sizeof(unsigned short));
    }

    float px = -dx*(N-1)*0.5f;
    for(int x=0; x<N; x++) {

      terrainVertices[nTerrainAttribsPerVertex*vertIdx+0] = px;	        // x
      terrainVertices[nTerrainAttribsPerVertex*vertIdx+1] = hf[z][x];	// y
      terrainVertices[nTerrainAttribsPerVertex*vertIdx+2] = pz;	        // z
      // add two indices to the strip
      if(z != N-1) {
        terrainTriangleIdx[idx++] = vertIdx;
        terrainTriangleIdx[idx++] = vertIdx + N;
      }

      vertIdx++;
      px += dx;
    }
    pz += dz;
  }

#define X 0
#define Y 1
#define Z 2

  float length;

  float *point0, *point1, *point2;
  float mul = dx * dz;
  float nx, ny, nz;

  int count;

  // For now we will do it the hard way - by generating the normals individually for each elevation point
  for (int iz=0; iz<N; iz++) {
    for (int ix=0; ix<N; ix++) {

      nx = ny = nz = 0.0f;

      // count of normals added to current vertex
      count = 0;

      point0 = terrainVertices + nTerrainAttribsPerVertex*(iz*N + ix) + Y;

      if(iz<N-1 && ix<N-1) {
        point1 = point0 + nTerrainAttribsPerVertex*N; //m_pVertices + 3L*((iz+1)*_resX + ix) + Y;
        point2 = point0 + nTerrainAttribsPerVertex;

        nx -= dz * (*point2 - *point0);
        ny += mul;
        nz -= dx * (*point1 - *point0);

        count++;
      }

      if(iz>0 && ix<N-1) {
        point1 = point0 + nTerrainAttribsPerVertex;
        point2 = point0 - nTerrainAttribsPerVertex*N; //m_pVertices + 3L*((iz-1)*_resX + ix) + Y;

        nx -= dz * (*point1 - *point0);
        ny += mul;
        nz += dx * (*point2 - *point0);

        count++;
      }

      if(iz>0 && ix>0) {
        point1 = point0 - nTerrainAttribsPerVertex*N; //m_pVertices + 3L*((iz-1)*_resX+ix) + Y;
        point2 = point0 - nTerrainAttribsPerVertex;

        nx += dz * (*point2 - *point0);
        ny += mul;
        nz += dx * (*point1 - *point0);

        count++;
      }

      if(iz<N-1 && ix>0) {
        point1 = point0 - nTerrainAttribsPerVertex;
        point2 = point0 + nTerrainAttribsPerVertex*N; //m_pVertices + 3L*((iz+1)*_resX + ix) + Y;

        nx += dz * (*point1 - *point0);
        ny += mul;
        nz -= dx * (*point2 - *point0);

        count++;
      }

      if(count == 0) {
        char errorStr[256];
        sprintf(errorStr, "generateTerrain(): Failed to find any normals at: [%d, %d]", ix, iz);
        std::cerr << errorStr << std::endl;
        exit(1);
      }

      // normalize it
      length = sqrt( nx*nx + ny*ny + nz*nz );

      if(length < 1.0E-5f) {
        nx = 0.0f;
        ny = 1.0f;
        nz = 0.0f;
      } else {
        nx /= length;
        ny /= length;
        nz /= length;
      }

      point0 = terrainVertices + nTerrainAttribsPerVertex*(iz*N + ix);

      point0[3] = nx;
      point0[4] = ny;
      point0[5] = nz;
    }
  }

#undef X
#undef Y
#undef Z
}


void init() {

  // opengl setup
  glClearColor(0.3f, 0.3f, 0.3f, 1.0f);
  //glEnable(GL_CULL_FACE);
  glEnable(GL_DEPTH_TEST);
  glPointSize(5.0f);

  // load shader
  std::vector<GLuint> shaders;
  shaders.push_back(pgr::createShaderFromSource(GL_VERTEX_SHADER, vertexShaderSrc));
  shaders.push_back(pgr::createShaderFromSource(GL_FRAGMENT_SHADER, fragmentShaderSrc));
  terrainShaderProgram = pgr::createProgram(shaders);

  // handles to vertex shader inputs
  GLint vertexLoc = glGetAttribLocation(terrainShaderProgram, "vertex");
  GLint normalLoc = glGetAttribLocation(terrainShaderProgram, "normal");
  colorLoc = glGetUniformLocation(terrainShaderProgram, "color");

  generateTerrain(3.5f/(N-1), 3.5f/(N-1));

  // buffer for vertices
  glGenBuffers(1, &terrainVbo);
  glBindBuffer(GL_ARRAY_BUFFER, terrainVbo);
  glBufferData(GL_ARRAY_BUFFER, sizeof(terrainVertices), terrainVertices, GL_STATIC_DRAW);
  glBindBuffer(GL_ARRAY_BUFFER, 0);

  // buffer for triangle indices
  glGenBuffers(1, &terrainTrianglesEao);
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, terrainTrianglesEao);
  glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(terrainTriangleIdx), terrainTriangleIdx, GL_STATIC_DRAW);
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

  // indexed triangle strips
  glGenVertexArrays(1, &terrainTrianglesVao);
  glBindVertexArray(terrainTrianglesVao);
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, terrainTrianglesEao);
  glBindBuffer(GL_ARRAY_BUFFER, terrainVbo);
  glEnableVertexAttribArray(vertexLoc);
  glVertexAttribPointer(vertexLoc, 3, GL_FLOAT, GL_FALSE, nTerrainAttribsPerVertex * sizeof(float), (void *)(0));
  glEnableVertexAttribArray(normalLoc);
  glVertexAttribPointer(normalLoc, 3, GL_FLOAT, GL_FALSE, nTerrainAttribsPerVertex * sizeof(float), (void *)(nTerrainAttribsPerVertex/2 * sizeof(float)));

  glBindVertexArray(0);
  CHECK_GL_ERROR();

  // initialize trackball -> drag from point [0,0] to point [0,0]
  trackball.setRotation(0.0f, 0.0f, 0.0f, 0.0f);
}

// deletes allocated buffers
void cleanup() {

  glDeleteVertexArrays(1, &terrainTrianglesVao);
  glDeleteBuffers(1, &terrainTrianglesEao);
  glDeleteBuffers(1, &terrainVbo);
  pgr::deleteProgramAndShaders(terrainShaderProgram);
}

void switchMode() {

  switch(displayMode) {
    case GL_POINT:
      displayMode = GL_LINE;
      break;
    case GL_LINE:
      displayMode = GL_FILL;
      break;
    case GL_FILL:
      displayMode = GL_POINT;
      break;
    default:;
  }
}

void refreshCb(int) {

  glutTimerFunc(REFRESH_INTERVAL, refreshCb, 0);
  elapsedTime = 0.001 * (double)glutGet(GLUT_ELAPSED_TIME);					// milliseconds => seconds

  float rotAngleDeg = elapsedTime * 60.0f;		// rotate 60 degrees per second
  float l0rotAngleDeg = elapsedTime * 60.0f;	// rotate 60 degrees per second
  float l1rotAngleDeg = elapsedTime * 40.0f;	// rotate 40 degrees per second
  float radius = 2.5f;
  float terrainCenterOff = -5.0f;

  lights[0].position = glm::vec4(radius * cos(glm::radians(l0rotAngleDeg)), radius * sin(glm::radians(l0rotAngleDeg)), terrainCenterOff, 1.0f);
  lights[1].position = glm::vec4(radius * cos(glm::radians(l1rotAngleDeg)), 0.0f, radius * sin(glm::radians(l1rotAngleDeg)) + terrainCenterOff, 1.0f);
  lights[2].position = glm::vec4(0.0f, radius * cos(glm::radians(l1rotAngleDeg)), radius * sin(glm::radians(l1rotAngleDeg)) + terrainCenterOff, 1.0f);

  model = glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 0.0f, terrainCenterOff));	// move terrain 5 units back
  model = glm::rotate(model, 30.0f, glm::vec3(1.0f, 0.0f, 0.0f));					// tilt terrain by 30 degrees

  modelRotation = glm::rotate(glm::mat4(1.0f), rotAngleDeg, glm::vec3(0.0f, 1.0f, 0.0f));				// the animation

  glutPostRedisplay();
}

void displayCb() {

  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

  // compose model transformations
  glm::mat4 newModel = model * trackballRotation * modelRotation;

  glUseProgram(terrainShaderProgram);
  glUniformMatrix4fv(glGetUniformLocation(terrainShaderProgram, "modelMat"), 1, GL_FALSE, glm::value_ptr(newModel));
  glUniformMatrix4fv(glGetUniformLocation(terrainShaderProgram, "projMat"), 1, GL_FALSE, glm::value_ptr(projection));

  for(int light = 0; light < NLIGHTS; ++light) {
    char buf[255];
    snprintf(buf, 255, "lights[%i].position", light);
    glUniform3fv(glGetUniformLocation(terrainShaderProgram, buf), 1, glm::value_ptr(lights[light].position));
    snprintf(buf, 255, "lights[%i].color", light);
    glUniform3fv(glGetUniformLocation(terrainShaderProgram, buf), 1, glm::value_ptr(lights[light].color));
  }

  glPolygonMode(GL_FRONT_AND_BACK, displayMode);

  glBindVertexArray(terrainTrianglesVao);

  glUniform3fv(colorLoc, 1, glm::value_ptr(sphereColor));
  // draws multiple triangles trips, primsCount[i] contains number of vertices in each strip,
  // vertex indices start at the indicesStartAt[i] index
  glMultiDrawElements(GL_TRIANGLE_STRIP, primsCount, GL_UNSIGNED_SHORT, indicesStartAt, N-1);
  // void glMultiDrawElements(GLenum mode, GLsizei *count, GLenum type, const GLvoid **indices, GLsizei primcount);
  // command has the same effect as the following sequence:
  //   for (i = 0; i < primcount; i++) {
  //     if (count[i] > 0)
  //       glDrawElements(mode, count[i], type, indices[i]);
  //   }

  glutSwapBuffers();
}

void reshapeCb(int w, int h) {

  winWidth = w;
  winHeight = h;

  glViewport(0, 0, w, h);
  projection =  glm::perspective(60.0f, float(w) / float(h) , 1.0f, 10.0f);
}

void keyboardCb(unsigned char key, int x, int y) {

  switch (key) {
    case 27:
      glutLeaveMainLoop();
      break;
    case ' ':
      switchMode();
      break;
  }
}

// mouse motion within the window with any button pressed (mouse drag)
void mouseMotionCb(int x, int y) {

  endGrabX = x;
  endGrabY = y;

  if(startGrabX != endGrabX || startGrabY != endGrabY) {

    /* get rotation from trackball using quaternion */
    trackball.addRotation(startGrabX, startGrabY, endGrabX, endGrabY, winWidth, winHeight);

    /* build rotation matrix from quaternion */
    trackball.getRotationMatrix(trackballRotation);

    startGrabX = endGrabX;
    startGrabY = endGrabY;

    glutPostRedisplay();
  }
}

//mouse button pressed within a window or released
void mouseCb(int button, int state, int x, int y) {

  if(button == GLUT_LEFT_BUTTON) {
    if(state == GLUT_DOWN) {
      startGrabX = x;
      startGrabY = y;
      glutMotionFunc(mouseMotionCb);
      return;
    } else {
      glutMotionFunc(NULL);
    }
  }
}

int main(int argc, char** argv) {

  glutInit(&argc, argv);

  glutInitContextVersion(pgr::OGL_VER_MAJOR, pgr::OGL_VER_MINOR);
  glutInitContextFlags(GLUT_FORWARD_COMPATIBLE);

  glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE | GLUT_DEPTH);
  glutInitWindowSize(WIN_WIDTH, WIN_HEIGHT);
  glutCreateWindow(WIN_TITLE);

  glutDisplayFunc(displayCb);
  glutReshapeFunc(reshapeCb);
  glutKeyboardFunc(keyboardCb);
  glutMouseFunc(mouseCb);
  glutTimerFunc(REFRESH_INTERVAL, refreshCb, 0);

  if(!pgr::initialize(pgr::OGL_VER_MAJOR, pgr::OGL_VER_MINOR))
    pgr::dieWithError("pgr init failed, required OpenGL not supported?");

  init();

  std::cout << "Use space to cycle through modes, Esc to quit." << std::endl;

  glutMainLoop();
  cleanup();
  return 0;
}
